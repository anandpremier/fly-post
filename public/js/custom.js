$('#state').on('change', function () {
    var id = $('#state').val();
    var token = $('#cp_token').val();
    var route = "/fetch/cities/";

    $.ajax({
        dataType: "json",
        type: "post",
        url: route,
        data: {
            "_token": token,
            "id": id
        },
        success: function (data) {
            $('#city').html(data);
        }
    });
});
$('#registeredit').on('click', function () {
    var old = $('#old_password').val();
    var newpass = $('#new_password').val();
    var retypepass = $('#retype_password').val();
    var token = $('#cp_token').val();
    var route = "/influencer/cp/";

    if (newpass === '' && retypepass === '')
    {
        $('#new_password_error').html('Please Insert Values');
        $('#retype_password_error').html('Please Insert Values');
        $('#new_password_error').attr('style', 'display:show;color:red');
        $('#retype_password_error').attr('style', 'display:show;color:red');
    } else if (newpass === '')
    {
        $('#new_password_error').html('Please Insert Values');
        $('#new_password_error').attr('style', 'display:show;color:red');
        $('#retype_password_error').attr('style', 'display:none');
    } else if (retypepass === '')
    {
        $('#retype_password_error').html('Please Insert Values');
        $('#retype_password_error').attr('style', 'display:show;color:red');
        $('#new_password_error').attr('style', 'display:none');
    } else if (newpass === retypepass && old === '') {
        $('#old_password_error').html('Please Insert Old Password');
        $('#old_password_error').attr('style', 'display:show;color:red');
        $('#new_password_error').attr('style', 'display:none');
        $('#retype_password_error').attr('style', 'display:none');
    } else if (newpass === retypepass && old !== '') {
        $('#old_password_error').attr('style', 'display:none;');
        $('#new_password_error').attr('style', 'display:none');
        $('#retype_password_error').attr('style', 'display:none');

        $.ajax({
            dataType: "json",
            type: "post",
            url: route,
            data: {
                "_token": token,
                "old": old,
                "new": newpass,
                "retype": retypepass,
            },
            success: function (data) {

                if (data == 1) {
                    alert('Password SuccessFully Changed');
                    $('#old_password').val('');
                    $('#new_password').val('');
                    $('#retype_password').val('');
                } else {
                    $('#old_password_error').attr('style', 'display:show;color:red;');
                    $('#new_password').val('');
                    $('#retype_password').val('');
                }

            }
        });
    } else {
        $('#new_password_error').html('Passwords Not Match');
        $('#retype_password_error').html('Passwords Not Match');
        $('#new_password_error').attr('style', 'display:show;color:red')
        $('#retype_password_error').attr('style', 'display:show;color:red')
    }
});

$('#datepicker').datepicker({
    uiLibrary: 'bootstrap',
    maxDate: new Date()
});

$("#influencerRegister").validate({
    ignore: "",
    rules: {
        firstname: "required",
        lastname: "required",
        country: "required",
        state: "required",
        city: "required",
        date_of_birth: {
            required:true
        },
        zipcode: "required",
        gender: "required",
        terms_and_conditions:"required",
        privacy_policy:"required",
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
            minlength: 8
        },
        password_confirmation: {
            required: true,
            minlength: 8,
            equalTo: "#password"
        },
        twitter_channel:{
            required: function() {
                if($("#fb_channel").val().length === 0)
                    return true;
                else
                    return false;
            }
        },
        facebookoAuth: {
            required: function() {
                if(($("#twitteroAuth").val().length === 0))
                    return true;
                else
                    return false;
            }
        },
    },
    errorPlacement: function (error, element)
    {
        element.after(error);

        if(element.attr("name") === "facebookoAuth"){
            error.insertAfter($('#twitterChannel'));
            $('<br>').insertAfter('#twitterChannel');
        }

        if(element.attr("name") === "city"){
            error.insertAfter($('.cityDrop .select2-selection__arrow'));
        }

        if(element.attr("name") === "state") {
            error.insertAfter($('.stateDrop .select2-selection__arrow'));
        }

        if(element.attr("name") === "gender"){
            error.insertAfter($('.genderDrop .select2-selection__arrow'));
        }

        if (element.attr("type") === "checkbox" && element.attr("name") === "terms_and_conditions") {
            $('<br>').insertAfter('#exampleCheck');
        }

        if (element.attr("type") === "checkbox" && element.attr("name") === "privacy_policy") {
            $('<br>').insertAfter('#exampleCheck1');
        }
    },
    messages: {
        firstname: "Please enter your firstname",
        lastname: "Please enter your lastname",
        password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 8 characters long"
        },
        password_confirmation: {
            required: "Please provide a password",
            minlength: "Your password must be at least 8 characters long",
            equalTo: "Please enter the same password as above"
        },
        email: "Please enter a valid email address",
        country: "Please enter a valid country",
        state: "Please select a valid state",
        city: "Please select a valid city",
        date_of_birth: {
            required:"Please enter a valid Date of Birth",

        },
        zipcode: "Please enter a valid zipcode",
        gender: "Please select a valid gender",
        terms_and_conditions:"Accept Terms And Conditions To Proceed",
        privacy_policy:"Accept Privacy Policy To Proceed",
        facebookoAuth:"Please Select Any Channel To Proceed",
        twitter_channel:"Please Select Any Channel To Proceed",
    }
});
$('#exampleCheck').on('change', function () {
    this.value = this.checked ? 1 : 0;
    // alert(this.value);
}).change();

$('#exampleCheck1').on('change', function () {
    this.value = this.checked ? 1 : 0;
    // alert(this.value);
}).change();

$('#channel_fb').on('change', function () {
    this.value = this.checked ? 1 : 0;
    // alert(this.value);
}).change();

$('#channel_tw').on('change', function () {
    this.value = this.checked ? 2 : 0;
    // alert(this.value);
}).change();

// city ajax call

$(".state").change(function () {
   
    $.ajax({
        dataType: "json",
        type: "GET",
        url: "city",
        data: {
            "_token": "{{ csrf_token() }}",
            "state": $('.stateVal').val()
        },
        success: function (data) {
            $(".cityVal option").remove();
            $(data).each(function (i, data) {

                $('.cityVal').append('<option value="' + data.id + '">' + data.name + '</option>');
            });
        }
    });

});

/* Share campaign on facebook */

function shareFunction(id) {
    $.ajax({
        dataType: "json",
        type: "get",
        url: "facebookShare/verify/",
        data:{
            "_token": "{{ csrf_token() }}",
            "id":id
        },
        success: function (data) {
            console.log(data);
            if (data === 1){
                LaunchFeedDialog(data);
                $('#fb'+id).html('<i class="fa fa-facebook"></i> Shared on Facebook').attr('class','btn btn-success').removeAttr('onclick');
            }
            else{
                alert('You Cannot Share This Campaign Right Now please Try Again Later');
            }
        }
    });
    $.ajax({
        dataType: "json",
        type: "GET",
        url: "sharePost",
        data: {
            "_token": "{{ csrf_token() }}",
            "id": id
        },
        success: function (data) {

        }
    });
}
$( document ).ready(function() {

    function $(i_obj) {
        return document.getElementById(i_obj);
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: '555176518557600', // App ID : Insert the APP ID of the APP you created here
            status: true,
        });
    }
  
});
//Function displays the Feed Dialog
function LaunchFeedDialog(data) {
    FB.ui({
        method: 'share',
        href: data.link,
    }, function(response){
        console.log(response);
        console.log(response['post_id']);
    });
}

function callback(response) {
    //Do anything you want here :)
    //document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
    //alert(response['post_id']); Some diagnostics lol :)
}

// Load the SDK Asynchronously. This is a very important part. It loads the Facebook javascript SDK automatically.
(function (d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "https://connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

// change password form

$("#influencerRegisterPasswordChnage").validate({
    rules: {
        old_password: "required",
        new_password: {
            required: true,
            minlength: 8
        },
        retype_password: {
            required: true,
            minlength: 8,
            equalTo: "#new_password"
        }

    },

    messages: {
        old_password: "Please enter your password",
        new_password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 8 characters long"
        },
        retype_password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 8 characters long",
            equalTo: "Please enter the same password as above"
        },

    }
});
