<!DOCTYPE html>
<html>
@include('backend.layouts.head')
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <b>Admin</b> Password Reset
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Enter Your Email To Reset Password</p>
        <form id="adminReset" action="{{ route('admin.password.email') }}" method="post">
            @csrf
            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                       name="email" value="{{ old('email') }}" autofocus placeholder="Email">
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong style="color: red;">{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                @if(session('status'))
                    <span class="invalid-feedback">
                        <strong style="color: forestgreen;">{{ session('status') }}</strong>
                    </span>
                @endif
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
                </div>
            </div>
        </form>
        <br>
        <a href="{{ route('admin.show.LoginForm') }}">I Want To Login</a><br>
        <a href="{{route('admin.register.form')}}" class="text-center">Register a new membership</a>
    </div>
</div>
<script src="{{URL::asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{URL::asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>
</body>
</html>
@include('backend.layouts.validation')
