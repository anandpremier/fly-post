@extends('frontend.layouts.app')
@section('content')
    <div class="bg_color register-form">
        <div class="container">
            <div class="page-wrapper p-t-180 p-b-100 font-robo">
                <div class="wrapper wrapper--w960">

                    <div class="card card-2">
                        <div class="card-heading"></div>
                        <div class="card-body">
                            <h2 class="title">{{ __('YOUR DATA WILL BE KEPT IN CONFIDENCE!') }}</h2>
                            <form method="POST" id="influencerRegister" action="{{ route('influenceruser.update') }}">
                                @csrf
                                @foreach ($errors->all() as $error)
                                    <li class="alert alert-danger">{{ $error }}</li>
                                @endforeach
                                <div class="row">
                                    <div class="col-md-6 padding_add">
                                        <input type="hidden" value="{{$InfluUserget->id}}" name="id">
                                        <input id="firstname" value="{{old('firstname') ? old('firstname') : ( ($InfluUserget->firstname) ? $InfluUserget->firstname : '' )}}" type="text" placeholder="First Name" class="input--style-2 @error('firstname') is-invalid @enderror" placeholder="Firstname" name="firstname"  autocomplete="firstname" autofocus>

                                        @error('firstname')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 padding_add">
                                        <input id="lastname" placeholder="Last Name" type="text" class="input--style-2 @error('lastname') is-invalid @enderror" placeholder="Lastname" name="lastname" value="{{old('lastname') ? old('lastname') : ( ($InfluUserget->lastname) ? $InfluUserget->lastname : '' )}}" autocomplete="lastname">

                                        @error('lastname')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>


                                    <div class="col-md-12 padding_add">

                                        <input id="email" type="email" placeholder="email" class="input--style-2 form-control @error('email') is-invalid @enderror" placeholder="email" name="email" value="{{old('email') ? old('email') : ( ($InfluUserget->email) ? $InfluUserget->email : '' )}}" autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="col-md-6 padding_add">
                                        <input id="datepicker" type="text"  placeholder="Date of Birth" class="input--style-2 form-control @error('date_of_birth') is-invalid @enderror"  value="{{old('date_of_birth') ? old('date_of_birth') : ( ($InfluUserget->date_of_birth) ? $InfluUserget->date_of_birth : '' )}}" name="date_of_birth">

                                        @error('date_of_birth')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror

                                    </div>

                                    <div class="col-md-6 padding_add">
                                        <input id="country" type="text"  placeholder="Country" value="India"  class="input--style-2 form-control @error('country') is-invalid @enderror" name="country" readonly>
                                        @error('country')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="col-md-6 padding_add">
                                        <div class="rs-select2 js-select-simple select--no-search state">
                                            <select name="state" class="stateVal input--style-2" id="state">
                                                <option disabled="disabled" selected="selected">State</option>
                                                @foreach($state as $states)
                                                    @if($InfluUserget->state[0] == $states->id)
                                                        <option value="{{$states->id}}" selected>{{$states->name}}</option>
                                                    @else
                                                        <option value="{{$states->id}}">{{$states->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 padding_add">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <input type="hidden" value="{{csrf_token()}}" id="cp_token">
                                            <select id="city" name="city" class="cityVal input--style-2">
                                                <option disabled="disabled">City</option>
                                                <option selected value="{{$city[0]->id}}">{{$city[0]->name}}</option>
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding_add">
                                        <input id="zipcode" type="text"  placeholder="zipcode" class="input--style-2 form-control @error('zipcode') is-invalid @enderror" value="{{old('zipcode') ? old('zipcode') : ( ($InfluUserget->zipcode) ? $InfluUserget->zipcode : '' )}}" name="zipcode">

                                        @error('zipcode')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror

                                    </div>
                                    <div class="col-md-6 padding_add">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="gender">
                                                <option disabled="disabled" >Gender</option>
                                                @if($InfluUserget->gender)
                                                    <option selected value="1">Male</option>
                                                    <option value="2">Female</option>
                                                @else
                                                    <option value="1">Male</option>
                                                    <option value="2" selected>Female</option>
                                                @endif
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                    <div class="btn-group col-md-6 padding_add">
                                        <button type="button" class="col-md-8 btn btn-primary"><i class="fa fa-facebook"></i> Facebook</button>
                                        @if(in_array(1, $InfluUserget->channel))
                                            <div id="fb_btn">
                                                <button type="button" id="fb_disconnect" style="border-radius: 0 !important;" onclick="deActive_fb()" class="col-md-4 btn btn-danger">Disconnect</button>
                                                <input type="hidden" value="1" name="fb_channel" id="fb_channel">
                                            </div>
                                        @else
                                            <div id="fb_btn">
                                                <button type="button" id="fb_connect" style="border-radius: 0 !important;" onclick="Activate_fb()" class="col-md-4 btn btn-success">Connect</button>
                                                <input type="hidden" name="fb_channel" id="fb_channel">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="btn-group col-md-6 padding_add">
                                        <button type="button" class="col-md-8 btn btn-info"><i class="fa fa-twitter"></i> Twitter</button>
                                        @if(in_array(2, $InfluUserget->channel))
                                            <div id="twitter_btn">
                                                <button type="button" id="twitter_disconnect" style="border-radius: 0 !important;" onclick="deActive_twitter()" class="col-md-4 btn btn-danger">Disconnect</button>
                                                <input type="hidden" value="1" name="twitter_channel" id="twitter_channel">
                                            </div>
                                        @else
                                            <div id="twitter_btn">
                                                <button type="button" id="twitter_connect" style="border-radius: 0 !important;" onclick="Activate_twitter()" class="col-md-4 btn btn-Success">Connect</button>
                                                <input type="hidden" name="twitter_channel" id="twitter_channel">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btnSpace">
                                            {{ __('Submit') }}
                                        </button>
                                        <button type="button" class="btn btn-primary btnSpace" data-toggle="modal" data-target="#myModal">Change Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body" style="float:left;">
                <form method="POST" id="influencerRegisterPasswordChnage" action="{{ route('influenceruser.cp') }}">
                    @csrf
                    <div class="form-group">
                        <input type="hidden" value="{{$InfluUserget->id}}" name="id">
                        <div class="col-md-12 unique">
                            <input id="old_password" name="old_password" type="password"  placeholder="OLD PASSWORD" class="form-control @error('old_password') is-invalid @enderror" autocomplete="old_password">
                            @error('old_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                        </div>

                        <div class="col-md-12 unique">
                            <input id="new_password" name="new_password" type="password"  placeholder="NEW PASSWORD" class="form-control @error('new_password') is-invalid @enderror" autocomplete="new_password">
                            @error('new_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                        </div>
                        <div class="col-md-12 unique">
                            <input id="retype_password" name="retype_password" type="password"  placeholder="RE-TYPE PASSWORD" class="form-control @error('retype_password') is-invalid @enderror" autocomplete="retype_password">
                            @error('retype_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <span role="alert">
                                <strong id="retype_password_error" style="color: red;display: none">Invalid Passwords</strong>
                            </span>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btnSpace">
                                {{ __('Update Password') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
