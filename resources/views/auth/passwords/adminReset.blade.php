<!DOCTYPE html>
<html>
@include('backend.layouts.head')
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <b>Admin</b> Change Password
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Enter Your Password To Reset</p>
        <form id="adminReset" action="{{ route('admin.password.update') }}" method="post">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus readonly>
            </div>

            <div class="form-group has-feedback">
                <input id="password" placeholder="Enter Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert" style="color: red">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group has-feedback">
                <input id="password-confirm" placeholder="Re-Type Password"type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Change Password</button>
                </div>
            </div>
        </form>
        <br>
    </div>
</div>
<script src="{{URL::asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{URL::asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>
</body>
</html>
@include('backend.layouts.validation')
