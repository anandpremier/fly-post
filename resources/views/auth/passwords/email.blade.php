@extends('frontend.layouts.app')

@section('content')

    <div class="bg_color register-form">
        <div class="container">
            <div class="page-wrapper p-t-180 p-b-100 font-robo">
                <div class="wrapper wrapper--w960">
                    <div class="card card-2">
                        <div class="card-heading"></div>
                        <div class="card-body">
                            <h2 class="title">{{ __('ENTER EMAIL ID TO SET PASSWORD!') }}</h2>
                            <form method="POST" id="influencerRegister" action="{{ route('password.email') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12 padding_add">
                                        <input id="email" type="email" placeholder="email" class="input--style-2 form-control @error('email') is-invalid @enderror" placeholder="email" name="email" autocomplete="email"
                                               @if(isset($name[0]))
                                               value="{{old('email') ? old('email') : ( ($getInfo->email) ? $getInfo->email : '' )}}"
                                            @endif
                                        >
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        @error('provider_id')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>Already Registered With This Facebook Account Before!!You Cannot Change Your Email</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btnSpace">
                                            {{ __('Submit') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
