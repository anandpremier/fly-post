<!DOCTYPE html>
<html>
@include('backend.layouts.head')

<head>
    @toastr_css
    <style>
        .unique{
            margin-bottom: 13px;
        }
    </style>
</head>
<body class="hold-transition register-page">
<div class="register-box" style="width: 1000px;margin: 1% auto">
    <div class="register-logo">
        <b>Admin</b> Registration
    </div>
    <div class="register-box-body">
        <p class="login-box-msg">Register a new membership</p>
        <form id="registerForm" enctype="multipart/form-data" action="{{route('admin.register.store')}}" method="post">
            @csrf
            <div class="row unique">
                <div class="col-md-12">
                    <input type="text" id="user_name" value="{{old('user_name')}}" placeholder="Username"class="form-control" name="user_name">
                    @if($errors->has('user_name'))
                        <span id="invalid-feedback" role="alert">
                         <strong id="error" style="color: red">{{ $errors->first('user_name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="row unique">
                <div class="col-md-6">
                    <input type="text" id="firstname" value="{{old('firstname')}}" class="form-control" name="firstname" placeholder="Firstname">
                    @if($errors->has('firstname'))
                        <span id="invalid-feedback" role="alert">
                        <strong id="error" style="color: red">{{ $errors->first('firstname') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <input type="text" id="lastname" value="{{old('lastname')}}" class="form-control" placeholder="Lastname" name="lastname">
                    @if($errors->has('lastname'))
                        <span id="invalid-feedback" role="alert">
                        <strong id="error" style="color: red">{{ $errors->first('lastname') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="row unique">
                <div class="col-md-6">
                    <input type="email" id="email" value="{{old('email')}}" class="form-control" placeholder="email" name="email">
                    @if($errors->has('email'))
                        <span id="invalid-feedback" role="alert">
                        <strong id="error" style="color: red">{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <input type="text" id="skype" value="{{old('skype')}}" class="form-control" name="skype" placeholder="Skype">
                    @if($errors->has('skype'))
                        <span id="invalid-feedback" role="alert">
                        <strong id="error" style="color: red">{{ $errors->first('skype') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="row unique">
                <div class="col-md-6">
                    <input type="text" id="phone" value="{{old('phone')}}" class="form-control" name="phone" placeholder="Phone">
                    @if($errors->has('phone'))
                        <span id="invalid-feedback" role="alert">
                        <strong id="error" style="color: red">{{ $errors->first('phone') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <input type="text" id="mobile" value="{{old('mobile')}}" class="form-control" name="mobile" placeholder="Mobile">
                    @if($errors->has('mobile'))
                        <span id="invalid-feedback" role="alert">
                        <strong id="error" style="color: red">{{ $errors->first('mobile') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="row unique">
                <div class="col-md-6">
                    <input type="password" id="pass" class="form-control" placeholder="Password" name="pass">
                    @if($errors->has('pass'))
                        <span id="invalid-feedback" role="alert">
                        <strong id="error" style="color: red">{{ $errors->first('pass') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <input type="password" id="retype_pass" class="form-control" name="retype_pass" placeholder="Retype password">
                    @if($errors->has('retype_pass'))
                        <span id="invalid-feedback" role="alert">
                        <strong id="error" style="color: red">{{ $errors->first('retype_pass') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <input type="file" id="profile" class="form-control" name="profile">
                </div>
                <div class="col-md-6">
                    <select name="designation" class="form-control">
                        <option disabled selected>SELECT DESIGNATION</option>
                        <option value="1">OVERALL ADMIN</option>
                        <option value="2">CAMPAIGN MANAGER</option>
                        <option value="3">SALES MANAGER</option>
                    </select>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
            </div>
        </form>
        <br>
        <a href="{{route('admin.show.LoginForm')}}" class="text-center">I already have a membership</a>
    </div>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.foot')
@include('backend.layouts.validation')
</body>
</html>

