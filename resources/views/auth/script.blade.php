<script>
    $(document).ready(function () {
        if($('#twitteroAuth').val().length > 0){
            $('#facebookChannel').removeAttr('onclick').attr("disabled", true).html('<i class="fa fa-facebook"></i>&nbsp; Connected');
        }
    });
</script>
<script>
    var facebook;
    var twitter;

    function callFromChild() {
		
        if(typeof facebook != "undefined") {
			facebook.close();
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/getFacebookSession",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                  
                    if(data === "undefined"){
                        alert('SomeOne Has Already Used This Facebook Account Please Select Another Account');
                    }
                    else{
                        $('#facebookoAuth').val(data.id);
                        $('#facebookChannel').html('<i class="fa fa-facebook"></i>&nbsp; Connected').attr('disabled','disabled').removeAttr('onclick');
                    }
                    facebook = undefined;
                }
            });
        }
        else{
            twitter.close();
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/getSession",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                    console.log(data);
                    if(data === "undefined"){
                        alert('SomeOne Has Already Used This Twitter Account Please Connect Another Account');
                    }
                    else{
                        $('#twitteroAuth').val(data.oauth_token);
                        $('#twitterChannel').html('<i class="fa fa-twitter"></i>&nbsp; Connected').attr('disabled', 'disabled').removeAttr('onclick');
                    }
                    twitter = undefined;
                }
            });
        }
    }

    function openTwitter() {
        console.log('hello');
        // $.ajax({
        //         dataType: "json",
        //         type: "GET",
        //         url: "/connect",
        //         data: {
        //             "_token": "{{ csrf_token() }}",
        //         },
        //         success: function (data) {
        //             console.log(data);
        //             if(data === "undefined"){
        //                 alert('SomeOne Has Already Used This Facebook Account Please Select Another Account');
        //             }
        //             else{
        //                 $('#facebookoAuth').val(response.authResponse.userID);
        //                 $('#facebookChannel').html('<i class="fa fa-facebook"></i>&nbsp; Connected').attr('disabled','disabled').removeAttr('onclick');
        //             }
        //             facebook = undefined;
        //         }
        //     });
        twitter = window.open('/connect','','width=700 ,height=400');
    }

    function openFacebook() {

        facebook = window.open('/connectfb','','width=1050,height=750');

        $.ajaxSetup({ cache: true });
         $.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
            FB.init({
                appId: '555176518557600',
                version: 'v2.7' // or v2.1, v2.2, v2.3, ...
            }); 
            FB.login(function(response) {
               console.log(response);
            if (response.status === "connected")
    	    {   
                $.ajax({
                dataType: "json",
                type: "POST",
                url: "/getFacebookuser",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "userID": response.authResponse.userID,
                    "accessToken": response.authResponse.accessToken,
                },
                success: function (data) {
                  
                    if(data === "undefined"){
                        alert('SomeOne Has Already Used This Facebook Account Please Select Another Account');
                    }
                    else{
                        $('#facebookoAuth').val(response.authResponse.userID);
                        $('#facebookChannel').html('<i class="fa fa-facebook"></i>&nbsp; Connected').attr('disabled','disabled').removeAttr('onclick');
                    }
                    facebook = undefined;
                }
            });
                   
            }else{
               // facebook = window.open('/connectfb','','width=1050,height=750');
              // $('#facebookChannel').html('<i class="fa fa-facebook"></i>&nbsp; Connected').attr('disabled','disabled').removeAttr('onclick');
                  
            }
        });
            // $('#loginbutton,#feedbutton').removeAttr('disabled');
            // FB.getLoginStatus(updateStatusCallback);
        });
    }

    function closePanel() {
        opener.callFromChild();
    }
  
</script>

<script>
    var editTwitter;

    function shareTweetPopup(id){
        $.ajax({
            dataType: "json",
            type: "post",
            url: "{{route('twitter.verify')}}",
            data:{
                "_token": "{{ csrf_token() }}",
              "id":id
            },
            success: function (data) {
                console.log(data);
                if (data === 1){
                    var url = "/postTweet/"+id;
                    editTwitter = window.open(url,'','width=400,height=400');
                }
                else{
                    alert('You Cannot Share This Campaign Right Now please Try Again Later');
                }
            }
        });
    }
    function disableShare(){
        if(typeof editTwitter != "undefined") {
            editTwitter.close();
            $.ajax({
                dataType: "json",
                type: "GET",
                url: "{{route('validateTweet')}}",
                success: function (data) {
                    console.log(data);
                    if (data.data === 1)
                        $('#twitter'+data.id).html('<i class="fa fa-twitter"></i> Shared on Twitter').attr('class','btn btn-success').removeAttr('onclick');
                    else
                        alert('Something Wrong Please Try Again later');
                }
            });
        }
    }

    function shareTweet(id) {
        $.ajax({
            dataType: "json",
            type: "GET",
            url: "{{route('shareTweet')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "id":id
            },
            success: function (data) {
                console.log(data);
            }
        });
    }

    function closeSharePopup(){
        opener.disableShare();
    }


</script>

<script>

    var editFacebook;
    var editTwitter;

    function callFromEditChild() {
        if(typeof editFacebook != "undefined") {
            editFacebook.close();
            $.ajax({
                dataType: "json",
                type: "GET",
                url: "{{route('remove_session_fb')}}",
                success: function (data) {
                    console.log(data);
                    if(data === 1)
                        $('#fb_btn').html('<button type="button" id="fb_disconnect" style="border-radius: 0 !important;" onclick="deActive_fb()" class="col-md-4 btn btn-danger">Disconnect</button>');
                    else
                        $('#fb_btn').html('<button type="button" id="fb_connect" style="border-radius: 0 !important;" onclick="Activate_fb()" class="col-md-4 btn btn-success">Connect</button>');
                }
            });
            editFacebook = undefined;
        }
        else{
            editTwitter.close();
            $.ajax({
                dataType: "json",
                type: "GET",
                url: "{{route('remove_session')}}",
                success: function (data) {
                    console.log(data);
                    if(data === 1)
                        $('#twitter_btn').html('<button type="button" id="twitter_disconnect" style="border-radius: 0 !important;" onclick="deActive_twitter()" class="col-md-4 btn btn-danger">Disconnect</button>');
                    else
                        $('#twitter_btn').html('<button type="button" id="twitter_connect" style="border-radius: 0 !important;" onclick="Activate_twitter()" class="col-md-4 btn btn-Success">Connect</button>');
                }
            });
            editTwitter = undefined;
        }
    }

    function Activate_fb(){
        editFacebook = window.open('/add/fb/channel','','width=1050,height=750');
    }

    function Activate_twitter(){
        editTwitter = window.open('/editconnect','','width=400,height=400');
    }

    function deActive_fb(){
        $.ajax({
            dataType: "json",
            type: "GET",
            url: "{{route('remove.fb.channel')}}",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function (data) {
                console.log(data);
                $('#fb_btn').html('<button type="button" id="fb_connect" style="border-radius: 0 !important;" onclick="Activate_fb()" class="col-md-4 btn btn-success">Connect</button>');
            }
        });
    }

    function deActive_twitter(){
        $.ajax({
            dataType: "json",
            type: "GET",
            url: "{{route('remove.twitter.channel')}}",
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function (data) {
                console.log(data);
                $('#twitter_btn').html('<button type="button" id="twitter_connect" style="border-radius: 0 !important;" onclick="Activate_twitter()" class="col-md-4 btn btn-Success">Connect</button>');
            }
        });
    }

    function closeEditPanel() {
        opener.callFromEditChild();
    }

function CloseTwitterPopup(objWindow, strMessage, twitterPostId, channelName) {

    
    // Close the popup (or whatever)
   console.log(twitterPostId);
   console.log(channelName);
   console.log(objWindow);
   console.log(strMessage);
    objWindow.close();
    //objWindow.resizeTo(30, 30);

    // Schedule our `finish` call to happen *almost* instantly;
    // this lets the browser do some UI work and then call us back
    setTimeout(finish, 0); // It won't really be 0ms, most browsers will do 10ms or so

    // Our `finish` call
    function finish() {
        if (strMessage == 'success' && strMessage != '') {
            $('#twitteroAuth').val(twitterPostId);
            $('#twitterChannel').html('<i class="fa fa-twitter"></i>&nbsp; Connected').attr('disabled','disabled').removeAttr('onclick');
        } else {
            alert('Some One Has Already Used This Twitter Account Please Select Another Account');
        }
    }
}
</script>

@if(Session::has('twitter_error') || Session::has('facebook_error'))
    <script>
        window.close();
    </script>
@endif

@if(Session::has('share_tweet_status'))
    <script>
        closeSharePopup();
    </script>
@endif

@if(Session::has('access_token') || Session::has('facebook_info'))
    <script>
        closePanel();
    </script>
@endif

@if(Session::has('edit_twitter_channel') || Session::has('edit_facebook'))
    <script>
        closeEditPanel();
    </script>
@endif
