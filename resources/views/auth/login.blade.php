@extends('frontend.layouts.app')

@section('content')
    <div class="bg_color login-form">
        <div class="container">
            <div class="page-wrapper p-t-180 p-b-100 font-robo">
                <div class="wrapper wrapper--w960">

                    <div class="card card-2">
                        <div class="card-heading"></div>
                        <div class="card-body">
                            @if(isset($statusAdmin))
                                <div class="col-md-12 alert alert-danger">
                                    {!! $statusAdmin !!}
                                </div>
                            @endif
                            @if(\Illuminate\Support\Facades\Session::has('successEmailVerify'))
                                <div class="col-md-12 alert alert-success">
                                    {{\Illuminate\Support\Facades\Session::get('successEmailVerify')}}
                                    {{\Illuminate\Support\Facades\Session::flush()}}
                                </div>
                            @endif
                            <div class="col-md-12"><h2 class="title">{{ __('Login') }}</h2></div>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="col-md-12">
                                    <input id="email" type="email" class="input--style-2 form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-12 unique">
                                    <input id="password" type="password" class="input--style-2 form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-12 unique">
                                    <div class="form-check">
                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" style="position: absolute;left: -75px;">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                                <div class="col-md-12" style="margin-left: 173px;">
                                    OR
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 unique">
                                        <a href="{{url('/redirect')}}" class="btn btn-primary btn-block"><i class="fa fa-facebook fa-fw"></i> Continue with Facebook</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
