@extends('frontend.layouts.app')
@section('content')
    <div class="bg_color register-form">
        <div class="container">
            <div class="page-wrapper p-t-180 p-b-100 font-robo">
                <div class="wrapper wrapper--w960">
                    <div class="card card-2">
                        <div class="card-heading"></div>
                        <div class="card-body">
                            <h2 class="title">{{ __('YOUR DATA WILL BE KEPT IN CONFIDENCE!') }}</h2>
                            <form method="POST" id="influencerRegister" action="{{ route('register.influencer.user') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6 padding_add">
                                        @if(isset($getInfo->email))
                                            <input value="{{$getInfo->id}}" name="channel_userid" id="channel_userid" type="hidden">
                                            <input value="{{$getInfo->token}}" name="channel_accesstoken" id="channel_accesstoken" type="hidden">
                                            <input type="hidden" value="{{$getInfo->id}}" name="twitteroAuth" id="twitteroAuth">
                                        @else
                                            <input type="hidden" name="twitteroAuth" id="twitteroAuth">
                                            <input type="hidden" name="facebookoAuth" id="facebookoAuth">
                                        @endif
                                        <input id="firstname" type="text" placeholder="First Name" class="input--style-2 @error('firstname') is-invalid @enderror" placeholder="Firstname" name="firstname" autocomplete="firstname" autofocus
                                               @if(isset($name[0]))
                                               value="{{old('firstname') ? old('firstname') : ( ($name[0]) ? $name[0] : '' )}}"
                                            @endif>
                                        @error('firstname')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 padding_add">
                                        <input id="lastname" placeholder="Last Name" type="text" class="input--style-2 @error('lastname') is-invalid @enderror" placeholder="Lastname" name="lastname"
                                               @if(isset($name[1]))
                                               value="{{old('lastname') ? old('lastname') : ( ($name[1]) ? $name[1] : '' )}}"
                                               @endif

                                               autocomplete="lastname">


                                        @error('lastname')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="col-md-12 padding_add">
                                        <input id="email" type="email" placeholder="email" class="input--style-2 form-control @error('email') is-invalid @enderror" placeholder="email" name="email" autocomplete="email"
                                               @if(isset($name[0]))
                                               value="{{old('email') ? old('email') : ( ($getInfo->email) ? $getInfo->email : '' )}}"
                                            @endif
                                        >
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                        @error('provider_id')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>Already Registered With This Facebook Account Before!!You Cannot Change Your Email</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 padding_add">
                                        <input id="password" type="password" class="input--style-2 form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 padding_add">
                                        <input id="password_confirmation" type="password" class="input--style-2 form-control" placeholder="Password Confirmation" name="password_confirmation" autocomplete="new-password">
                                    </div>
                                    <div class="col-md-6 padding_add">
                                        <input id="datepicker" type="text"  placeholder="Date of Birth" class="input--style-2 form-control @error('date_of_birth') is-invalid @enderror"  name="date_of_birth">

                                        @error('date_of_birth')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 padding_add">
                                        <input id="country" type="text"  placeholder="Country" value="India"  class="input--style-2 form-control @error('country') is-invalid @enderror" name="country" readonly>
                                        @error('country')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 padding_add stateDrop">

                                        <div class="state">
                                            <select name="state" class="stateVal input--style-2">
                                                <option disabled="disabled" selected="selected">State</option>
                                                @foreach($statusList as $statusListData)
                                                    <option value="{{ $statusListData->id }}">{{ $statusListData->name }}</option>
                                                @endforeach
                                            </select>

                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding_add cityDrop">
                                        <div>
                                            <select name="city"  class="cityVal input--style-2">

                                                <option disabled="disabled" selected="selected">City</option>

                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding_add">
                                        <input id="zipcode" type="text"  placeholder="zipcode" class="input--style-2 form-control @error('zipcode') is-invalid @enderror" name="zipcode">
                                        @error('zipcode')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6 padding_add genderDrop">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="gender">
                                                <option disabled="disabled" selected="selected">Gender</option>
                                                <option value="1">Male</option>
                                                <option value="0">Female</option>
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 padding_add">
                                        <a id="facebookChannel" onclick="openFacebook()" class="btn btn-primary"><i class="fa fa-facebook"></i>&nbsp; Connect</a>
                                        <a id="twitterChannel" onclick="openTwitter()"  class="btn btn-info"><i class="fa fa-twitter"></i>&nbsp; Connect</a>
  </div>
                                    <div class="col-md-12">
                                        <div class="form-check">
                                            <label class="form-check-label" for="exampleCheck">I accept the  Terms and Conditions</label>
                                            <input type="checkbox" value="0" name="terms_and_conditions" class="form-check-input" id="exampleCheck" style="position: absolute;left: -41px;">
                                        </div>
                                        <div class="form-check">
                                            <label class="privacy form-check-label" for="exampleCheck1" id="labelPrivacy">I accept the privacy policy</label>
                                            <input type="checkbox" value="0" name="privacy_policy" class="form-check-input" id="exampleCheck1" style="position: absolute;left: -96px;">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btnSpace">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

