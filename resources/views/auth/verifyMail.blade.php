@extends('frontend.layouts.app')

@section('content')

    <div class="bg_color register-form">
        <div class="container">
            <div class="page-wrapper p-t-180 p-b-100 font-robo">
                <div class="wrapper wrapper--w960">
                    <div class="card card-2">
                        <div class="card-heading"></div>
                        <div class="card-body">
                            <h2 class="title">
                                Password Verification Link Has Been Sent To Your Email Id !!<br><br> Please Verify Your Email To Enjoy Services
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
