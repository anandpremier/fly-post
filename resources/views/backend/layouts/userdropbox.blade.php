<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="{{ URL::asset('dash/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
        <span class="hidden-xs">{{ Auth::guard('admin')->user()->email}}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="user-header">
            <img src="{{ URL::asset('dash/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            <p>
                {{ Auth::guard('admin')->user()->firstname}} {{ Auth::guard('admin')->user()->lastname}} - Admin
                <small>Member since Nov. 2019</small>
            </p>
        </li>
        {{--<li class="user-body">
            <div class="row">
                <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                </div>
            </div>
        </li>--}}
        <li class="user-footer">
            <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">
                    <span class="glyphicon glyphicon-user"></span> Profile
                </a>
            </div>
            <div class="pull-right">
                <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                    <span class="glyphicon glyphicon-log-out"></span>
                    Sign out
                </a>
                <form id="logout-form" action="{{ route('admin.user.logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</li>
