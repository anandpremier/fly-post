<!DOCTYPE html>
<html>
@include('backend.layouts.headercdn')
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="{{route('admin.dashboard')}}" class="logo">
            <span class="logo-mini"><b>F-P</b></span>
            <span class="logo-lg"><b>FLY-POST</b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    {{--@include('backend.layouts.message')--}}
                    {{--@include('backend.layouts.notification')--}}
                    @include('backend.layouts.task')
                    @include('backend.layouts.userdropbox')
                    {{--<li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>--}}
                </ul>
            </div>
        </nav>
    </header>
@include('backend.layouts.navigation')
