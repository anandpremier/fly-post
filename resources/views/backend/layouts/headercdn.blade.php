<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ URL::asset('dash/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dash/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dash/bower_components/Ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dash/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dash/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dash/bower_components/morris.js/morris.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dash/bower_components/jvectormap/jquery-jvectormap.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dash/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dash/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dash/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- Toastrr For Notification -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <!-- Css And Js For Toastr -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.semanticui.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/accordion.min.css" integrity="sha256-UHeBVR0fprbNitzKPhdn/I/0NP+tlqNmcNbPqgluZuM=" crossorigin="anonymous" />
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>-->
    <![endif]-->

    <!-- Google Font -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .error {
            color: red;
        }
        #example_length{
            margin-left: 20px;
        }
        #example_info{
            margin-left: 20px;
        }
        .example_filter{
            padding-right: 5px;
        }
        #example{
            margin-left: 20px;
            width: 98.4%;
        }
        .right.aligned.eight.wide.column {
            width: 50% !important;
            float: right;
            right: 0;
            text-align: right;
        }
        .ui.basic.buttons {
            -webkit-box-shadow: none;
            box-shadow: none;
            border: 1px solid rgba(34,36,38,.15);
            border-radius: .28571429rem;
            margin-left: 20px;
        }
        .inputSpace{
            margin-bottom: 10px;
        }
        .dropdown-menu.open {
            height: 150px !important;
            overflow: scroll !important;
        }
        select.email_filed_text {
            width: 100%;
            height: 250px;
        }

    </style>
</head>
