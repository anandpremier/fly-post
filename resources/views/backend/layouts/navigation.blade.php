<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ URL::asset('dash/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::guard('admin')->user()->username}} </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <form class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat" disabled>
                    <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Users</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.get.admin')}}"><i class="fa fa-circle-o text-yellow"></i>List Admin</a></li>
                    <li><a href="{{route('admin.getInfluencers')}}"><i class="fa fa-circle-o text-yellow"></i>List Influencers</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Campaign Management</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.get.campaign')}}"><i class="fa fa-circle-o text-yellow"></i>List Campaign</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Content Management</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.get.country')}}"><i class="fa fa-circle-o text-yellow"></i>Country</a></li>
                    {{--<li><a href="{{route('admin.email.index')}}"><i class="fa fa-circle-o text-yellow"></i>E-Mail's</a></li>--}}
                    <li><a href="{{route('admin.mail.get')}}"><i class="fa fa-circle-o text-yellow"></i>Mail Template</a></li>
                    <li><a href="{{route('admin.faq.get')}}"><i class="fa fa-circle-o text-yellow"></i>FAQ's</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>
