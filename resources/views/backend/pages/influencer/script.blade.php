<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function  fetchCities(){
        var id = $('#state').val();
        var url = "{{route('admin.fetch.cities', ":id")}}";
        url = url.replace(':id', id);
        $.ajax({
            dataType: "json",
            type: "POST",
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id,
            },
            success: function (data) {
                $('#cities').html(data);
            }
        });
    }
</script>
<script>
    function  changeStatus(id){
        var url = "{{route('admin.update.status.influencers', ":id")}}";
        url = url.replace(':id', id);
        if($('#'+id). prop("checked") === true)
            var status = 1;
        else
            var status = 0;

        $.ajax({
            dataType: "json",
            type: "POST",
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id,
                "status":status
            },
            success: function (data) {
                /*$("#example").DataTable().ajax.reload();*/
            }
        });
    }
</script>
<script>
    function deleteInfluencer(input) {
        var id = input;
        var url = "{{route('admin.destroy.influencers', ":id")}}";
        url = url.replace(':id', id);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Influencer!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id,
                    },
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });
    }
</script>
<script>
    $(document).ready(function () {
        $("#influencerRegister").validate({
            rules:{
                firstname:{
                    required:true,
                },
                lastname:{
                    required:true
                },
                email:{
                    required:true,
                    email:true,
                },
                password:{
                    required:true,
                    minlength:8
                },
                password_confirmation:{
                    equalTo:"#password",
                    required:true,
                    minlength:8
                },
                date_of_birth:{
                    required:true,
                    date:true,
                },
                country:{
                    required:true
                },
                zipcode:{
                    required:true,
                    integer:true,
                },
                gender:{
                    required:true,
                    integer:true,
                },
                state:{
                    required:true,
                    integer:true,
                },
                cities:{
                    required:true,
                    integer:true
                }
            },
            messages:{
                firstname:{
                    required:"Please Enter First Name",
                },
                lastname:{
                    required:"Please Enter Last Name"
                },
                email:{
                    required:"Please Enter Your Email",
                    email:"Please Enter Valid Email To Register"
                },
                password:{
                    required:"Please Enter Password",
                    minlength:"Password Length Must Be 8 Charachters"
                },
                password_confirmation:{
                    required:"Please Enter Password",
                    minlength:"Password Length Must Be 8 Charachters",
                    equalTo:"Confirm Password Must Be Equal To Password"
                },
                date_of_birth:{
                    required:"Please Enter Date Of Birth",
                    date:"Date Is Invalid",
                },
                country:{
                    required:"Please Select Country Name"
                },
                zipcode:{
                    required:"Please Enter Zip-Code",
                    integer:"Please Enter Valid Zip-Code",
                },
                gender:{
                    required:"Please Select Gender",
                    integer:"Invalid Selection",
                },
                state:{
                    required:"Please Select Proper State",
                    integer:"Please Select Proper State",
                },
                cities:{
                    required:"Please Select Proper City",
                    integer:"Please Select Proper City",
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#influencerEdit").validate({
            rules:{
                firstname:{
                    required:true,
                },
                lastname:{
                    required:true
                },
                email:{
                    required:true,
                    email:true,
                },
                date_of_birth:{
                    required:true,
                    date:true,
                },
                country:{
                    required:true
                },
                zipcode:{
                    required:true,
                    integer:true,
                },
                state:{
                    required:true,
                    integer:true,
                },
                cities:{
                    required:true,
                    integer:true,
                },
                profile_url:{
                    url: true,
                },
                facebook:{
                    required:true,
                    integer:true,
                },
                twitter:{
                    required:true,
                    integer:true,
                },
                bookDesc:{
                    required:true,
                },
                descTwitter:{
                    required:true,
                },
            },
            messages:{
                firstname:{
                    required:"Please Enter First Name",
                },
                lastname:{
                    required:"Please Enter Last Name"
                },
                email:{
                    required:"Please Enter Email",
                    email:"Please Enter Valid Email To Register"
                },
                date_of_birth:{
                    required:"Please Select Date Of Birth",
                    date:"Date Is Invalid",
                },
                country:{
                    required:"Please Enter Country Name"
                },
                zipcode:{
                    required:"Please Enter Zip-Code",
                    integer:"Please Enter Valid Zip-Code",
                },
                state:{
                    required:"Please Select State",
                    integer:"Please Enter Valid State",
                },
                cities:{
                    required:"Please Select City",
                    integer:"Please Enter Valid City",
                },
                profile_url:{
                    url: "Please Insert Proper Url",
                },
                facebook:{
                    required:"Please Select Approval Status",
                    integer:"Invalid Selection of Approval Status",
                },
                twitter:{
                    required:"Please Select Approval Status",
                    integer:"Invalid Selection of Approval Status",
                },
                bookDesc:{
                    required:"Please Enter Reason For DisApproving",
                },
                descTwitter:{
                    required:"Please Enter Reason For DisApproving"
                },
            },
        });
    });
</script>

<script>
    $(document).ready(function () {
        if($('#facebook').val() === "0")
            $('#facebookDiv').attr('style','display:show');
        else
            $('#facebookDiv').attr('style','display:none');

        if($('#twitter').val() === "0")
            $('#twitterDiv').attr('style','display:show');
        else
            $('#twitterDiv').attr('style','display:none');
    });

    $('#facebook').on('change',function () {
        if($('#facebook').val() === "0")
            $('#facebookDiv').attr('style','display:show');
        else
            $('#facebookDiv').attr('style','display:none');
    });
    $('#twitter').on('change',function () {
        if($('#twitter').val() === "0")
            $('#twitterDiv').attr('style','display:show');
        else
            $('#twitterDiv').attr('style','display:none');
    });
</script>
