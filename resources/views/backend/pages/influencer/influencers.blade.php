<title>Influencers List</title>
<head>
    <link rel="stylesheet" href="{{ URL::asset('css/switch.css') }}">
</head>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Influencers
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Influencers</li>
            <li class="active"><a href="{{route('admin.getInfluencers')}}">List Influencers</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List Of All Influencers </h3>
                    <a class="btn btn-primary pull-right" href="{{route('admin.create.influencers')}}" style="margin-right: 28px;">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add New Influencer
                    </a>
                </div>
                <div class="box-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>FirstName</th>
                                <th>LastName</th>
                                <th>Email</th>
                                <th>Date Of Birth</th>
                                <th>Gender</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($obj as $k=> $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->firstname}}</td>
                                    <td>{{$data->lastname}}</td>
                                    <td>{{$data->email}}</td>
                                    <td>{{$data->date_of_birth}}</td>
                                    <td>
                                        @if($data->gender)
                                            Male
                                        @else
                                            Female
                                        @endif
                                    </td>
                                    <td>
                                        @if($data->status)
                                            <label class="switch">
                                                <input type="checkbox" onclick="changeStatus({{$data->id}})" id="{{$data->id}}" checked>
                                                <span class="slider round"></span>
                                            </label>
                                        @else
                                            <label class="switch">
                                                <input type="checkbox" onclick="changeStatus({{$data->id}})" id="{{$data->id}}">
                                                <span class="slider round"></span>
                                            </label>
                                        @endif
                                    </td>
                                    <td>
                                        <a  href="{{route('admin.history.influencers',$data->id)}}">
                                            <i class="fa fa-inr" style="font-size:24px" title="history"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a  href="{{route('admin.edit.influencers',$data->id)}}">
                                            <i class="fa fa-edit" style="font-size:24px;color: green;" title="Edit"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a onclick="deleteInfluencer({{$data->id}})" data-id="{{$data->id}}">
                                            <i class="fa fa-trash" style="cursor:pointer;font-size:24px;color: red;" title="Delete"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            {{--<tfoot>
                            <tr>
                                <th>Id</th>
                                <th>FirstName</th>
                                <th>LastName</th>
                                <th>Email</th>
                                <th>Date Of Birth</th>
                                <th>Gender</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>--}}
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.influencer.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
        } );
        table.buttons().container()
            .appendTo( $('div.eight.column:eq(0)', table.table().container()) );
    } );
</script>


