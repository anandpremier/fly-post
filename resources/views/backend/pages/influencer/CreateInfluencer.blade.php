<title>Add Influencer</title>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Influencer Registration Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Generate Information</li>
            <li class="active"><a href="{{--{{route('admin.create.paper')}}--}}">Register Influencer</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details Of The Influencer</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form class="form-horizontal" method="POST" id="influencerRegister" action="{{route('admin.register.influencers')}}">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">FirstName:</label>
                                <input id="editfirstname" type="text" placeholder="First Name"
                                       class="form-control @error('firstname') is-invalid @enderror" name="firstname"
                                       value="{{ old('firstname') }}" autocomplete="firstname"
                                       autofocus>
                                @if($errors->has('firstname'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">LastName:</label>
                                <input id="editlastname" placeholder="Last Name" type="text"
                                       class="form-control @error('lastname') is-invalid @enderror" name="lastname"
                                       value="{{ old('lastname') }}" autocomplete="lastname">
                                @if($errors->has('lastname'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-12" style="margin-top: 17px;">
                                <label class="col-sm-2control-label">Email:</label>
                                <input id="email" type="email" placeholder="email"
                                       class="form-control @error('email') is-invalid @enderror" name="email"
                                       value="{{ old('email') }}" autocomplete="email">
                                @if($errors->has('email'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">Password:</label>
                                <input id="password" type="password" placeholder="Password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       autocomplete="new-password">
                                @if($errors->has('password'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">Confirm Password:</label>
                                <input id="password_confirmation" type="password" class="form-control"
                                       placeholder="Password Confirmation" name="password_confirmation"
                                       autocomplete="new-password">
                                @if($errors->has('password_confirmation'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error"
                                                style="color: red">{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">Country:</label>
                                <input id="country" class="form-control @error('country') is-invalid @enderror" name="country"
                                       style="text-transform:uppercase" value="India" readonly placeholder="India">
                                @if($errors->has('country'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('country') }}</strong>
                                 </span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">ZipCode:</label>
                                <input id="zipcode" type="text" placeholder="zipcode"
                                       class="form-control @error('zipcode') is-invalid @enderror" name="zipcode">
                                @if($errors->has('zipcode'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error"
                                                style="color: red">{{ $errors->first('zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">State:</label>
                                <select id="state" class="form-control @error('country') is-invalid @enderror" name="state"
                                        onchange="fetchCities()" style="text-transform:uppercase">
                                    <option selected disabled>Select State</option>
                                    @foreach($country as $cnt)
                                        <option value="{{$cnt['id']}}">{{$cnt['name']}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('country'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('country') }}</strong>
                                 </span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">City:</label>
                                <select id="cities"
                                        class="form-control @error('zipcode') is-invalid @enderror" name="cities" style="text-transform:uppercase">
                                    <option selected disabled>---SELECT CITIES---</option>
                                </select>
                                @if($errors->has('zipcode'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error"
                                                style="color: red">{{ $errors->first('zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">Date Of Birth:</label>
                                <input id="datepicker" type="date" placeholder="Date of Birth"
                                       class="form-control @error('date_of_birth') is-invalid @enderror"
                                       name="date_of_birth">
                                @if($errors->has('date_of_birth'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('date_of_birth') }}</strong>
                                 </span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">Gender:</label>
                                <select name="gender" class="form-control">
                                    <option selected disabled>Select Gender</option>
                                    <option value="1">Male</option>
                                    <option value="0">Female</option>
                                </select>
                                @if($errors->has('gender'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('gender') }}</strong>
                                 </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="checkbox" name="facebook[]"
                                       class="form-check-input" id="facebook" value="1">
                                <label class="form-check-label" for="facebook"> Facebook</label>
                                @if($errors->has('facebook'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('facebook') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="facebook[]" class="form-check-input"
                                       id="twitter" value="2">
                                <label class="form-check-label" for="twitter">
                                    Twitter</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="checkbox" name="terms_and_conditions"
                                       class="form-check-input " id="exampleCheck">
                                <label class="form-check-label " for="exampleCheck1">I accept the Terms and
                                    Conditions</label>
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" name="privacy_policy" class="form-check-input"
                                       id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1"> I accept the privacy
                                    policy</label>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Add Influencer">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.influencer.script')
