<title>Influencer History</title>
@include('backend.layouts.app')
<style>
    label{
        font-size: 22px;
    }
    .unique{
        margin-top: 18px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Influencer History
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>List Influencer</li>
            <li class="active"><a onclick="location.reload()" style="cursor: pointer">Influencer History</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Details Of The Influencer</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <div class="box-body">
                    @if(isset($influencer))
                        @foreach($influencer as $data)
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Email</th>
                                        <th>BirthDate</th>
                                        <th>Gender:</th>
                                        <th>State</th>
                                        <th>City</th>
                                        <th>Total Remuneration</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{$data->firstname}}</td>
                                        <td>{{$data->lastname}}</td>
                                        <td>{{$data->email}}</td>
                                        <td>{{$data->date_of_birth}}</td>
                                        <td>
                                            @if($data->gender == "1")
                                                Male
                                            @else
                                                Female
                                            @endif
                                        </td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->city_name}}</td>
                                        <td id="totalRemu"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            @break
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="box box-info">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Post Shared On Facebook</h3>
                    </div>
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Campaign Name</th>
                                <th>Campaign Title</th>
                                <th>Landing Url</th>
                                <th>Remuneration</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=0;?>
                            @foreach($influencerFacebookShare as $data)
                                @if(isset($data->name))
                                    <tr>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->title}}</td>
                                        <td>
                                            <a href="{{$data->post_url_fb}}" target="_blank">
                                                {{$data->post_url_fb}}
                                            </a>
                                        </td>
                                        <td>{{$data->remuneration}}</td>
                                        <?php $i = $i + $data->remuneration ?>
                                    </tr>
                                @else

                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box box-info">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Post Shared On Twitter</h3>
                    </div>
                    <div class="table-responsive data-table-wrapper">
                        <table id="example1" style="margin-left: 23px" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Campaign Name</th>
                                <th>Campaign Title</th>
                                <th>Landing Url</th>
                                <th>Remuneration</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($influencerTwitterShare as $data)
                                @if(isset($data->name))
                                    <tr>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->title}}</td>
                                        <td>
                                            <a href="{{$data->post_url_twitter}}" target="_blank">
                                                {{$data->post_url_twitter}}
                                            </a>
                                        </td>
                                        <td>{{$data->remuneration}}</td>
                                        <?php $i = $i + $data->remuneration ?>
                                    </tr>
                                @else

                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" value="{{$i}}" id="total">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.influencer.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable(
            {searching: false,"bLengthChange": false,"bInfo": false,"bPaginate": false,}
        );
    } );

</script>

<script>
    $(document).ready(function() {
        var table = $('#example1').DataTable(
            {searching: false,"bLengthChange": false,"bInfo": false,"bPaginate": false,}
        );
        $('#totalRemu').html($('#total').val());
    } );
</script>
