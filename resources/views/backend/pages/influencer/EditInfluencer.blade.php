<title>Edit Influencer</title>
@include('backend.layouts.app')
<style>
    .unique{
        margin-top: 5px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Edit Influencer Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Generate Information</li>
            <li class="active"><a onclick="location.reload()" style="cursor: pointer">Edit Influencer</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Details Of The Influencer</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form class="form-horizontal" method="POST" id="influencerEdit" action="{{route('admin.register.influencers')}}">
                    @csrf
                    <input type="hidden" value="{{$obj->id}}" name="editid">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="emailVerify" class="col-sm-3control-label">Email Verification:</label>
                                @if($obj->email_verify)
                                    <input type="text" id="emailVerify" class="col-sm-9 form-control" disabled="disabled" readonly value="Verified" style="color: green">
                                @else
                                    <input type="text" id="emailVerify" class="col-sm-9 form-control" disabled="disabled" readonly value="Pending [Not-Verified]" style="color: red">
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="status" class="col-sm-3control-label">Status:</label>
                                <select id="status" name="status" class="col-sm-9 form-control">
                                    @if($obj->status)
                                        <option value="1" selected>Approve</option>
                                        <option value="0">InApprove</option>
                                    @else
                                        <option value="1">Approve</option>
                                        <option value="0" selected>InApprove</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">FirstName:</label>
                                <input id="editfirstname" type="text" placeholder="First Name"
                                       class="col-sm-4 form-control @error('firstname') is-invalid @enderror" name="firstname"
                                       @if(isset($obj->firstname))
                                       value="{{old('firstname') ? old('firstname') : ( ($obj->firstname) ? $obj->firstname : '' )}}"
                                       @endif autocomplete="firstname"
                                       autofocus disabled>
                                @if($errors->has('firstname'))
                                    <span id="invalid-feedback" role="alert">
                                        <br><strong id="error" style="color: red">{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">LastName:</label>
                                <input id="editlastname" placeholder="Last Name" type="text"
                                       class="col-sm-4 form-control @error('lastname') is-invalid @enderror" name="lastname"
                                       @if(isset($obj->lastname))
                                       value="{{old('lastname') ? old('lastname') : ( ($obj->lastname) ? $obj->lastname : '' )}}"
                                       @endif
                                       autocomplete="lastname" disabled>
                                @if($errors->has('lastname'))
                                    <span id="invalid-feedback" role="alert">
                                        <br><strong id="error" style="color: red">{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="col-sm-2control-label">Email:</label>
                                <input id="email" type="email" placeholder="email"
                                       class="form-control @error('email') is-invalid @enderror" name="email"
                                       @if(isset($obj->email))
                                       value="{{old('email') ? old('email') : ( ($obj->email) ? $obj->email : '' )}}"
                                       @endif
                                       autocomplete="email" disabled>
                                @if($errors->has('email'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">Country:</label>
                                <input id="country" class="form-control unique @error('country') is-invalid @enderror" name="country"
                                       value="India" placeholder="India" disabled>
                                @if($errors->has('country'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('country') }}</strong>
                                 </span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">ZipCode:</label>
                                <input id="zipcode" type="text" placeholder="zipcode"
                                       @if(isset($obj->zipcode))
                                       value="{{old('zipcode') ? old('zipcode') : ( ($obj->zipcode) ? $obj->zipcode : '' )}}"
                                       @endif
                                       class="form-control unique @error('zipcode') is-invalid @enderror" name="zipcode" disabled>
                                @if($errors->has('zipcode'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">State:</label>
                                <select id="state" class="form-control @error('state') is-invalid @enderror" name="state"
                                        onchange="fetchCities()" style="text-transform:uppercase" disabled>
                                    <option disabled>Select State</option>
                                    @foreach($country as $cnt)
                                        @if($cnt['id'] == $obj->state[0])
                                            <option value="{{$cnt['id']}}" selected>{{$cnt['name']}}</option>
                                        @else
                                            <option value="{{$cnt['id']}}">{{$cnt['name']}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if($errors->has('state'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('state') }}</strong>
                                 </span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">City:</label>
                                <select id="cities"
                                        class="form-control @error('cities') is-invalid @enderror" name="cities" style="text-transform:uppercase" disabled>
                                    <option disabled>---SELECT CITIES---</option>
                                    <option value="{{$cities[0]->id}}" selected>{{$cities[0]->name}}</option>
                                </select>
                                @if($errors->has('zipcode'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error"
                                                style="color: red">{{ $errors->first('zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">Date Of Birth:</label>
                                <input id="datepicker" type="date" placeholder="Date of Birth"
                                       class="form-control @error('date_of_birth') is-invalid @enderror"
                                       @if(isset($obj->date_of_birth))
                                       value="{{old('date_of_birth') ? old('date_of_birth') : ( ($obj->date_of_birth) ? $obj->date_of_birth : '' ) }}"
                                       @endif
                                       name="date_of_birth" disabled>
                                @if($errors->has('date_of_birth'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('date_of_birth') }}</strong>
                                 </span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label class="col-sm-2control-label">Gender:</label>
                                <select name="gender" class="form-control" disabled>
                                    @if($obj->gender)
                                        <option disabled>Select Gender</option>
                                        <option value="1" selected>Male</option>
                                        <option value="0">Female</option>
                                    @elseif($obj->gender == 0 && $obj->gender != '')
                                        <option disabled>Select Gender</option>
                                        <option value="1">Male</option>
                                        <option value="0" selected>Female</option>
                                    @else
                                        <option selected disabled>Select Gender</option>
                                        <option value="1">Male</option>
                                        <option value="0">Female</option>
                                    @endif
                                </select>
                                @if($errors->has('gender'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('gender') }}</strong>
                                 </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="col-sm-2control-label">Profile:</label>
                                <a target="_blank" href="https://www.facebook.com/search/people/?q={{$obj->firstname}} {{$obj->lastname}}" style="cursor: pointer;">
                                    <img class="form-control" style="border-radius:10px;height: 193px;" src="{{asset('/profileImages/').'/'. $obj->provider_user_id.'.jpg'}}">
                                </a>
                            </div>
                            <div class="col-md-9">
                                <label class="col-sm-2control-label">ProfileUrl:</label>
                                <input class="form-control unique" name="profile_url" placeholder="Enter Profile Url Here">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    @if(isset($obj->channel[0]) && !empty($obj->channel[0]))
                                        @if($obj->channel[0] == 1)
                                            <button type="button" class="btn btn-primary btn-block"><i class="fa fa-facebook"></i> Conntected</button>
                                        @else
                                            <button type="button" class="btn btn-primary btn-block"><i class="fa fa-facebook"></i> Not-Conntected</button>
                                        @endif
                                    @else
                                        <button type="button" class="btn btn-primary btn-block"><i class="fa fa-facebook"></i> Not-Conntected</button>
                                    @endif
                                </div>
                                @if(isset($obj->channel[0]) && !empty($obj->channel[0]))
                                    @if($obj->channel[0] == 1)
                                        <div class="col-md-9">
                                            <select class="form-control" id="facebook" name="facebook">
                                                <option>--Select Approval For Facebook Channel</option>
                                                @if(isset($facebook))
                                                    @if($facebook)
                                                        <option value="1" selected>Approve</option>
                                                        <option value="0">DisApprove</option>
                                                    @else
                                                        <option value="1">Approve</option>
                                                        <option value="0" selected>DisApprove</option>
                                                    @endif
                                                @else
                                                    <option value="1">Approve</option>
                                                    <option value="0">DisApprove</option>
                                                @endif
                                            </select>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-12" id="facebookDiv" style="display: none">
                                    <textarea class="form-control" id="bookDesc" name="bookDesc"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    @if(isset($obj->channel[0]))
                                        @if(isset($obj->channel[1]) && $obj->channel[1] == 2)
                                            <button type="button" class="btn btn-info btn-block"><i class="fa fa-twitter"></i> Conntected</button>
                                        @elseif($obj->channel[0] == 2)
                                            <button type="button" class="btn btn-info btn-block"><i class="fa fa-twitter"></i> Conntected</button>
                                        @else
                                            <button type="button" class="btn btn-info btn-block"><i class="fa fa-twitter"></i> Not-Conntected</button>
                                        @endif
                                    @else
                                        <button type="button" class="btn btn-info btn-block"><i class="fa fa-twitter"></i> Not-Conntected</button>
                                    @endif
                                </div>
                                @if(isset($obj->channel[0]))
                                    @if((isset($obj->channel[1]) && $obj->channel[1] == 2) || $obj->channel[0] == 2)
                                        <div class="col-md-9">
                                            <select class="form-control" id="twitter" name="twitter">
                                                <option>--Select Approval For Twitter Channel</option>
                                                @if(isset($twitter))
                                                    @if($twitter)
                                                        <option value="1" selected>Approve</option>
                                                        <option value="0">DisApprove</option>
                                                    @else
                                                        <option value="1">Approve</option>
                                                        <option value="0" selected>DisApprove</option>
                                                    @endif
                                                @else
                                                    <option value="1">Approve</option>
                                                    <option value="0">DisApprove</option>
                                                @endif
                                            </select>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12" >
                                <div class="col-md-12" id="twitterDiv" style="display: none">
                                    <textarea class="form-control" id="descTwitter" name="descTwitter"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Save Influencer">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.influencer.script')
