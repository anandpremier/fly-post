
<title>Edit Country</title>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Update Country Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Country</li>
            <li class="active"><a onclick="location.reload()" style="cursor: pointer">Edit Country</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Details Of The Country</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form class="form-horizontal" method="POST" id="CountryRegister" action="{{route('admin.register.country')}}">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="hidden" name="editid" value="{{$country->id}}">
                                <label>Country Name:</label>
                                <input id="country" type="text" placeholder="Country Name"
                                       class="form-control @error('country') is-invalid @enderror" name="country"
                                       @if(isset($country->name))
                                       value="{{old('country') ? old('country') : ( ($country->name) ? $country->name : '' )}}"
                                       @endif
                                       style="text-transform:uppercase"
                                       autofocus>
                                @if($errors->has('country'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Country Code:</label>
                                <input id="code" type="text" placeholder="Country Code"
                                       class="form-control @error('email') is-invalid @enderror" name="country_code"
                                       @if(isset($country->code))
                                       value="{{old('country_code') ? old('country_code') : ( ($country->code) ? $country->code : '' )}}"
                                       @endif
                                       autocomplete="email" style="text-transform:uppercase">
                                @if($errors->has('country_code'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('country_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="form-group">
                            <div class="col-sm-6">
                                <input id="numberCode" type="text" placeholder="Number Code"
                                       class="form-control @error('numCode') is-invalid @enderror" name="numCode"
                                       @if(isset($country->numcode))
                                            value="{{old('numCode') ? old('numCode') : ( ($country->numcode) ? $country->numcode : '' )}}"
                                       @endif
                                       autocomplete="new-password">
                                @if($errors->has('numCode'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">*{{ $errors->first('numCode') }}</strong>
                                    </span>
                                @endif
                            </div>--}}
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Phone Code:</label>
                                <input id="phoneCode" type="text" class="form-control"
                                       placeholder="Phone Code" name="phoneCode"
                                       @if(isset($country->phonecode))
                                       value="{{old('phoneCode') ? old('phoneCode') : ( ($country->phonecode) ? $country->phonecode : '' )}}"
                                       @endif
                                       autocomplete="new-password">
                                @if($errors->has('phoneCode'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('phoneCode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Edit Country">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.country.script')
