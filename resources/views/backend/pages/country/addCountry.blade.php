<title>Add Country</title>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Country Registration Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Country</li>
            <li class="active"><a href="{{route('admin.create.country')}}">List Country</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details Of The Country</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form class="form-horizontal" method="POST" id="CountryRegister" action="{{route('admin.register.country')}}">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Coountry:</label>
                                <input id="country" type="text" placeholder="Country Name"
                                       class="form-control @error('country') is-invalid @enderror" name="country"
                                       value="{{ old('country') }}" autocomplete="firstname"
                                       style="text-transform:uppercase"
                                       autofocus>
                                @if($errors->has('country'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Country Code:</label>
                                <input id="code" type="text" placeholder="Country Code"
                                       class="form-control @error('email') is-invalid @enderror" name="country_code"
                                       value="{{ old('country_code') }}" autocomplete="email" style="text-transform:uppercase">
                                @if($errors->has('country_code'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('country_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Phone Code:</label>
                                <input id="phoneCode" type="text" class="form-control"
                                       placeholder="Phone Code" name="phoneCode"
                                       autocomplete="new-password">
                                @if($errors->has('phoneCode'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error"
                                                style="color: red">{{ $errors->first('phoneCode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Add Country">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.country.script')
