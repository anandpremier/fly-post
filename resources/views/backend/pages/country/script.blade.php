<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function deleteCountry(input) {
        var id = input;
        var url = "{{route('admin.destroy.country', ":id")}}";
        url = url.replace(':id', id);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Country!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                        },
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            });
    }
</script>
<script>
    $(document).ready(function () {
        $("#CountryRegister").validate({
            rules:{
                country_code:{
                    required:true,
                },
                country:{
                    required:true
                },
                numCode:{
                    required:true,
                    integer:true,
                },
                phoneCode:{
                    required:true,
                    integer:true,
                }
            },
            messages:{
                country_code:{
                    required:"Please Enter Country Code",
                },
                country:{
                    required:"Please Enter Country Name"
                },
                numCode:{
                    required:"Please Enter Number-Code",
                    integer:"Please Enter Valid Number-Code",
                },
                phoneCode:{
                    required:"Please Enter Phone-Code",
                    integer:"Please Enter Valid Phone-Code",
                }
            }
        });
    });
</script>
