<title>Country List</title>
<head>
    <link rel="stylesheet" href="{{ URL::asset('css/switch.css') }}">
</head>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Country
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Content Management</li>
            <li class="active"><a href="{{route('admin.get.country')}}">List Country</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List Of All Country </h3>
                    <a class="btn btn-primary pull-right" href="{{route('admin.create.country')}}" style="margin-right: 28px;">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add New Country
                    </a>
                </div>
                <div class="box-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Country Name</th>
                                <th>Country Code</th>
                                <th>Phone Code</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($country as $k=> $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->code}}</td>
                                    <td>{{$data->phonecode}}</td>
                                    <td>
                                        <a class="btn btn-success btn-sm" href="{{route('admin.edit.country',$data->id)}}">
                                            <span class="glyphicon glyphicon-edit"></span> Edit
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button onclick="deleteCountry({{$data->id}})" data-id="{{$data->id}}" class="btn btn-danger btn-sm">
                                            <span class="glyphicon glyphicon-remove"></span> Delete
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.country.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
        } );
        table.buttons().container()
            .appendTo( $('div.eight.column:eq(0)', table.table().container()) );
    } );
</script>


