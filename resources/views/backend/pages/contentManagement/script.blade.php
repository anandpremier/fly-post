<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function deleteEMAIL(input) {
        var id = input;
        var url = "{{route('admin.destroy.mail', ":id")}}";
        url = url.replace(':id', id);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this E-mail!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                        },
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            });
    }
</script>
<script>
    function deleteTemplate(input){
        var id = input;
        var url = "{{route('admin.email.template.destroy', ":id")}}";
        url = url.replace(':id', id);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this E-Mail Template!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                        },
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            });
    }
</script>
<script>
    $(document).ready(function () {
        $("#addEmail").validate({
            rules:{
                email:{
                    required:true,
                    email:true,
                },
                designation:{
                    required:true,
                    integer:true
                }
            },
            messages:{
                email:{
                    required:"Please Enter Email",
                    email:"Please Enter Valid Email"
                },
                designation:{
                    required:"Please Select Designation",
                    integer:"Invalid Selection"
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#emailTemplate").validate({
            ignore: [],
            debug: false,
            rules:{
                from:{
                    required:true,
                    email:true,
                },
                status:{
                    required:true,
                    integer:true,
                },
                /*cc:{
                    required:true,
                    email:true,
                },
                bcc:{
                    required:true,
                    email:true,
                },*/
                for_whom:{
                    required:true,
                },
                editor:{
                    required:true,
                },
                emailEvent:{
                    required:true,
                },
                subject:{
                    required:true,
                }
            },
            messages:{
                from:{
                    required:"Please Select Proper Sender",
                    email:"Invalid Selection",
                },
                status:{
                    required:"Please Select Proper Status",
                    integer:"Invalid Selection",
                },
                /*cc:{
                    required:"Please Select Proper CC",
                    email:"Invalid Selection",
                },
                bcc:{
                    required:"Please Select Proper Bcc",
                    email:"Invalid Selection",
                },*/
                for_whom:{
                    required:"Please Select For Whom",
                },
                editor:{
                    required:"Please Enter Template Design",
                },
                emailEvent:{
                    required:"Please Enter Proper Event Name",
                },
                subject:{
                    required:"Please Enter Proper Subject",
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#addFaq").validate({
            ignore: [],
            debug: false,
            rules:{
                question:{
                    required:true,
                },
                status:{
                    required:true,
                    integer:true,
                },
                for_whom:{
                    required:true,
                },
                editor:{
                    required:true,
                },
            },
            messages:{
                question:{
                    required:"Please Enter Proper Question Here",
                },
                status:{
                    required:"Please Select Proper Status",
                    integer:"Invalid Selection",
                },
                for_whom:{
                    required:"Please Select For Whom",
                },
                editor:{
                    required:"Please Enter Answer Here",
                },
            }
        });
    });
</script>
<script>
    function deleteFaq(input) {
        var id = input;
        var url = "{{route('admin.destroy.faqs', ":id")}}";
        url = url.replace(':id', id);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Faq!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                        },
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            });
    }


    function insertValueQuery () {
        var myListBox = $('#tablefields').val();
        var optionalDP = $('#optionalDP').val();
        if(optionalDP == 1){
            var subject = $('#subject').val();
            subject = subject + myListBox;
            $('#subject').val(subject);
        }
        else if(optionalDP == 2)
        {
            var editor = CKEDITOR.instances['editor'].getData(editor);
            editor = editor + myListBox;
            CKEDITOR.instances['editor'].setData(editor);
        }
    }
</script>
