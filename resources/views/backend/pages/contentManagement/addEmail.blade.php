<title>Add E-mail Address</title>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Add E-mail Address
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Content Management</li>
            <li class="active"><a onclick="location.reload();" style="cursor: pointer">Add E-mail Address</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details To Add E-mail Address</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form class="form-horizontal" id="addEmail" method="POST" action="{{route('admin.email.store')}}">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>E-Mail:</label>
                                <input id="email" type="text" placeholder="Enter Email"
                                       class="form-control @error('email') is-invalid @enderror" name="email"
                                       value="{{ old('email') }}" autocomplete="email"
                                       style="text-transform:uppercase"
                                       autofocus>
                                @if($errors->has('email'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Designation:</label>
                                <select id="designation" class="form-control @error('designation') is-invalid @enderror" name="designation">
                                    <option selected disabled>SELECT DESIGNATION</option>
                                    <option value="1">OVERALL ADMIN</option>
                                    <option value="2">CAMPAIGN MANAGER</option>
                                    <option value="3">SALES MANAGER</option>
                                </select>
                                {{--<input id="designation" type="text" placeholder="Enter Designation"
                                       class="form-control @error('designation') is-invalid @enderror" name="designation"
                                       value="{{ old('designation') }}" autocomplete="designation"
                                       style="text-transform:uppercase"
                                       autofocus>
                                @if($errors->has('designation'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('designation') }}</strong>
                                    </span>
                                @endif--}}
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.contentManagement.script')
