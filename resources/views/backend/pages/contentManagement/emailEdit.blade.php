<title>Edit E-mail Address</title>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Edit E-mail Address
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Content Management</li>
            <li class="active"><a onclick="location.reload();" style="cursor: pointer">Edit E-mail Address</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details To Edit E-mail Address</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form class="form-horizontal" id="addEmail" method="POST" action="{{route('admin.email.store')}}">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" value="{{$data[0]->id}}">
                                <label>E-Mail:</label>
                                <input id="email" type="text" placeholder="Enter Email"
                                       class="form-control @error('email') is-invalid @enderror" name="email"
                                       autocomplete="email"
                                       @if(isset($data))
                                       value="{{old('email') ? old('email') : ( ($data[0]->email) ? $data[0]->email : '' )}}"
                                       @endif
                                       style="text-transform:uppercase"
                                       autofocus>
                                @if($errors->has('email'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Designation:</label>
                                <select id="designation" class="form-control @error('designation') is-invalid @enderror" name="designation">
                                    @if(isset($data[0]->designation))
                                        @if($data[0]->designation == 1)
                                            <option disabled>SELECT DESIGNATION</option>
                                            <option value="1" selected>OVERALL ADMIN</option>
                                            <option value="2">CAMPAIGN MANAGER</option>
                                            <option value="3">SALES MANAGER</option>
                                        @elseif($data[0]->designation == 2)
                                            <option disabled>SELECT DESIGNATION</option>
                                            <option value="1">OVERALL ADMIN</option>
                                            <option value="2" selected>CAMPAIGN MANAGER</option>
                                            <option value="3">SALES MANAGER</option>
                                        @elseif($data[0]->designation == 3)
                                            <option disabled>SELECT DESIGNATION</option>
                                            <option value="1">OVERALL ADMIN</option>
                                            <option value="2">CAMPAIGN MANAGER</option>
                                            <option value="3" selected>SALES MANAGER</option>
                                        @else
                                            <option disabled selected>SELECT DESIGNATION</option>
                                            <option value="1">OVERALL ADMIN</option>
                                            <option value="2">CAMPAIGN MANAGER</option>
                                            <option value="3">SALES MANAGER</option>
                                        @endif
                                    @else
                                        <option disabled selected>SELECT DESIGNATION</option>
                                        <option value="1">OVERALL ADMIN</option>
                                        <option value="2">CAMPAIGN MANAGER</option>
                                        <option value="3">SALES MANAGER</option>
                                    @endif
                                </select>
                                {{--<input id="designation" type="text" placeholder="Enter Designation"
                                       class="form-control @error('designation') is-invalid @enderror" name="designation"
                                       @if(isset($data))
                                       value="{{old('designation') ? old('designation') : ( ($data[0]->designation) ? $data[0]->designation : '' )}}"
                                       @endif
                                       style="text-transform:uppercase"
                                       autofocus>
                                @if($errors->has('designation'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('designation') }}</strong>
                                    </span>
                                @endif--}}
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.contentManagement.script')
