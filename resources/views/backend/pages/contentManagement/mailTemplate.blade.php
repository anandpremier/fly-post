<title>Add E-mail Templates</title>
<style>
    .unique{
        margin-bottom: 10px;
    }
</style>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Add E-mail Templates
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Content Management</li>
            <li class="active"><a onclick="location.reload();" style="cursor: pointer">Add E-mail Templates</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details To Add E-mail Templates</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form class="form-horizontal" id="emailTemplate" method="POST" action="{{route('admin.email.template.store')}}">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12 unique">
                                <label>E-Mail Template:</label>
                                <input id="emailEvent" type="text" placeholder="Email Event"
                                       class="form-control @error('emailEvent') is-invalid @enderror" name="emailEvent"
                                       value="{{ old('emailEvent') }}" autocomplete="emailEvent"
                                       autofocus>
                                @if($errors->has('emailEvent'))
                                    <span id="invalid-feedback" role="alert">
                                    <strong id="error" style="color: red">{{ $errors->first('emailEvent') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 unique">
                                <label>Subject:</label>
                                <input id="subject" type="text" placeholder="Subject"
                                       class="form-control @error('subject') is-invalid @enderror" name="subject"
                                       value="{{ old('subject') }}" autocomplete="subject"
                                       autofocus>
                                @if($errors->has('subject'))
                                    <span id="invalid-feedback" role="alert">
                                    <strong id="error" style="color: red">{{ $errors->first('subject') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 unique">
                                <label>Status:</label>
                                <select class="form-control" name="status" id="status">
                                    <option selected disabled>--SELECT STATUS--</option>
                                    <option value="1">Active</option>
                                    <option value="0">Deacitve</option>
                                </select>
                                @if($errors->has('status'))
                                    <span id="invalid-feedback" role="alert">
                                    <strong id="error" style="color: red">
                                        Please Select Status{{--{{ $errors->first('status') }}--}}
                                    </strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-sm-3 unique">
                                <label>For Whom:</label>
                                <select class="form-control" name="for_whom" id="for_whom">
                                    <option selected disabled>--FOR WHOM--</option>
                                    <option value="1">Influencer</option>
                                    <option value="0">Multiplier</option>
                                </select>
                                @if($errors->has('for_whom'))
                                    <span id="invalid-feedback" role="alert">
                                    <strong id="error" style="color: red">
                                        For Whom You Want To Make Template? {{--{{ $errors->first('status') }}--}}
                                    </strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-sm-6 unique">
                                <label>From:</label>
                                <select class="form-control" name="from" id="from">
                                    <option selected disabled>--SELECT FROM--</option>
                                    @foreach($email as $data)
                                        <option value="{{$data->email}}">{{$data->email}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('from'))
                                    <span id="invalid-feedback" role="alert">
                                    <strong id="error" style="color: red">
                                        Please Select Sender{{--{{ $errors->first('from') }}--}}
                                    </strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 unique">
                                <label>CC:</label>
                                <select class="form-control" name="cc" id="cc">
                                    <option selected disabled>--SELECT CC--</option>
                                    @foreach($email as $data)
                                        <option value="{{$data->email}}">{{$data->email}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('cc'))
                                    <span id="invalid-feedback" role="alert">
                                    <strong id="error" style="color: red">
                                        Please Select CC{{--{{ $errors->first('cc') }}--}}
                                    </strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-sm-6 unique">
                                <label>BCC:</label>
                                <select class="form-control" name="bcc" id="bcc">
                                    <option selected disabled>--SELECT BCC--</option>
                                    @foreach($email as $data)
                                        <option value="{{$data->email}}">{{$data->email}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('bcc'))
                                    <span id="invalid-feedback" role="alert">
                                    <strong id="error" style="color: red">
                                        Please Select BCC{{--{{ $errors->first('bcc') }}--}}
                                    </strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 unique">
                                <label>Template:</label>
                                <textarea class="form-control" id="editor" name="editor"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit">
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-2">
            <div id="tablefieldscontainer">
                <label>Columns</label>
                <select style="width: 93px;" id="optionalDP">
                    <option disabled selected>Select</option>
                    <option value="1">Subject</option>
                    <option value="2">Template</option>
                </select>
                <select class="email_filed_text" id="tablefields" name="dummy" size="13" multiple="multiple" ondblclick="insertValueQuery()">
                    @foreach($email_fields as $fields)
                        <option value="[{{$fields->field_name}}]">{{$fields->field_name}}</option>
                    @endforeach
                </select>
                <div id="tablefieldinsertbuttoncontainer"><input type="button" class="button" name="insert" value="<<" onclick="insertValueQuery()" title="Insert"></div>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.contentManagement.script')
<script>
    $('textarea').ckeditor();
</script>
