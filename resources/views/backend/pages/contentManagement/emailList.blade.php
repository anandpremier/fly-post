<title>E-mail</title>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;E-mail
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Content Management</li>
            <li class="active"><a onclick="location.reload();" style="cursor: pointer">E-mail</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List Of All Email </h3>
                    <a class="btn btn-primary pull-right" href="{{route('admin.email.create')}}" style="margin-right: 28px;">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add New E-Mail
                    </a>
                </div>
                <div class="box-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>E-mail</th>
                                <th>Designation</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($obj as $k=> $data)
                            <tr>
                                <td>{{$data->id}}</td>
                                <td>{{$data->email}}</td>
                                <td>
                                    @if($data->designation == 1)
                                        OVERALL ADMIN
                                    @elseif($data->designation == 2)
                                        CAMPAIGN MANAGER
                                    @elseif($data->designation == 3)
                                        SALES MANAGER
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-success btn-sm" href="{{route('admin.edit.email',$data->id)}}">
                                        <span class="glyphicon glyphicon-edit"></span> Edit
                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button onclick="deleteEMAIL({{$data->id}})" data-id="{{$data->id}}" class="btn btn-danger btn-sm">
                                        <span class="glyphicon glyphicon-remove"></span> Delete
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                            {{--<tfoot>
                            <tr>
                                <th>Id</th>
                                <th>E-mail</th>
                                <th>Designation</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>--}}
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.contentManagement.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
        } );
        table.buttons().container()
            .appendTo( $('div.eight.column:eq(0)', table.table().container()) );
    } );
</script>


