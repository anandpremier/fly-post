
<title>Add Faq's</title>
<style>
    .unique{
        margin-bottom: 10px;
    }
</style>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Add Faq's Templates
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Content Management</li>
            <li class="active"><a onclick="location.reload();" style="cursor: pointer">Add Faq's</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details To Add Faq's</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form class="form-horizontal" id="addFaq" method="POST" action="{{route('admin.faqs.store')}}">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12 unique">
                                <label>Question:</label>
                                <input id="question" type="text" placeholder="Enter Question"
                                       class="form-control @error('question') is-invalid @enderror" name="question"
                                       value="{{ old('question') }}" autocomplete="emailEvent"
                                       style="text-transform:uppercase"
                                       autofocus>
                                @if($errors->has('question'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('question') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 unique">
                                <label>Answer:</label>
                                <textarea class="form-control" name="editor" placeholder="Your Answer here..."></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 unique">
                                <label>Status:</label>
                                <select class="form-control" name="status" id="status">
                                    <option selected disabled>--SELECT STATUS--</option>
                                    <option value="1">Active</option>
                                    <option value="0">Deacitve</option>
                                </select>
                                @if($errors->has('status'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">
                                            Please Select Status{{--{{ $errors->first('status') }}--}}
                                        </strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-6 unique">
                                <label>For Whom:</label>
                                <select class="form-control" name="for_whom" id="for_whom">
                                    <option selected disabled>--FOR WHOM--</option>
                                    <option value="0">General</option>
                                    <option value="1">Influencer</option>
                                    <option value="2">Advertiser</option>
                                </select>
                                @if($errors->has('for_whom'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">
                                            For Whom You Want To Make Faq's? {{--{{ $errors->first('status') }}--}}
                                        </strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.contentManagement.script')
<script>
    $('textarea').ckeditor();
</script>
