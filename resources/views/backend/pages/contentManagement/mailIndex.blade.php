<title>E-mail Templates</title>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;E-mail Templates
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Content Management</li>
            <li class="active"><a href="{{route('admin.mail.get')}}">E-mail Templates</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List Of All Email </h3>
                    <a class="btn btn-primary pull-right" href="{{route('admin.email.template')}}" style="margin-right: 28px;">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add New E-Mail Template
                    </a>
                </div>
                <div class="box-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Template Events</th>
                                <th>For Whom</th>
                                <th>Status</th>
                                <th>FROM</th>
                                <th>CC</th>
                                <th>BCC</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;?>
                            @foreach($data as $k=> $data1)
                                <tr>
                                    <td>{{$data[$i]->id}}</td>
                                    <td>{{$data[$i]->event}}</td>
                                    <td>
                                        @if($data[$i]->for_whom)
                                            Influencer
                                        @else
                                            Multiplier
                                        @endif
                                    </td>
                                    <td>
                                        @if($data[$i]->status)
                                            Active
                                        @else
                                            DeActive
                                        @endif
                                    </td>
                                    <td>{{$data[$i]->from}}</td>
                                    <td>{{$data[$i]->cc}}</td>
                                    <td>{{$data[$i]->bcc}}</td>
                                    <td>
                                        <a class="btn btn-success btn-sm" href="{{route('admin.email.template.edit',$data[$i]->id)}}">
                                            <span class="glyphicon glyphicon-edit"></span> Edit
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button class="btn btn-danger btn-sm" onclick="deleteTemplate({{$data[$i]->id}})">
                                            <span class="glyphicon glyphicon-remove"></span> Delete
                                        </button>
                                    </td>
                                </tr>
                                <?php $i++;?>
                            @endforeach
                            </tbody>
                            {{--<tfoot>
                            <tr>
                                <th>Id</th>
                                <th>E-mail Template Events</th>
                                <th>For Whom</th>
                                <th>Status</th>
                                <th>FROM</th>
                                <th>CC</th>
                                <th>BCC</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>--}}
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.contentManagement.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
        } );
        table.buttons().container()
            .appendTo( $('div.eight.column:eq(0)', table.table().container()) );
    } );
</script>


