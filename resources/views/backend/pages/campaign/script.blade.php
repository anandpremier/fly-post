<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function deleteCampaign(input) {
        var id = input;
        var url = "{{route('admin.destroy.campaign', ":id")}}";
        url = url.replace(':id', id);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Campaign!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                        },
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            });
    }
</script>
<script>
    function dataFilter() {
        var favorite = [];
        $.each($("input[name='channel[]']:checked"), function(){
            favorite.push($(this).val());
        });
        var fromage    = $('#fromage').val();
        var toage    = $('#toage').val();
        var gender = $('#gender').val();
        var states = $('#states').val();
        var cities = $('#cities').val();
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "{{route('admin.fetch.influencers')}}",
            data: {
                "_token": "{{ csrf_token() }}",
                "channel":favorite,
                "fromage":fromage,
                "toage":toage,
                "gender":gender,
                "state":states,
                "cities":cities
            },
            success: function (data) {
                $('#dynamicFetch').html(data['count']);

                var elems = [];
                var i=0;
                $.each(data['data'], function() {
                    elems[i] = data['data'][i]['id'];
                    i++;
                });
                $('#influencers').val(elems);
            }
        });

    }
</script>
<script>
    $(document).ready(function () {

        $.validator.addMethod('greaterThanEqualTo1', function(value, element, param) {
            var val = Number(value);
            var par= Number($(param).val());
            return val >= par;
        }, "Invalid Age");

        $.validator.addMethod('greaterThan1', function(value, element, param) {
            return this.optional(element) || value >= $(param).val();
        }, "Date Must Be [Greater Than (OR) Equal To] 'Starting Date:'");

        $("#editCampaign").validate({
            rules:{
                fromage:{
                    required:true,
                    integer:true,
                },
                toage:{
                    required:true,
                    integer:true,
                    greaterThanEqualTo1: '#fromage',
                },
                startdate:{
                    required:true,
                    date:true,
                },
                enddate:{
                    required:true,
                    date:true,
                    greaterThan1:'#startdate'
                },
                campaign_name:{
                    required:true,
                },
                shares:{
                    required:true,
                    integer:true,
                },
                overperform:{
                    required:true,
                    number:true,
                },
                link:{
                    required:true,
                    url: true
                },
                imageCampaign:{
                    extension: "jpg|jpeg|png",
                    maxsize: 200000
                },
                title:{
                    required:true
                },
                labeling:{
                    required:true,
                },
                labeling_placement:{
                    required:true
                },
                description:{
                    required:true,
                },
                "cities[]":{
                    required:true,
                },
                gender:{
                    required:true,
                    integer:true
                },
                c_d_s:{
                    required:true,
                    integer:true
                }
            },
            messages:{
                fromage:{
                    required:"Please Select Starting Age Group",
                    integer:"Invalid Age"
                },
                toage:{
                    required:"Please Select Ending Age Group",
                    integer:"Invalid Age"
                },
                enddate:{
                    required:"Please Select Campaign End Date",
                    date:"Please Select Proper Date",
                },
                startdate:{
                    required:"Please Select Campaign Starting Date",
                    date:"Please Select Proper Date",
                },
                campaign_name:{
                    required:"Please Enter Campaign Name",
                },
                shares:{
                    required:"Please Enter Number Of Shares",
                    integer:"Please Enter Valid Shares Amount"
                },
                overperform:{
                    required:"Please Enter OverPerform",
                    float:"Please Enter Valid OverPerform",
                },
                link:{
                    required:"Please Enter Web link to content",
                    url:"Invalid Link",
                },
                imageCampaign:{
                    required:"Please Select Campaign Image",
                    extension: "Please Select From jpg|jpeg|png Format",
                    maxsize: "Please Select File Less Than 2MB Size"
                },
                title:{
                    required:"Please Enter Campaign Title"
                },
                labeling:{
                    required:"Please Enter Promotional Labelling"
                },
                labeling_placement:{
                    required:"Please Select Promotional Labeling Placement"
                },
                description:{
                    required:"Please Enter Campaign Description",
                },
                "cities[]":{
                    required:"Please Select Cities",
                },
                gender:{
                    required:"Please Select Gender",
                    integer:"Please Select Valid Gender"
                },
                c_d_s:{
                    required:"Please Enter Daily Share Limit",
                    integer:"Please Enter Valid Daily Share Limit",
                },
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $.validator.addMethod('greaterThanEqualTo', function(value, element, param) {
            var val = Number(value);
            var par= Number($(param).val());
            return val >= par;
        }, "Invalid Age");

        $.validator.addMethod('greaterThan', function(value, element, param) {
            return this.optional(element) || value >= $(param).val();
        }, "Date Must Be [Greater Than (OR) Equal To] 'Starting Date:'");

        $.validator.addMethod('greaterThanToday', function(value, element, param) {
            var dateString = $(param).val();
            var myDate = new Date(dateString);
            var today = new Date();
            return this.optional(element) || myDate >= today;
        }, "Date Must Be [Greater Than] Current Date");

        $("#campaignForm").validate({
            rules:{
                fromage:{
                    required:true,
                    integer:true,
                },
                toage:{
                    required:true,
                    integer:true,
                    greaterThanEqualTo: '#fromage',
                },
                startdate:{
                    required:true,
                    date:true,
                    greaterThanToday: '#startdate',
                },
                enddate:{
                    required:true,
                    date:true,
                    greaterThan:'#startdate'
                },
                campaign_name:{
                    required:true,
                },
                shares:{
                    required:true,
                    integer:true,
                },
                overperform:{
                    required:true,
                    number:true,
                },
                link:{
                    required:true,
                    url: true
                },
                image:{
                    required:true,
                    extension: "jpg|jpeg|png",
                    maxsize: 200000
                },
                imageCampaign:{
                    extension: "jpg|jpeg|png",
                    maxsize: 200000
                },
                title:{
                    required:true
                },
                labeling:{
                    required:true,
                },
                labeling_placement:{
                    required:true
                },
                description:{
                    required:true,
                },
                "cities[]":{
                    required:true,
                },
                gender:{
                    required:true,
                    integer:true
                },
                c_d_s:{
                    required:true,
                    integer:true
                },
            },
            messages:{
                fromage:{
                    required:"Please Select Starting Age Group",
                    integer:"Invalid Age"
                },
                toage:{
                    required:"Please Select Ending Age Group",
                    integer:"Invalid Age"
                },
                enddate:{
                    required:"Please Select Campaign End Date",
                    date:"Please Select Proper Date",
                },
                startdate:{
                    required:"Please Select Campaign Starting Date",
                    date:"Please Select Proper Date",
                },
                campaign_name:{
                    required:"Please Enter Campaign Name",
                },
                shares:{
                    required:"Please Enter Number Of Shares",
                    integer:"Please Enter Valid Shares Amount"
                },
                overperform:{
                    required:"Please Enter OverPerform",
                    number:"Please Enter Valid OverPerform",
                },
                link:{
                    required:"Please Enter Web link to content",
                    url: "Please Enter Proper Url"
                },
                image:{
                    required:"Please Select Campaign Image",
                    extension: "Please Select From jpg|jpeg|png Format",
                    maxsize: "Please Select File Less Than 2MB Size"
                },
                imageCampaign:{
                    required:"Please Select Campaign Image",
                    extension: "Please Select From jpg|jpeg|png Format",
                    maxsize: "Please Select File Less Than 2MB Size"
                },
                title:{
                    required:"Please Enter Campaign Title"
                },
                labeling:{
                    required:"Please Enter Promotional Labelling"
                },
                labeling_placement:{
                    required:"Please Select Promotional Labeling Placement"
                },
                description:{
                    required:"Please Enter Campaign Description",
                },
                "cities[]":{
                    required:"Please Select Cities",
                },
                gender:{
                    required:"Please Select Gender",
                    integer:"Please Select Valid Gender"
                },
                c_d_s:{
                    required:"Please Enter Daily Share Limit",
                    integer:"Please Enter Valid Daily Share Limit",
                },
            },

            submitHandler: function(form) {
                form.submit();
            }

        });
    });
</script>

<script>
    $(document).ready(function () {
        $("#manageCampaignForm").validate({
            rules:{
                noc:{
                    required:true,
                    number:true,
                },
                status:{
                    required:true,
                    integer:true,
                },
                datetime:{
                    required:true,
                    date:true,
                },
                batchsize:{
                    required:true,
                    number:true,
                },
                time:{
                    required:true,
                    number:true,
                },
                remuneration:{
                    required:true,
                    number:true,
                },
                rc:{
                    required:true,
                    /*number:true,*/
                }
            },
            messages:{
                noc:{
                    required:"Please Enter Number Of Comments ",
                    number:"Invalid Number Of Comments"
                },
                status:{
                    required:"Please Select Proper Status",
                    integer:"Invalid Selection",
                },
                datetime:{
                    required:"Please Select Proper Date And Time",
                    date:"Invalid Selection"
                },
                batchsize:{
                    required:"PLease Enter Proper BatchSize",
                    number:"Invalid Batch Size",
                },
                time:{
                    required:"Please Enter Proper Time Interval",
                    number:"Invalid Time Entered",
                },
                remuneration:{
                    required:"Please Enter Proper Remuneration",
                    number:"Invalid Time Entered",
                },
                rc:{
                    required:"Please Enter Remuneration Comment"
                    /*number:true,*/
                }
            }
        });
    });
</script>
