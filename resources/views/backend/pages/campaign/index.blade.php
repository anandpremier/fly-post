
<title>Campaign List</title>
@include('backend.layouts.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Campaign
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Campaign Management</li>
            <li class="active"><a href="{{route('admin.get.campaign')}}">List Campaign</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List Of All Campaign </h3>
                    <a class="btn btn-primary pull-right" href="{{route('admin.create.campaign')}}" style="margin-right: 28px;">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add New Campaign
                    </a>
                </div>
                <div class="box-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>No.Of Shares</th>
                                <th>OverPerform</th>
                                {{--<th>Description</th>--}}
                                <th>Campaign Start</th>
                                <th>Campaign End</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($obj as $k=> $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->title}}</td>
                                    <td>
                                        @if($data->status == 0)
                                            Waiting for Approval
                                        @elseif($data->status == 1)
                                            Approved But Not Started
                                        @elseif($data->status == 2)
                                            Active/Approved
                                        @elseif($data->status == 3)
                                            Finished
                                        @elseif($data->status == 4)
                                            Not Approved
                                        @elseif($data->status == 5)
                                            Mail Sending Paused
                                        @elseif($data->status == 6)
                                            Paused
                                        @endif
                                    </td>
                                    <td>{{$data->number_of_shares}}</td>
                                    <td>{{$data->shares_over_performance}}</td>
                                    {{--<td>{{$data->message}}</td>--}}
                                    <td>{{$data->campaign_start}}</td>
                                    <td>{{$data->campaign_end}}</td>
                                    <td>
                                        <a style="color: green;font-size: x-large;cursor: pointer;"href="{{route('admin.edit.campaign',$data->id)}}">
                                            <i class="fa fa-edit"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a style="color: red;font-size: x-large;cursor: pointer;" onclick="deleteCampaign({{$data->id}})" data-id="{{$data->id}}">
                                            <i class="fa fa-trash"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a style="color: dodgerblue;font-size: x-large;cursor: pointer;" href="{{route('admin.manage.campaign',$data->id)}}" data-id="{{$data->id}}">
                                            <i class="fa fa-cogs"></i>
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.campaign.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
        } );
        table.buttons().container()
            .appendTo( $('div.eight.column:eq(0)', table.table().container()) );
    } );
</script>


