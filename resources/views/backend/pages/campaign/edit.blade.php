<title>Edit Campaign</title>
@include('backend.layouts.app')
<style>
    .unique{
        margin-bottom: 13px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Update Campaign Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Campaign Management</li>
            <li class="active"><a onclick="location.reload()" style="cursor: pointer">Edit Campaign</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details To Update The Campaign</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="editCampaign" enctype="multipart/form-data" action="{{route('admin.register.campaign')}}"
                      method="post">
                    @csrf
                    <div class="box-body">
                        <input type="hidden" value="{{$campaign->id}}" name="editid">
                        <div class="form-group">
                            <input type="hidden" name="influencers[]" id="influencers">
                            <div class="col-md-2 unique" style="margin-right: -36px;">
                                <label>
                                    Campaign Type:
                                </label>
                            </div>
                            <div class="col-md-3 unique" style="margin-right: -98px;">
                                <input type="radio" name="campaign_type" value="1" checked>
                                <label>
                                    Social Seeding
                                </label>
                            </div>
                            <div class="col-md-4 unique" style="margin-right: -84px;">
                                <input type="radio" name="campaign_type" value="2">
                                <label>
                                    Social Performance Seeding
                                </label>
                            </div>
                            <div class="col-md-3 unique" style="width: 432px;">
                                <input type="radio" name="campaign_type" value="3">
                                <label>
                                    Video Seeding
                                </label>
                                <label style="margin-left: 127px;">
                                    Available Influencer:
                                    <span id="dynamicFetch">0000</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Campaign Name:</label>
                                <input type="text" id="campaign_name" class="form-control" name="campaign_name"
                                       @if(isset($campaign->name))
                                       value="{{old('campaign_name') ? old('campaign_name') : ( ($campaign->name) ? $campaign->name : '' )}}"
                                       @endif
                                       placeholder="Campain Name*:">
                                @if($errors->has('campaign_name'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('campaign_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-3 unique">
                                <label>No Of Shares:</label>
                                <input type="text" id="shares" class="form-control" placeholder="No. of Share* :"
                                       @if(isset($campaign->number_of_shares))
                                       value="{{old('shares') ? old('shares') : ( ($campaign->number_of_shares) ? $campaign->number_of_shares : '' )}}"
                                       @endif
                                       name="shares">
                                @if($errors->has('shares'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('shares') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-3 unique">
                                <label>Overperform:</label>
                                <input type="text" id="overperform" class="form-control" placeholder="Overperform* :"
                                       @if(isset($campaign->remuneration))
                                       value="{{old('overperform') ? old('overperform') : ( ($campaign->shares_over_performance) ? $campaign->shares_over_performance : '' )}}"
                                       @endif
                                       name="overperform">
                                @if($errors->has('overperform'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('overperform') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 unique">
                                <label>Web Link:</label>
                                <input type="text" id="link" class="form-control" placeholder="Web link to content*   ? "
                                       @if(isset($campaign->link))
                                       value="{{old('link') ? old('link') : ( ($campaign->link) ? $campaign->link : '' )}}"
                                       @endif
                                       name="link">
                                @if($errors->has('link'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Campaign Image:</label>
                                <input type="file" id="imageCampaign" onchange="readURL(this);" class="form-control"
                                       name="imageCampaign">
                            </div>
                            <div class="col-md-6 unique">
                                <label>Campaign Daily Share:</label>
                                <input type="text" id="c_d_s" class="form-control" name="c_d_s" placeholder="Daily Campaign Share Limit*:"
                                       @if(isset($campaign->c_d_S))
                                       value="{{old('c_d_s') ? old('c_d_s') : ( ($campaign->c_d_S) ? $campaign->c_d_S : '' )}}"
                                    @endif
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Campaign Title:</label>
                                <input type="text" id="title" class="form-control" name="title"
                                       @if(isset($campaign->title))
                                       value="{{old('title') ? old('title') : ( ($campaign->title) ? $campaign->title : '' )}}"
                                       @endif
                                       placeholder="Campaign Title* ?">
                                @if($errors->has('title'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>Labeling:</label>
                                <input type="text" id="labeling" class="form-control" name="labeling"
                                       @if(isset($campaign->sponsored_post))
                                       value="{{old('labeling') ? old('labeling') : ( ($campaign->sponsored_post) ? $campaign->sponsored_post : '' )}}"
                                       @endif
                                       placeholder="Promotional labeling ?">
                                @if($errors->has('labeling'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('labeling') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Description:</label>
                                <textarea class="form-control" style="height: 290px;" id="description" name="description" placeholder="Description* ?">
                                    @if(isset($campaign->message))
                                        {{$campaign->message}}
                                    @endif
                                </textarea>
                            </div>
                            <div class="col-md-6 unique">
                                <label>Country:</label>
                                <input class="form-control" style="height: 46px;" placeholder="Country:INDIA" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>State:</label>
                                <select id="states" onchange="dataFilter()" class="form-control" multiple name="states[]">
                                    <option selected disabled>--SELECT STATE--</option>
                                    @foreach($state as $data)
                                        <option value="{{$data['id']}}">{{$data['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 unique">
                                <label>City:</label>
                                <select id="cities" onchange="dataFilter()" class="form-control" name="cities[]" multiple>
                                    <option selected disabled>--SELECT CITIES--</option>
                                    @foreach($cities as $data)
                                        <option value="{{$data->id}}" selected>
                                            {{$data->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-1 unique">
                                    <label>
                                        Channel:
                                    </label>
                                </div>
                                <div class="col-md-2 unique" style="margin-right: -47px;">
                                    <input type="checkbox" onchange="dataFilter()" name="channel[]" value="1"
                                           @if($campaign->getCampaignChannel[0]['channel'] == 1) checked @endif>
                                    <label>
                                        Facebook
                                    </label>
                                </div>
                                <div class="col-md-3 unique" style="margin-right: -129px;">
                                    <input type="checkbox" onchange="dataFilter()" name="channel[]" value="2"
                                           @if(isset($campaign->getCampaignChannel[0]['channel']) &&$campaign->getCampaignChannel[0]['channel'] == 2) checked @endif>
                                    <label>
                                        Twitter
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 unique">
                                    <label>Gender:</label>
                                    <select class="form-control" onchange="dataFilter()" name="gender" id="gender">
                                        @if($campaign->gender == 2)
                                            <option value="2" selected>ALL</option>
                                            <option value="0">FEMALE</option>
                                            <option value="1">MALE</option>
                                        @elseif($campaign->gender == 0)
                                            <option value="2">ALL</option>
                                            <option value="0" selected>FEMALE</option>
                                            <option value="1">MALE</option>
                                        @else
                                            <option value="2">ALL</option>
                                            <option value="0">FEMALE</option>
                                            <option value="1" selected>MALE</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-6 unique">
                                    <label>Salesman:</label>
                                    <select class="form-control" name="salesman">
                                        <option selected disabled>--SELECT SALESMAN--</option>
                                        @foreach($email as $salesman)
                                            <option value="{{$salesman->id}}">{{$salesman->email}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('salesman'))
                                        <span id="invalid-feedback" role="alert">
                                            <strong id="error" style="color: red">{{ $errors->first('salesman') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>From Age:</label>
                                <input type="number" placeholder="From Age." id="fromage" onchange="dataFilter()" name="fromage"
                                       @if(isset($campaign->age_from))
                                       value="{{old('fromage') ? old('fromage') : ( ($campaign->age_from) ? $campaign->age_from : '' )}}"
                                       @endif
                                       class="form-control">
                                @if($errors->has('fromage'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('fromage') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>To Age:</label>
                                <input type="number" placeholder="To Age." onchange="dataFilter()" id="toage" name="toage"
                                       @if(isset($campaign->age_to))
                                       value="{{old('toage') ? old('toage') : ( ($campaign->age_to) ? $campaign->age_to : '' )}}"
                                       @endif
                                       class="form-control">
                                @if($errors->has('toage'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('toage') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Starting Date:</label>
                                <input type="date" value="{{$campaign->campaign_start}}" name="startdate" id="startdate" class="form-control">
                            </div>
                            <div class="col-md-6 unique">
                                <label>Ending Date:</label>
                                <input type="date" value="{{$campaign->campaign_end}}" name="enddate" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Edit Campaign ">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.campaign.script')
<script>
    $(document).ready(function () {
        $('#states').on('change',function () {
            var val = $('#states').val();

            var id = val;
            var url = "{{route('admin.fetch.cities', ":id")}}";
            url = url.replace(':id', id);

            $.ajax({
                dataType: "json",
                type: "POST",
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id":val
                },
                success: function (data) {
                    $('#cities').html(data);
                }
            });
        });
    });
</script>
<script>
    function readURL(input) {
        var abc = $('#imageCampaign').val();
        var fileExtension = ['jpg','jpeg','png'];
        var abc1 = abc.split('.').pop().toLowerCase();
        if($.inArray(abc1,fileExtension) === 0)
        {
            $('#imageCampaign-error').hide();
        }
        else
        {
            if (input.files && input.files[0]) {
                $('#imageCampaign-error').hide();
            }
        }
    }
</script>

<script>
    dataFilter();
</script>
