<title>Create Campaign</title>
@include('backend.layouts.app')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
<style>
    .unique{
        margin-bottom: 13px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Campaign Registration Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Campaign Management</li>
            <li class="active"><a href="{{route('admin.create.campaign')}}">Add Campaign</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details Of The Campaign</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="campaignForm" enctype="multipart/form-data" action="{{route('admin.register.campaign')}}"
                      method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" name="influencers[]" id="influencers">
                            <div class="col-md-2 unique" style="margin-right: -36px;">
                                <label>
                                    Campaign Type:
                                </label>
                            </div>
                            <div class="col-md-3 unique" style="margin-right: -98px;">
                                <input type="radio" name="campaign_type" value="1" checked>
                                <label>
                                    Social Seeding
                                </label>
                            </div>
                            <div class="col-md-4 unique" style="margin-right: -84px;">
                                <input type="radio" name="campaign_type" value="2">
                                <label>
                                    Social Performance Seeding
                                </label>
                            </div>
                            <div class="col-md-3 unique" style="width: 432px;">
                                <input type="radio" name="campaign_type" value="3">
                                <label>
                                    Video Seeding
                                </label>
                                <label style="margin-left: 127px;">
                                    Available Influencer:
                                    <span id="dynamicFetch">0000</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Campaign Name:</label>
                                <input type="text" id="campaign_name" class="form-control" name="campaign_name"
                                       placeholder="Campaign Name*:"
                                       value="{{old('campaign_name')}}">
                                @if($errors->has('campaign_name'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('campaign_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-3 unique">
                                <label>No Of Shares:</label>
                                <input type="text" id="shares" class="form-control" placeholder="No. of Share* :"
                                       name="shares"
                                       value="{{old('shares')}}">
                                @if($errors->has('shares'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('shares') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-3 unique">
                                <label>Overperform:</label>
                                <input type="text" id="overperform" class="form-control" placeholder="Overperform* :"
                                       name="overperform"
                                       value="{{old('overperform')}}">
                                @if($errors->has('overperform'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('overperform') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 unique">
                                <label>Web Link:</label>
                                <input type="text" id="link" class="form-control" placeholder="Web link to content*: " name="link"
                                       value="{{old('link')}}">
                                @if($errors->has('link'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Campaign Image:</label>
                                <input type="file" id="image" onchange="readURL(this);" class="form-control" name="image">
                            </div>
                            <div class="col-md-6 unique">
                                <label>Campaign Daily Share:</label>
                                <input type="text" id="c_d_s" class="form-control" name="c_d_s" placeholder="Daily Campaign Share Limit*:">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Campaign Title:</label>
                                <input type="text" id="title" class="form-control"
                                       value="{{old('title')}}" name="title" placeholder="Campaign Title* ?">
                                @if($errors->has('title'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>Labeling:</label>
                                <input type="text" id="labeling" class="form-control"
                                       value="{{old('labeling')}}" name="labeling" placeholder="Promotional labeling ?">
                                @if($errors->has('labeling'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('labeling') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Description:</label>
                                <textarea class="form-control" style="height: 298px;" id="description" name="description"
                                          value="{{old('description')}}" placeholder="Description* ?"></textarea>
                                @if($errors->has('description'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>Country:</label>
                                <input class="form-control" style="height: 46px;" placeholder="Country:INDIA" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>City:</label>
                                <select id="states" onchange="dataFilter()" class=" form-control" multiple name="states[]" style="height: 88px">
                                    <option selected disabled>--SELECT STATE--</option>
                                    @foreach($state as $data)
                                        <option value="{{$data['id']}}">{{$data['name']}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('state'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>City:</label>
                                <select id="cities" onchange="dataFilter()" class=" form-control" multiple name="cities[]" style="height: 88px">
                                    <option selected disabled>--SELECT CITIES--</option>
                                </select>
                                @if($errors->has('cities'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('cities') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-1 unique">
                                    <label>
                                        Channel:
                                    </label>
                                </div>
                                <div class="col-md-5 unique" style="margin-right: -252px;">
                                    <input type="checkbox" onchange="dataFilter()" name="channel[]" value="1" checked>
                                    <label>
                                        Facebook
                                    </label>
                                </div>
                                <div class="col-md-6 unique" style="margin-right: -129px;">
                                    <input type="checkbox" onchange="dataFilter()" name="channel[]" value="2" checked>
                                    <label>
                                        Twitter
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 unique">
                                    <label>Gender:</label>
                                    <select class="form-control" onchange="dataFilter()" name="gender" id="gender">
                                        <option value="2" selected>ALL</option>
                                        <option value="0">FEMALE</option>
                                        <option value="1">MALE</option>
                                    </select>
                                    @if($errors->has('gender'))
                                        <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6 unique">
                                    <label>Salesman:</label>
                                    <select class="form-control" name="salesman">
                                        <option selected disabled>--SELECT SALESMAN--</option>
                                        @foreach($email as $salesman)
                                            <option value="{{$salesman->id}}">{{$salesman->email}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('salesman'))
                                        <span id="invalid-feedback" role="alert">
                                            <strong id="error" style="color: red">{{ $errors->first('salesman') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>From Age:</label>
                                <input type="number" id="fromage" onchange="dataFilter()" placeholder="From Age."
                                       value="{{old('fromage')}}" name="fromage" class="form-control">
                                @if($errors->has('fromage'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('fromage') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>To Age:</label>
                                <input type="number" id="toage" onchange="dataFilter()" placeholder="To Age."
                                       value="{{old('toage')}}" name="toage" {{--style="width: 314px;margin-left: -58px;"--}} class="form-control">
                                @if($errors->has('toage'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('toage') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Starting Date:</label>
                                <input type="date" name="startdate" id="startdate"
                                       value="{{old('startdate')}}" class="form-control">
                                @if($errors->has('startdate'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('startdate') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique" >
                                <label>Ending Date:</label>
                                <input type="date" name="enddate" value="{{old('enddate')}}" class="form-control">
                                @if($errors->has('enddate'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('enddate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Add Campaign</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.campaign.script')
<script>
    $(document).ready(function () {
        $('#states').on('change',function () {
            var val = $('#states').val();
            if(val == ""){
                val = -1;
            }
            var id = val;
            var url = "{{route('admin.fetch.cities', ":id")}}";
            url = url.replace(':id', id);

            $.ajax({
                dataType: "json",
                type: "POST",
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id":val
                },
                success: function (data) {
                    $('#cities').html(data);
                }
            });
        });
    });
</script>
<script>
    dataFilter();
</script>
<script>
    function readURL(input) {
        var abc = $('#image').val();
        var fileExtension = ['jpg','jpeg','png'];
        var abc1 = abc.split('.').pop().toLowerCase();
        if($.inArray(abc1,fileExtension) === 0)
        {
            $('#image-error').hide();
        }
        else
        {
            if (input.files && input.files[0]) {
                $('#image-error').hide();
            }
        }
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
