<title>Manage Campaign</title>
@include('backend.layouts.app')
<style>
    .unique{
        margin-top: 30px;
    }
</style>
<link rel="stylesheet" type="text/css" href="https://www.jqueryscript.net/demo/Clean-jQuery-Date-Time-Picker-Plugin-datetimepicker/jquery.datetimepicker.css">
<link rel="stylesheet" type="text/css" href="https://www.jqueryscript.net/css/jquerysctipttop.css">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Manage Campaign
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Campaign Management</li>
            <li class="active"><a onclick="location.reload()" style="cursor: pointer;">Manage Campaign</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details To Manage Campaign</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="manageCampaignForm" enctype="multipart/form-data" action="{{route('admin.manage.campaign.store')}}"
                      method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="hidden" id="campId" name="id" value="{{$campaign[0]->id}}">
                                <div class="col-md-4 unique">
                                    <label>Status:</label>
                                    @if(isset($campaign))
                                        @if($campaign[0]->status == 0)
                                            <select class="form-control" name="status">
                                                <option disabled>Status</option>
                                                <option selected value="0">Waiting for Approval</option>
                                                <option value="1">Approved but not Started</option>
                                                <option value="2">Active</option>
                                                <option value="3">Finished</option>
                                                <option value="4">Not approved</option>
                                                <option value="5">Sending mail paused</option>
                                                <option value="6">Paused</option>
                                            </select>
                                        @elseif($campaign[0]->status == 1)
                                            <select class="form-control" name="status">
                                                <option disabled>Status</option>
                                                <option value="0">Waiting for Approval</option>
                                                <option selected value="1">Approved but not Started</option>
                                                <option value="2">Active</option>
                                                <option value="3">Finished</option>
                                                <option value="4">Not approved</option>
                                                <option value="5">Sending mail paused</option>
                                                <option value="6">Paused</option>
                                            </select>
                                        @elseif($campaign[0]->status == 2)
                                            <select class="form-control" name="status">
                                                <option disabled>Status</option>
                                                <option value="0">Waiting for Approval</option>
                                                <option value="1">Approved but not Started</option>
                                                <option selected value="2">Active</option>
                                                <option value="3">Finished</option>
                                                <option value="4">Not approved</option>
                                                <option value="5">Sending mail paused</option>
                                                <option value="6">Paused</option>
                                            </select>
                                        @elseif($campaign[0]->status == 3)
                                            <select class="form-control" name="status">
                                                <option disabled>Status</option>
                                                <option value="0">Waiting for Approval</option>
                                                <option value="1">Approved but not Started</option>
                                                <option value="2">Active</option>
                                                <option selected value="3">Finished</option>
                                                <option value="4">Not approved</option>
                                                <option value="5">Sending mail paused</option>
                                                <option value="6">Paused</option>
                                            </select>
                                        @elseif($campaign[0]->status == 4)
                                            <select class="form-control" name="status">
                                                <option disabled>Status</option>
                                                <option value="0">Waiting for Approval</option>
                                                <option value="1">Approved but not Started</option>
                                                <option value="2">Active</option>
                                                <option value="3">Finished</option>
                                                <option selected value="4">Not approved</option>
                                                <option value="5">Sending mail paused</option>
                                                <option value="6">Paused</option>
                                            </select>
                                        @elseif($campaign[0]->status == 5)
                                            <select class="form-control" name="status">
                                                <option disabled>Status</option>
                                                <option value="0">Waiting for Approval</option>
                                                <option value="1">Approved but not Started</option>
                                                <option value="2">Active</option>
                                                <option value="3">Finished</option>
                                                <option value="4">Not approved</option>
                                                <option selected value="5">Sending mail paused</option>
                                                <option value="6">Paused</option>
                                            </select>
                                        @elseif($campaign[0]->status == 6)
                                            <select class="form-control" name="status">
                                                <option disabled>Status</option>
                                                <option value="0">Waiting for Approval</option>
                                                <option value="1">Approved but not Started</option>
                                                <option value="2">Active</option>
                                                <option value="3">Finished</option>
                                                <option value="4">Not approved</option>
                                                <option value="5">Sending mail paused</option>
                                                <option selected value="6">Paused</option>
                                            </select>
                                        @else
                                            <select class="form-control" name="status">
                                                <option disabled="">Status</option>
                                                <option selected value="0">Waiting for Approval</option>
                                                <option value="1">Approved but not Started</option>
                                                <option value="2">Active</option>
                                                <option value="3">Finished</option>
                                                <option value="4">Not approved</option>
                                                <option value="5">Sending mail paused</option>
                                                <option value="6">Paused</option>
                                            </select>
                                        @endif
                                    @else
                                        <select class="form-control" name="status">
                                            <option disabled="">Status</option>
                                            <option selected value="0">Waiting for Approval</option>
                                            <option value="1">Approved but not Started</option>
                                            <option value="2">Active</option>
                                            <option value="3">Finished</option>
                                            <option value="4">Not approved</option>
                                            <option value="5">Sending mail paused</option>
                                            <option value="6">Paused</option>
                                        </select>
                                    @endif
                                </div>
                                <div class="col-md-4 unique">
                                    <label>Send Mail Until:</label>
                                    @if(isset($campaign))
                                        @if(isset($campaign[0]->send_mail_until))
                                            <input id="datetimepicker" name="datetime" placeholder="Send Mail Until" class="form-control" value="{{$campaign[0]->send_mail_until}}">
                                        @else
                                            <input id="datetimepicker" name="datetime" placeholder="Send Mail Until" class="form-control">
                                        @endif
                                    @else
                                        <input id="datetimepicker" name="datetime" placeholder="Send Mail Until" class="form-control">
                                    @endif
                                </div>
                                <div class="col-md-4 unique">
                                    <label>Batch Size:</label>
                                    @if(isset($campaign))
                                        @if(isset($campaign[0]->batch_size))
                                            <input id="batchsize" name="batchsize" placeholder="BatchSize" class="form-control" value="{{$campaign[0]->batch_size}}">
                                        @else
                                            <input id="batchsize" name="batchsize" placeholder="BatchSize" class="form-control">
                                        @endif
                                    @else
                                        <input id="batchsize" name="batchsize" placeholder="BatchSize" class="form-control">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-md-12">
                                <div class="col-md-4 unique">
                                    <label>Time Interval:</label>
                                    @if(isset($campaign))
                                        @if(isset($campaign[0]->cron_interval))
                                            <input type="number" id="time" name="time" placeholder="Time Interval (Minutes)" class="form-control" step="0.01" value="{{$campaign[0]->cron_interval}}">
                                        @else
                                            <input type="number" id="time" name="time" placeholder="Time Interval (Minutes)" class="form-control" step="0.01">
                                        @endif
                                    @else
                                        <input type="number" id="time" name="time" placeholder="Time Interval (Minutes)" class="form-control" step="0.01">
                                    @endif
                                </div>
                                <div class="col-md-4 unique">
                                    <label>Remuneration:</label>
                                    @if(isset($campaign))
                                        @if(isset($campaign[0]->remuneration))
                                            <input type="number" name="remuneration" placeholder="Remuneration" class="form-control" step="0.01" value="{{$campaign[0]->remuneration}}">
                                        @else
                                            <input type="number" name="remuneration" placeholder="Remuneration" class="form-control" step="0.01">
                                        @endif
                                    @else
                                        <input type="number" name="remuneration" placeholder="Remuneration" class="form-control" step="0.01">
                                    @endif
                                </div>
                                <div class="col-md-4 unique" >
                                    <a class="btn btn-primary form-control" style="margin-top: 22px;" id="calculateBatch">Calculate Batch Size</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-6 unique">
                                    <label>Remuneration Comment:</label>
                                    @if(isset($campaign))
                                        @if(isset($campaign[0]->comment_remuneration))
                                            <input type="text" name="rc" maxlength="5" placeholder="Remuneration Comment" class="form-control" step="0.01" value="{{$campaign[0]->comment_remuneration}}">
                                        @else
                                            <input type="text" name="rc" maxlength="5" placeholder="Remuneration Comment" class="form-control" step="0.01">
                                        @endif
                                    @else
                                        <input type="text" name="rc" maxlength="5" placeholder="Remuneration Comment" class="form-control" step="0.01">
                                    @endif
                                </div>
                                <div class="col-md-6 unique">
                                    <label>Number Of Comment:</label>
                                    @if(isset($campaign))
                                        @if(isset($campaign[0]->comment_number))
                                            <input type="number" name="noc" maxlength="5" placeholder="Number Of Comments" class="form-control" step="0.01" value="{{$campaign[0]->comment_number}}">
                                        @else
                                            <input type="number" name="noc" maxlength="5" placeholder="Number Of Comments" class="form-control" step="0.01">
                                        @endif
                                    @else
                                        <input type="number" name="noc" maxlength="5" placeholder="Number Of Comments" class="form-control" step="0.01">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" id="btnSubmit" value="Manage Campaign ">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.campaign.script')
<script src="https://www.jqueryscript.net/demo/Clean-jQuery-Date-Time-Picker-Plugin-datetimepicker/jquery.datetimepicker.js"></script>
<script>
    $('#datetimepicker').datetimepicker({
        dateFormat: "dd-mm-yy",
    });
</script>
<script>
    $(document).ready(function (){
        $('#calculateBatch').on('click',function () {
            var date = $('#datetimepicker').val();
            var interval = $('#time').val();
            var campId = $('#campId').val();
            if(date !== '' && interval !== '') {
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: "{{route('admin.batchsize.calculate')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "date": date,
                        "interval": interval,
                        'campId': campId
                    },
                    success: function (data) {
                        if(data > -1)
                            $('#batchsize').val(data);
                        else
                            alert('Invalid Date Selection');

                    }
                });
            }
            else{
                $('#btnSubmit').click();
                $('#status-error').attr('style','display:none');
                $('#batchsize-error').attr('style','display:none');
                $('#remuneration-error').attr('style','display:none');
                $('#rc-error').attr('style','display:none');
                $('#noc-error').attr('style','display:none');
            }
        });
    });
</script>
