<title>Add Admin</title>
@include('backend.layouts.app')
<style>
    .unique{
        margin-bottom: 13px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Admin Registration Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Users</li>
            <li class="active"><a href="{{route('admin.create.admin')}}">Admin Register</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details Of The Admin</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="registerForm" enctype="multipart/form-data" action="{{route('admin.register.admin')}}"
                      method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-12 unique">
                                <label>UserName:</label>
                                <input type="text" id="user_name" placeholder="Username" class="form-control"
                                       value="{{old('user_name')}}" name="user_name">
                                @if($errors->has('user_name'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>FirstName:</label>
                                <input type="text" id="firstname" class="form-control" name="firstname"
                                       value="{{old('firstname')}}" placeholder="Firstname">
                                @if($errors->has('firstname'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>LastName:</label>
                                <input type="text" id="lastname" class="form-control" placeholder="Lastname"
                                       value="{{old('lastname')}}" name="lastname">
                                @if($errors->has('lastname'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Email:</label>
                                <input type="email" id="email" value="{{old('email')}}" class="form-control" placeholder="email" name="email">
                                @if($errors->has('email'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>Skype:</label>
                                <input type="text" id="skype" class="form-control"  value="{{old('skype')}}" name="skype" placeholder="Skype">
                                @if($errors->has('skype'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('skype') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Phone:</label>
                                <input type="text" id="phone" class="form-control" name="phone" value="{{old('phone')}}" placeholder="Phone">
                                @if($errors->has('phone'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>Mobile:</label>
                                <input type="text" id="mobile" class="form-control" name="mobile" value="{{old('mobile')}}" placeholder="Mobile">
                                @if($errors->has('mobile'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Password:</label>
                                <input type="password" id="pass" class="form-control" placeholder="Password" name="pass">
                                @if($errors->has('pass'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('pass') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>ReType Password:</label>
                                <input type="password" id="retype_pass" class="form-control" name="retype_pass"
                                       placeholder="Retype password">
                                @if($errors->has('retype_pass'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('retype_pass') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>ProfileImage:</label>
                                <input type="file" id="profile" class="form-control" name="profile">
                            </div>
                            <div class="col-md-6 unique">
                                <label>Designation:</label>
                                <select name="designation" class="form-control">
                                    <option disabled selected>SELECT DESIGNATION</option>
                                    <option value="1">OVERALL ADMIN</option>
                                    <option value="2">CAMPAIGN MANAGER</option>
                                    <option value="3">SALES MANAGER</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Add Admin ">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.admins.script')
