{{--@if(isset($admin->username))
    value="{{old('country') ? old('country') : ( ($country->name) ? $country->name : '' )}}"
@endif--}}
<title>Edit Admin</title>
@include('backend.layouts.app')
<style>
    .unique{
        margin-bottom: 13px;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Update Admin Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Users</li>
            <li class="active"><a onclick="location.reload()" style="cursor: pointer">Edit Admin</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details To Update Admin</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="registerForm" enctype="multipart/form-data" action="{{route('admin.register.admin')}}"
                      method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" name="editid" value="{{$admin->id}}">
                            <div class="col-md-12 unique">
                                <label>Username:</label>
                                <input type="text" id="user_name"
                                       placeholder="Username"
                                       class="form-control"
                                       name="user_name"
                                       @if(isset($admin->username))
                                            value="{{old('user_name') ? old('user_name') : ( ($admin->username) ? $admin->username : '' )}}"
                                        @endif>
                                @if($errors->has('user_name'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>FirstName:</label>
                                <input type="text" id="firstname" class="form-control" name="firstname"
                                       @if(isset($admin->firstname))
                                       value="{{old('firstname') ? old('firstname') : ( ($admin->firstname) ? $admin->firstname : '' )}}"
                                       @endif
                                       placeholder="Firstname">
                                @if($errors->has('firstname'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>LastName:</label>
                                <input type="text" id="lastname" class="form-control" placeholder="Lastname"
                                       @if(isset($admin->lastname))
                                       value="{{old('lastname') ? old('lastname') : ( ($admin->lastname) ? $admin->lastname : '' )}}"
                                       @endif
                                       name="lastname">
                                @if($errors->has('lastname'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Email:</label>
                                <input type="email" id="email" class="form-control"
                                       @if(isset($admin->email))
                                       value="{{old('email') ? old('email') : ( ($admin->email) ? $admin->email : '' )}}"
                                       @endif
                                       placeholder="email" name="email">
                                @if($errors->has('email'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>Skype:</label>
                                <input type="text" id="skype" class="form-control"
                                       @if(isset($admin->skype))
                                       value="{{old('skype') ? old('skype') : ( ($admin->skype) ? $admin->skype : '' )}}"
                                       @endif
                                       name="skype" placeholder="Skype">
                                @if($errors->has('skype'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('skype') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Phone:</label>
                                <input type="text" id="phone" class="form-control"
                                       @if(isset($admin->phone))
                                       value="{{old('phone') ? old('phone') : ( ($admin->phone) ? $admin->phone : '' )}}"
                                       @endif
                                       name="phone" placeholder="Phone">
                                @if($errors->has('phone'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>Mobile:</label>
                                <input type="text" id="mobile" class="form-control"
                                       @if(isset($admin->mobile))
                                       value="{{old('mobile') ? old('mobile') : ( ($admin->mobile) ? $admin->mobile : '' )}}"
                                       @endif
                                       name="mobile" placeholder="Mobile">
                                @if($errors->has('mobile'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>ProfileImage:</label>
                                <input type="file" id="profile1" class="form-control" name="profile1">
                            </div>
                            <div class="col-md-6 unique">
                                <label>Designation:</label>
                                <select name="designation" class="form-control">
                                    @if($admin->au_ar_id == 1)
                                    <option disabled>SELECT DESIGNATION</option>
                                    <option value="1" selected>OVERALL ADMIN</option>
                                    <option value="2">CAMPAIGN MANAGER</option>
                                    <option value="3">SALES MANAGER</option>
                                    @elseif($admin->au_ar_id == 2)
                                        <option disabled>SELECT DESIGNATION</option>
                                        <option value="1">OVERALL ADMIN</option>
                                        <option value="2" selected>CAMPAIGN MANAGER</option>
                                        <option value="3">SALES MANAGER</option>
                                    @elseif($admin->au_ar_id == 3)
                                        <option disabled>SELECT DESIGNATION</option>
                                        <option value="1">OVERALL ADMIN</option>
                                        <option value="2">CAMPAIGN MANAGER</option>
                                        <option value="3" selected>SALES MANAGER</option>
                                    @else
                                        <option disabled selected>SELECT DESIGNATION</option>
                                        <option value="1">OVERALL ADMIN</option>
                                        <option value="2">CAMPAIGN MANAGER</option>
                                        <option value="3">SALES MANAGER</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Edit Admin ">
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.layouts.sidebar')
@include('backend.layouts.foot')
@include('backend.pages.admins.script')
