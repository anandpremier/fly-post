<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function deleteAdmin(input) {
        var id = input;
        var url = "{{route('admin.destroy.admin', ":id")}}";
        url = url.replace(':id', id);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Admin!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                        },
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            });
    }
</script>
<script>
    $(document).ready(function () {
        $('#registerForm').validate({ // initialize the plugin
            rules: {
                user_name: {
                    required: true,
                },
                email:{
                    required:true,
                    email:true
                },
                firstname:{
                    required:true
                },
                lastname:{
                    required:true
                },
                skype:{
                    required:true
                },
                phone:{
                    required:true,
                    number:true,
                    minlength:10,
                    maxlength:10,
                },
                mobile:{
                    required:true,
                    number:true,
                    minlength:10,
                    maxlength:10
                },
                pass:{
                    required:true,
                    minlength:6
                },
                retype_pass:{
                    equalTo:"#pass",
                    required:true,
                    minlength:6
                },
                profile:{
                    required:true,
                    extension: "jpeg|jpg|png",
                    maxsize:200000
                },
                terms:{
                    required:true,
                },
                designation:{
                    required:true,
                    integer:true,
                }
            },
            messages:{
                user_name: {
                    required: "Please Enter UserName",
                },
                email:{
                    required:"Please Enter Email-Address Properly",
                    email:"Please Enter Valid Email-Address"
                },
                firstname:{
                    required:"Please Enter FirstName"
                },
                lastname:{
                    required:"Please Enter LastName"
                },
                skype:{
                    required:"Please Enter SkypeId"
                },
                phone:{
                    required:"Please Enter Phone-Number",
                    number:"Please Enter Valid Phone-Number",
                    minlength:"Please Enter 10 digit Valid Phone-Number",
                    maxlength: "Please Enter 10 digit Valid Phone-Number",
                },
                mobile:{
                    required:"Please Enter Mobile-Number",
                    number:"Please Enter Valid Mobile-Number",
                    minlength:"Please Enter 10 digit Valid Mobile-Number",
                    maxlength:"Please Enter 10 digit Valid Mobile-Number",
                },
                pass:{
                    required:"Please Enter Password",
                    minlength:"InValid Password Length(6-Required)"
                },
                retype_pass:{
                    equalTo:"Confirm Password Not Matched",
                    required:"Please Enter Confirm Password",
                    minlength:"InValid Password Length(6-Required)"
                },
                profile:{
                    required:"Please Select Profile Image",
                    extension:"Invalid File (PNG|JPEG|JPG - Required)",
                    maxsize:"Please Select 2MB Image"
                },
                terms:{
                    required:"Terms And Condition Required"
                },
                designation:{
                    required:'Designation is Required For Admin',
                    integer:'Invalid Selection',
                }
            }
        });
    });
</script>
