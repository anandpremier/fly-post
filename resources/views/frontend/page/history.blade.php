@extends('frontend.layouts.app')
@section('content')
    <div class="col-md-9">
        <div class="table-responsive data-table-wrapper">
            <table id="example" class="ui celled table" style="text-align-last: center;width: 1356px;">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Campaign Title</th>
                    <th>Remuneration</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;?>
                @foreach($campaign_shared as $data)
                    <tr>
                        <th class="col-md-1" scope="row">{{$i}}</th>
                        <td class="col-md-3">{{$data->title}}</td>
                        <td class="col-md-3">{{$data->remuneration}}</td>
                    </tr>
                    <?php $i++;?>
                @endforeach
                </tbody>
                {{--<tfoot>
                <tr>
                    <th>Id</th>
                    <th>Campaign Title</th>
                    <th>Remuneration</th>
                </tr>
                </tfoot>--}}
            </table>
        </div>
    </div>
@endsection

