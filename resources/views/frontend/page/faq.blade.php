
@extends('frontend.layouts.app')


@section('content')
    <section class="accordion-section clearfix mt-3" aria-label="Question Accordions">
  <div class="container">
  
	  <h2>Frequently Asked Questions </h2>
          
          @foreach($influnceFaqget as $influnceFaqgetData)
	  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
		  <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
			<h3 class="panel-title">
			  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$influnceFaqgetData->id}}" aria-expanded="true" aria-controls="collapse{{$influnceFaqgetData->id}}">
				{{$influnceFaqgetData->question}}
			  </a>
			</h3>
		  </div>
		  <div id="collapse{{$influnceFaqgetData->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading0">
			<div class="panel-body px-3 mb-4">
                             {!! $influnceFaqgetData->answer !!}
			  			  
			</div>
		  </div>
		</div>
		
	  </div>
          @endforeach
  
  </div>
</section>
@stop

