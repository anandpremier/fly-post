@extends('frontend.layouts.app')
@section('content')
    <div class="container">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(!isset($shares_fb) && !isset($shares_twitter))
                    @foreach($campaignList as $campaignListData)
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h2>
                                    {{$campaignListData['title']}}
                                </h2>
                            </div>
                            <div class="panel-body">
                                <img src="{{ asset('campaignImages/') }}/{{$campaignListData['image']}}" style="height: 221px;width: 300px;">
                                <table class = "table table-hover customTable">
                                    <tr style="font-size: 23px;">
                                        <td><strong>{!! $campaignListData['name'] !!}</strong></td>
                                        <td><strong>{!! $campaignListData['remuneration'] !!}</strong></td>
                                    </tr>

                                    <tr style="font-size: 23px;">
                                        <td>
                                            <strong>
                                                <a target="_blank" style="text-decoration: none;color: black" href="{!! $campaignListData['link'] !!}">
                                                    {!! $campaignListData['link'] !!}
                                                </a>
                                            </strong>
                                        </td>
                                        <td><strong>{!!  $campaignListData['sponsored_post'] !!}</strong></td>
                                    </tr>
                                    <tr style="font-size: 23px;">
                                        <td><button id="fb{{$campaignListData['crone_mail_id']}}" onClick="shareFunction({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-primary"><i class="fa fa-facebook "></i> Share on Facebook</button></td>
                                        <td><button id="twitter{{$campaignListData['crone_mail_id']}}"onClick="shareTweetPopup({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-info"><i class="fa fa-twitter"></i> Share on Twitter</button></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @endforeach
                @elseif(isset($shares_fb) && isset($shares_twitter))
                    @foreach($campaignList as $campaignListData)
                        @if(in_array($campaignListData->id, $shares_twitter) && in_array($campaignListData->id, $shares_fb))
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h2>
                                        {{$campaignListData['title']}}
                                    </h2>
                                </div>
                                <div class="panel-body">
                                    <img src="{{ asset('campaignImages/') }}/{{$campaignListData['image']}}" style="height: 221px;width: 300px;">
                                    <table class = "table table-hover customTable">
                                        <tr style="font-size: 23px;">
                                            <td><strong>{!! $campaignListData['name'] !!}</strong></td>
                                            <td><strong>{!! $campaignListData['remuneration'] !!}</strong></td>
                                        </tr>

                                        <tr style="font-size: 23px;">
                                            <td>
                                                <strong>
                                                    <a target="_blank" style="text-decoration: none;color: black" href="{!! $campaignListData['link'] !!}">
                                                        {!! $campaignListData['link'] !!}
                                                    </a>
                                                </strong>
                                            </td>
                                            <td><strong>{!!  $campaignListData['sponsored_post'] !!}</strong></td>
                                        </tr>
                                        <tr style="font-size: 23px;">
                                            <td><button id="fb{{$campaignListData['crone_mail_id']}}" type="button" class="btn btn-success"><i class="fa fa-facebook "></i> Shared on Facebook</button></td>
                                            <td><button id="twitter{{$campaignListData['crone_mail_id']}}" type="button" class="btn btn-success"><i class="fa fa-twitter"></i> Shared on Twitter</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @elseif(!in_array($campaignListData->id, $shares_twitter) && !in_array($campaignListData->id, $shares_fb))
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h2>
                                        {{$campaignListData['title']}}
                                    </h2>
                                </div>
                                <div class="panel-body">
                                    <img src="{{ asset('campaignImages/') }}/{{$campaignListData['image']}}" style="height: 221px;width: 300px;">
                                    <table class = "table table-hover customTable">
                                        <tr style="font-size: 23px;">
                                            <td><strong>{!! $campaignListData['name'] !!}</strong></td>
                                            <td><strong>{!! $campaignListData['remuneration'] !!}</strong></td>
                                        </tr>

                                        <tr style="font-size: 23px;">
                                            <td>
                                                <strong>
                                                    <a target="_blank" style="text-decoration: none;color: black" href="{!! $campaignListData['link'] !!}">
                                                        {!! $campaignListData['link'] !!}
                                                    </a>
                                                </strong>
                                            </td>
                                            <td><strong>{!!  $campaignListData['sponsored_post'] !!}</strong></td>
                                        </tr>
                                        <tr style="font-size: 23px;">
                                            <td><button id="fb{{$campaignListData['crone_mail_id']}}" onClick="shareFunction({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-primary"><i class="fa fa-facebook "></i> Share on Facebook</button></td>
                                            <td><button id="twitter{{$campaignListData['crone_mail_id']}}"onClick="shareTweetPopup({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-info"><i class="fa fa-twitter"></i> Share on Twitter</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @elseif(!in_array($campaignListData->id, $shares_fb) && in_array($campaignListData->id, $shares_twitter))
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h2>
                                        {{$campaignListData['title']}}
                                    </h2>
                                </div>
                                <div class="panel-body">
                                    <img src="{{ asset('campaignImages/') }}/{{$campaignListData['image']}}" style="height: 221px;width: 300px;">
                                    <table class = "table table-hover customTable">
                                        <tr style="font-size: 23px;">
                                            <td><strong>{!! $campaignListData['name'] !!}</strong></td>
                                            <td><strong>{!! $campaignListData['remuneration'] !!}</strong></td>
                                        </tr>

                                        <tr style="font-size: 23px;">
                                            <td>
                                                <strong>
                                                    <a target="_blank" style="text-decoration: none;color: black" href="{!! $campaignListData['link'] !!}">
                                                        {!! $campaignListData['link'] !!}
                                                    </a>
                                                </strong>
                                            </td>
                                            <td><strong>{!!  $campaignListData['sponsored_post'] !!}</strong></td>
                                        </tr>
                                        <tr style="font-size: 23px;">
                                            <td><button id="fb{{$campaignListData['crone_mail_id']}}" onClick="shareFunction({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-primary"><i class="fa fa-facebook "></i> Share on Facebook</button></td>
                                            <td><button id="twitter{{$campaignListData['crone_mail_id']}}"type="button" class="btn btn-success"><i class="fa fa-twitter"></i> Shared on Twitter</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @else
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h2>
                                        {{$campaignListData['title']}}
                                    </h2>
                                </div>
                                <div class="panel-body">
                                    <img src="{{ asset('campaignImages/') }}/{{$campaignListData['image']}}" style="height: 221px;width: 300px;">
                                    <table class = "table table-hover customTable">
                                        <tr style="font-size: 23px;">
                                            <td><strong>{!! $campaignListData['name'] !!}</strong></td>
                                            <td><strong>{!! $campaignListData['remuneration'] !!}</strong></td>
                                        </tr>

                                        <tr style="font-size: 23px;">
                                            <td>
                                                <strong>
                                                    <a target="_blank" style="text-decoration: none;color: black" href="{!! $campaignListData['link'] !!}">
                                                        {!! $campaignListData['link'] !!}
                                                    </a>
                                                </strong>
                                            </td>
                                            <td><strong>{!!  $campaignListData['sponsored_post'] !!}</strong></td>
                                        </tr>
                                        <tr style="font-size: 23px;">
                                            <td><button id="fb{{$campaignListData['crone_mail_id']}}" type="button" class="btn btn-success"><i class="fa fa-facebook "></i> Shared on Facebook</button></td>
                                            <td><button id="twitter{{$campaignListData['crone_mail_id']}}" onClick="shareTweetPopup({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-success"><i class="fa fa-twitter"></i> Shared on Twitter</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @elseif(isset($shares_fb))
                    @foreach($campaignList as $campaignListData)
                        @if(in_array($campaignListData->id, $shares_fb))
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h2>
                                        {{$campaignListData['title']}}
                                    </h2>
                                </div>
                                <div class="panel-body">
                                    <img src="{{ asset('campaignImages/') }}/{{$campaignListData['image']}}" style="height: 221px;width: 300px;">
                                    <table class = "table table-hover customTable">
                                        <tr style="font-size: 23px;">
                                            <td><strong>{!! $campaignListData['name'] !!}</strong></td>
                                            <td><strong>{!! $campaignListData['remuneration'] !!}</strong></td>
                                        </tr>

                                        <tr style="font-size: 23px;">
                                            <td>
                                                <strong>
                                                    <a target="_blank" style="text-decoration: none;color: black" href="{!! $campaignListData['link'] !!}">
                                                        {!! $campaignListData['link'] !!}
                                                    </a>
                                                </strong>
                                            </td>
                                            <td><strong>{!!  $campaignListData['sponsored_post'] !!}</strong></td>
                                        </tr>
                                        <tr style="font-size: 23px;">
                                            <td><button id="fb{{$campaignListData['crone_mail_id']}}" type="button" class="btn btn-success"><i class="fa fa-facebook "></i> Shared on Facebook</button></td>
                                            <td><button id="twitter{{$campaignListData['crone_mail_id']}}" onClick="shareTweetPopup({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-info"><i class="fa fa-twitter"></i> Share on Twitter</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @else
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h2>
                                        {{$campaignListData['title']}}
                                    </h2>
                                </div>
                                <div class="panel-body">
                                    <img src="{{ asset('campaignImages/') }}/{{$campaignListData['image']}}" style="height: 221px;width: 300px;">
                                    <table class = "table table-hover customTable">
                                        <tr style="font-size: 23px;">
                                            <td><strong>{!! $campaignListData['name'] !!}</strong></td>
                                            <td><strong>{!! $campaignListData['remuneration'] !!}</strong></td>
                                        </tr>

                                        <tr style="font-size: 23px;">
                                            <td>
                                                <strong>
                                                    <a target="_blank" style="text-decoration: none;color: black" href="{!! $campaignListData['link'] !!}">
                                                        {!! $campaignListData['link'] !!}
                                                    </a>
                                                </strong>
                                            </td>
                                            <td><strong>{!!  $campaignListData['sponsored_post'] !!}</strong></td>
                                        </tr>
                                        <tr style="font-size: 23px;">
                                            <td><button id="fb{{$campaignListData['crone_mail_id']}}" onClick="shareFunction({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-primary"><i class="fa fa-facebook "></i> Share on Facebook</button></td>
                                            <td><button id="twitter{{$campaignListData['crone_mail_id']}}" onClick="shareTweetPopup({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-info"><i class="fa fa-twitter"></i> Share on Twitter</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @else
                    @foreach($campaignList as $campaignListData)
                        @if(in_array($campaignListData->id, $shares_twitter))
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h2>
                                        {{$campaignListData['title']}}
                                    </h2>
                                </div>
                                <div class="panel-body">
                                    <img src="{{ asset('campaignImages/') }}/{{$campaignListData['image']}}" style="height: 221px;width: 300px;">
                                    <table class = "table table-hover customTable">
                                        <tr style="font-size: 23px;">
                                            <td><strong>{!! $campaignListData['name'] !!}</strong></td>
                                            <td><strong>{!! $campaignListData['remuneration'] !!}</strong></td>
                                        </tr>

                                        <tr style="font-size: 23px;">
                                            <td>
                                                <strong>
                                                    <a target="_blank" style="text-decoration: none;color: black" href="{!! $campaignListData['link'] !!}">
                                                        {!! $campaignListData['link'] !!}
                                                    </a>
                                                </strong>
                                            </td>
                                            <td><strong>{!!  $campaignListData['sponsored_post'] !!}</strong></td>
                                        </tr>
                                        <tr style="font-size: 23px;">
                                            <td><button id="fb{{$campaignListData['crone_mail_id']}}"onClick="shareFunction({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-primary"><i class="fa fa-facebook "></i> Share on Facebook</button></td>
                                            <td><button id="twitter{{$campaignListData['crone_mail_id']}}" type="button" class="btn btn-success"><i class="fa fa-twitter"></i> Shared on Twitter</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @else
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h2>
                                        {{$campaignListData['title']}}
                                    </h2>
                                </div>
                                <div class="panel-body">
                                    <img src="{{ asset('campaignImages/') }}/{{$campaignListData['image']}}" style="height: 221px;width: 300px;">
                                    <table class = "table table-hover customTable">
                                        <tr style="font-size: 23px;">
                                            <td><strong>{!! $campaignListData['name'] !!}</strong></td>
                                            <td><strong>{!! $campaignListData['remuneration'] !!}</strong></td>
                                        </tr>

                                        <tr style="font-size: 23px;">
                                            <td>
                                                <strong>
                                                    <a target="_blank" style="text-decoration: none;color: black" href="{!! $campaignListData['link'] !!}">
                                                        {!! $campaignListData['link'] !!}
                                                    </a>
                                                </strong>
                                            </td>
                                            <td><strong>{!!  $campaignListData['sponsored_post'] !!}</strong></td>
                                        </tr>
                                        <tr style="font-size: 23px;">
                                            <td><button id="fb{{$campaignListData['crone_mail_id']}}" onClick="shareFunction({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-primary"><i class="fa fa-facebook "></i> Share on Facebook</button></td>
                                            <td><button id="twitter{{$campaignListData['crone_mail_id']}}"onClick="shareTweetPopup({{ $campaignListData['crone_mail_id']}})" type="button" class="btn btn-info"><i class="fa fa-twitter"></i> Share on Twitter</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection
