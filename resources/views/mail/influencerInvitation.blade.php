@extends('mail.layouts.app')

@section('content')
    <div class="content">
        <div>
            Hello!<br>
            Click here to confirm your account:
        </div>

        <a href="{{$verify_mail}}" style="text-decoration: none;">

            <div align="center" style="margin: 12px;
        height: 32px;
        color: #fff;
        background-color: #5cb85c;
        border-color: #4cae4c;
        width: 138px;
        padding-top: 13px;
        margin-left: 115px;
        border-radius: 12px;">
                Click HERE
            </div>

        </a>
        Thank you for using our application!
        @yield('title', config('app.name'))
    </div>
@endsection

