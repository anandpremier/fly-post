
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<div class="container">
    <h2>Available Campaign For You</h2>
    <div class="card">
        @foreach($campaigns as $data)
            <div class="card-header">{{$data->title}}</div>
            <div class="card-body"><img src="{{ asset('campaignImages/') }}/{{$data->image}}"></div>
            <br><br><br>
        @endforeach
    </div>
</div>
