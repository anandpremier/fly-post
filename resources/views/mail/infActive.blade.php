@extends('mail.layouts.app')

@section('content')
    <div class="content">
        <div style="width:700px;color: black;background-color: deepskyblue;border-color: #d6e9c6;padding-top: 9px;padding-bottom: 12px;padding-left: 26px;">
            Hello!! <strong>{{$Rows[0]->firstname}} {{$Rows[0]->lastname}}</strong><br><br>
            Congratulation !! Your Account Was Confirmed By Admin<br><br>
            You Can Now Access Every Services Of The Application
        </div>
        </a><br>
        <div>
            Thank you for using our application!
            @yield('title', config('app.name'))
        </div>
    </div>
@endsection

