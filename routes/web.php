<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Routing\Route;
//use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->to('/login');
})->name('index');


Route::get('password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);

Route::get('/campaign', 'HomeController@index')->name('campaign');
Route::get('/influencer/account/{token}', 'frontend\InfluenceruserController@updateAccountStatus')->name('updateStatusAccount');

Route::get('connect', 'backend\TwitterController@connect');
Route::get('connectfb', 'backend\FacebookController@connect');
Route::post('getFacebookSession', 'backend\FacebookController@sessionData');
Route::post('getFacebookuser', 'backend\FacebookController@getFacebookuser');
Route::get('/redirecttwitter', 'SocialAuthTwitterController@redirect');
Route::get('/callbackUrl', 'SocialAuthTwitterController@callbackUrl');
Route::post('getSession', 'backend\TwitterController@sessionData');
Route::get('twittercallback', 'backend\TwitterController@callback')->name('twitter.callback');

Route::group(['namespace'=>'Auth'], function () {
    Route::post('/register/influencer', 'RegisterController@store')->name('register.influencer.user');  
    Route::get('/mail', 'RegisterController@mail')->name('mail');
    Route::get('/aa', 'RegisterController@aa')->name('aa');
    Route::get('/verifyMail', 'RegisterController@verifyMail')->name('verifyMail');
    Route::get('/adminVerifyMail', 'RegisterController@adminVerifyMail')->name('adminVerifyMail');
    Route::get('/influencer/register/', 'RegisterController@showRegistrationForm')->name('registerInf');
    Route::get('/redirect', 'FacebookController@redirect');
    Route::get('/callback', 'FacebookController@callback');
    Route::get('/city', 'RegisterController@getCity')->name('getCity');
});
Auth::routes();

Route::group(['middleware' => 'influencerUser','namespace'=>'frontend'], function () {
    Route::get('influencer/history/', 'InfluenceruserController@history')->name('influenceruser.history');
    Route::get('edit/profile/{id}', 'InfluenceruserController@edit')->name('influenceruser.edit');
    Route::post('/edit', 'InfluenceruserController@update')->name('influenceruser.update');
    Route::post('influencer/cp/', 'InfluenceruserController@changePassword')->name('influenceruser.cp');
    Route::get('facebookShare/verify/','InfluenceruserController@shareVerify')->name('facebookShare.verify');
    Route::post('twitter/verify/', 'InfluenceruserController@shareVerify')->name('twitter.verify');
    Route::post('fetch/cities/', 'InfluenceruserController@fetchCities')->name('influenceruser.fetchCities');
    Route::get('faq', 'InfluenceruserController@influenceruserFaq')->name('faq');
});

//Route::get('getFeed', 'backend\FacebookController@getFeed');
Route::group(['middleware' => 'influencerUser','namespace'=>'backend'], function () {
    Route::get('sharePost', 'FacebookController@shareCamping')->name('share');
    Route::get('shareTweet', 'TwitterController@shareTweet')->name('shareTweet');
    Route::get('postTweet/{id}', 'TwitterController@postTweet')->name('postTweet');
    Route::get('shareValidate', 'TwitterController@validateTweet')->name('validateTweet');
    Route::get('error', 'TwitterController@error')->name('twitter.error');
    Route::get('add_channel', 'TwitterController@addChannel')->name('twitter.add');
    Route::get('/remove/fb', 'FacebookController@removeChannel')->name('remove.fb.channel');
    Route::get('/remove/twitter', 'TwitterController@removeChannel')->name('remove.twitter.channel');
    Route::get('/remove_session', 'TwitterController@removeSession')->name('remove_session');
    Route::get('/remove_session_fb', 'FacebookController@removeSession')->name('remove_session_fb');
    Route::get('editconnect', 'TwitterController@editConnect');
    Route::get('/add/fb/channel', 'FacebookController@getInfo');
    Route::get('/add/channel/fb', 'FacebookController@addChannel')->name('fb.add');
});
    /*================================================================================================*/
    /*========================BACKEND========================================================================*/
    /*========================ROUTES========================================================================*/
    /*========================FROM========================================================================*/
    /*========================HERE========================================================================*/
    /*================================================================================================*/
Route::group(['namespace'=>'Auth','prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', 'AdminLoginController@showLoginForm')->name('show.LoginForm');
    Route::post('/login/validate', 'AdminLoginController@login')->name('login.validate');
    /*================================================================================================*/
    Route::get('/register', function (){
        return redirect()->route('admin.show.LoginForm');
    })->name('register.form');
    Route::post('/register/store', 'AdminRegisterController@store')->name('register.store');
    /*================================================================================================*/
    Route::get('/password/reset', 'AdminForgotPasswordController@index')->name('reset.form');
    Route::post('/email/reset', 'AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset/{token}', 'AdminResetPasswordController@showResetForm')->name('password.reset.link');
    Route::post('/update/reset', 'AdminResetPasswordController@reset')->name('password.update');
    Route::post('/logout', 'AdminLoginController@logout')->name('user.logout');
});

    /*=============================Routes With===================================================================*/
    /*=============================MiddleWare===================================================================*/
Route::group(['middleware' => 'AdminUser','namespace'=>'backend','prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/dashboard', 'AdminUsersController@showDashboard')->name('dashboard');
    /*================================COUNTRY CRUD ROUTES ============================*/
    Route::get('/country','CountryController@index')->name('get.country');
    Route::get('/create/country', 'CountryController@create')->name('create.country');
    Route::post('/store/country', 'CountryController@store')->name('register.country');
    Route::get('/edit/country/{id}', 'CountryController@edit')->name('edit.country');
    Route::post('/delete/country/{id}', 'CountryController@delete')->name('destroy.country');
    /*================================INFLUENCERS USER CRUD ROUTES ============================*/
    Route::get('/users/influencers','InfluencersController@index')->name('getInfluencers');
    Route::get('/create/influencers', 'InfluencersController@create')->name('create.influencers');
    Route::post('/store/influencers', 'InfluencersController@store')->name('register.influencers');
    Route::get('/edit/influencers/{id}', 'InfluencersController@edit')->name('edit.influencers');
    Route::get('/history/influencers/{id}', 'InfluencersController@history')->name('history.influencers');
    Route::post('/delete/influencers/{id}', 'InfluencersController@delete')->name('destroy.influencers');
    Route::post('/status/influencers/{id}', 'InfluencersController@changeStatus')->name('update.status.influencers');
    /*================================ADMIN USER CRUD ROUTES ============================*/
    Route::get('/users/admin','AdminUsersController@index')->name('get.admin');
    Route::get('/create/admin', 'AdminUsersController@create')->name('create.admin');
    Route::post('/store/admin', 'AdminUsersController@store')->name('register.admin');
    Route::get('/edit/admin/{id}', 'AdminUsersController@edit')->name('edit.admin');
    Route::post('/delete/admin/{id}', 'AdminUsersController@delete')->name('destroy.admin');
    /*================================CAMPAIGN CRUD ROUTES ============================*/
    Route::get('/list/campaign', 'CampaignController@index')->name('get.campaign');
    Route::get('/create/campaign', 'CampaignController@create')->name('create.campaign');
    Route::post('/store/campaign', 'CampaignController@store')->name('register.campaign');
    Route::get('/edit/campaign/{id}', 'CampaignController@edit')->name('edit.campaign');
    Route::get('/manage/campaign/{id}', 'CampaignController@manage')->name('manage.campaign');
    Route::post('/manage/campaign/store', 'CampaignController@manageStore')->name('manage.campaign.store');
    Route::post('/delete/campaign/{id}', 'CampaignController@delete')->name('destroy.campaign');
    Route::post('/fetch/cities/{id}', 'CampaignController@fetchCities')->name('fetch.cities');
    Route::post('/fetch/influencers', 'CampaignController@fetchInfluencers')->name('fetch.influencers');
    Route::post('/manage/batchSize', 'CampaignController@batchSize')->name('batchsize.calculate');
    /*================================EMAIL=========== ============================*/
    Route::get('/cms/e-mail', 'emailController@index')->name('email.index');
    Route::get('/cms/e-mail/create', 'emailController@create')->name('email.create');
    Route::post('/cms/e-mail/store', 'emailController@store')->name('email.store');
    Route::post('/cms/e-mail/delete/{id}', 'emailController@delete')->name('destroy.mail');
    Route::get('/cms/e-mail/edit/{id}', 'emailController@edit')->name('edit.email');
    /*================================MAIL Templates ============================*/
    Route::get('/cms/mail/template/list', 'EmailTemplateController@index')->name('mail.get');
    Route::get('/cms/mail/template', 'EmailTemplateController@create')->name('email.template');
    Route::get('/cms/mail/edit/{id}', 'EmailTemplateController@edit')->name('email.template.edit');
    Route::post('/cms/mail/store', 'EmailTemplateController@store')->name('email.template.store');
    Route::post('/cms/mail/delete/{id}', 'EmailTemplateController@delete')->name('email.template.destroy');
    /*================================FAQ's Templates ============================*/
    Route::get('/cms/faq/list', 'FaqController@index')->name('faq.get');
    Route::get('/cms/faq/create', 'FaqController@create')->name('faqs.create');
    Route::post('/cms/faq/store', 'FaqController@store')->name('faqs.store');
    Route::get('/cms/faq/{id}', 'FaqController@edit')->name('edit.faqs');
    Route::post('/cms/faq/delete/{id}', 'FaqController@delete')->name('destroy.faqs');
});

