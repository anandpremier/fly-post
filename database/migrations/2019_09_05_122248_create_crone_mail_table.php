<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCroneMailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crone_mail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->nullable();
            $table->string('subject')->nullable();
            $table->string('body')->nullable();
            $table->integer('influencer_id')->nullable();
            $table->integer('campaign_id')->nullable();
            $table->timestamp('executedate')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->tinyInteger('reminder')->default('0');
            $table->timestamps();

            $table->foreign('influencer_id')
                ->references('id')->on('influencerusers')
                ->onDelete('cascade');

            $table->foreign('campaign_id')
                ->references('id')->on('campaign')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crone_mail');
    }
}
