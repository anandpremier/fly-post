<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('admin_id')->unsigned();
            $table->date('campaign_start');
            $table->date('campaign_end');
            $table->tinyInteger('campaign_type')->default('1')->comment('
                1-Social Seeding
                2-Social Performance Seeding
                3-Video Seeding
            ');
            $table->text('message');
            $table->string('sponsored_post');
            $table->string('link');
            $table->string('image');
            $table->string('title');
            $table->string('name')->nullable();
            $table->integer('age_from')->nullable();
            $table->integer('age_to')->nullable();
            $table->integer('gender')->comment('0-Female,1-Male');
            $table->tinyInteger('status')->default('0')->comment('0= Waiting for approval,1 = Approved but not Started,2 = active/Approved,3 = finished,4=Not approved,5= sending_mail_paused,6= paused');
            $table->string('not_approve_msg')->nullable();
            $table->integer('available_influencer')->comment('available_influencer')->nullable();
            $table->string('url')->nullable()->comment('url of landing page');
            $table->integer('batch_size')->nullable();
            $table->integer('cron_interval')->nullable();
            $table->dateTime('finished')->nullable();
            $table->float('remuneration',10,2)->default('0');
            $table->integer('number_of_shares')->default('10000');
            $table->integer('shares_over_performance')->default('0');
            $table->float('comment_remuneration',10,2)->nullable();
            $table->integer('comment_number')->nullable();
            $table->integer('comment_number_current')->nullable()->default('0');
            $table->integer('created_by')->nullable();
            $table->timestamp('created_on')->useCurrent();
            $table->integer('deleted_by')->nullable();
            $table->timestamp('deleted_on')->nullable()->default(null);
            $table->string('states')->nullable();
            $table->string('cities')->nullable();
            $table->string('channel')->nullable();
            $table->integer('deleted')->nullable();
            $table->string('ca_cm_code')->nullable();
            $table->dateTime('send_mail_until')->nullable();
            $table->timestamps();

            $table->foreign('admin_id')
                    ->references('id')->on('adminuser')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
}
