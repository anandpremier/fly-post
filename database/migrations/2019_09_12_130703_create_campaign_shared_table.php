<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignSharedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_shared', function (Blueprint $table) {
             $table->bigIncrements('id');
             $table->integer('ca_id')->unsigned();
             $table->integer('influencerusers_id')->unsigned();
             $table->tinyInteger('first_channel_shared')->nullable()->comment = '1=fb,2=twitter';
             $table->tinyInteger('shared_on_fb')->default('0')->nullable();
             $table->tinyInteger('shared_on_twitter')->default('0')->nullable();
             $table->float('share_price',10,2)->default('0.00')->nullable();
             $table->float('comment_remuneration',10,2)->nullable();
             $table->float('sharing_comment',10,2)->nullable();
             $table->tinyInteger('ispaid')->default('0')->nullable()->comment = '0= not paid, 1= payment is done';
             $table->tinyInteger('comment_checked')->default('0')->nullable()->comment = '0 = not checked, 1 = checked';
             $table->integer('pa_id')->unsigned()->nullable();
             $table->tinyInteger('device')->default('2')->nullable()->comment = '0 = mobile, 1 = tablet, 2 = Laptop/Desktop';
             $table->string('post_url_fb',255)->nullable();
             $table->string('post_url_twitter',255)->nullable();
             $table->tinyInteger('post_DeletedOrNotPublic')->default('0')->nullable()->comment = '0 = not deleted, 1 = deleted';
             $table->tinyInteger('deletedpost_remm_deducted')->default('0')->nullable()->comment = '0 = not deducted, 1= deducted';
             $table->timestamp('deleted_post_check_on')->nullable();
             $table->timestamps();
             
             $table->foreign('ca_id')
                    ->references('id')->on('campaign')
                    ->onDelete('cascade');
             
             $table->foreign('influencerusers_id')
                    ->references('id')->on('influencerusers')
                    ->onDelete('cascade');
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_shared');
    }
}
