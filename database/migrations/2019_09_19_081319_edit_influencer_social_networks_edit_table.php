<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditInfluencerSocialNetworksEditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('influencer_social_networks', function (Blueprint $table) {
        $table->string('channel_accesstoken',255)->nullable()->change();
        $table->string('twitter_accesstoken_secret',255)->nullable()->change();
        
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
