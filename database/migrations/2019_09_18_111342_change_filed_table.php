<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFiledTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
    Schema::table('influencerusers', function (Blueprint $table) {
        $table->string('password')->nullable()->change();
        $table->string('date_of_birth')->nullable()->change();
        $table->string('country')->nullable()->change();
        $table->string('gender')->nullable()->change();
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
