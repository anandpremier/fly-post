<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencerSocialNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('influencer_social_networks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('influencer_id');
            $table->tinyInteger('channel_id');
            $table->string('channelname');
            $table->integer('channel_userid');
            $table->tinyInteger('isapporved')->comment('0-not_apporved, 1-approved, 2-pending');
            $table->text('reason_notapproved');
            $table->string('channel_accesstoken');
            $table->string('twitter_accesstoken_secret');
            $table->timestamp('channel_approval_date');
            $table->timestamps();
            
             $table->foreign('influencer_id')
                    ->references('id')->on('influencerusers')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_social_networks');
    }
}
