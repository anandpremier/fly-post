<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencerusers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('date_of_birth');
            $table->string('country');
            $table->string('zipcode');
            $table->string('gender')->default(0)->comment('1-male,0-female')->nullable();
            $table->string('terms_and_conditions')->default(0)->comment('1-active,0-deactive')->nullable();
            $table->string('privacy_policy')->default(0)->comment('1-active,0-deactive')->nullable();
            $table->string('status')->default(0)->comment('1-active,0-deactive')->nullable();
            $table->string('email_verify')->default(0)->comment('1-active,0-deactive')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencerUsers');
    }
}
