<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditInfluencerSocialNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::table('influencer_social_networks', function (Blueprint $table) {
        $table->string('isapporved')->default('0')->nullable()->change();
        $table->string('reason_notapproved')->default('0')->nullable()->change();
        $table->string('channel_accesstoken')->nullable()->change();
        $table->string('twitter_accesstoken_secret')->nullable()->change();
        $table->string('channel_approval_date')->nullable()->change();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
