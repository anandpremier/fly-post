<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchedInfluencersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matched_influencers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('campaign_id');
            $table->integer('influencer_id');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            
             $table->foreign('influencer_id')
                ->references('id')->on('influencerusers')
                ->onDelete('cascade');

            $table->foreign('campaign_id')
                ->references('id')->on('campaign')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matched_influencers');
    }
}
