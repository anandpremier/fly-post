<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInfluencerSocialNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('influencer_social_networks', function (Blueprint $table) {
            $table->string('channle_url')->nullable()->after('twitter_accesstoken_secret'); 
            $table->string('user_profile_image')->nullable()->after('twitter_accesstoken_secret');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
