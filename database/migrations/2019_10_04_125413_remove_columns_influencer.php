<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsInfluencer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('influencerusers', function (Blueprint $table) {
            $table->text('reason_notapproved')->nullable();
        });

        Schema::table('influencerusers', function($table) {
            $table->dropColumn('channel');
            $table->dropColumn('provider_user_id');
            $table->dropColumn('provider');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
