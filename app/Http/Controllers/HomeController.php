<?php

namespace App\Http\Controllers;

use App\Model\frontend\InfluencerUsers;
use Illuminate\Http\Request;
use App\Model\backend\crone_mail;
use App\Model\frontend\Campaignshared;
use Illuminate\Support\Facades\Session;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $verify = InfluencerUsers::where('id',Auth::user()->id)->get();
        if($verify[0]->email_verify == 0){
            auth()->logout();
            return redirect()->route('verifyMail');
        }

        if($verify[0]->status == 0){
            auth()->logout();
            $statusAdmin = "Sorry !! Your Account Is Not Approved By Admin.<br /> You'll Get Notified Once Admin Will Approve Your Profile ";
            return view('auth.login', compact('statusAdmin'));
        }

        $id = Auth::user()->id;
        Session::put('current_user_id_twitter',$id);

        $campaignList = crone_mail::query()
            ->leftJoin('campaign','campaign.id','=','cron_mail.campaign_id')
            ->where('cron_mail.influencer_id',$id)
            ->where('cron_mail.status',1)
            ->where('campaign.status',2)
            ->select([
                'cron_mail.id AS crone_mail_id',
                'campaign.*'
            ])
            ->get();

        $i = 0;
        foreach($campaignList as $camps)
        {
            $shared_fb = Campaignshared::query()
                ->where('ca_id',$camps->id)
                ->where('influencerusers_id',$id)
                ->where('shared_on_fb',1)
                ->get();

            $shared_twitter = Campaignshared::query()
                ->where('ca_id',$camps->id)
                ->where('influencerusers_id',$id)
                ->where('shared_on_twitter',1)
                ->get();

            if($shared_fb->isNotEmpty()){
                $shares_fb[$i] = $camps->id;
                $i++;
            }

            if($shared_twitter->isNotEmpty()){
                $shares_twitter[$i] = $camps->id;
                $i++;
            }
        }
        if(empty($shares_twitter) && empty($shares_fb))
            return view('home', compact('campaignList'));
        elseif(!empty($shares_twitter) && !empty($shares_fb))
            return view('home', compact('shares_fb','shares_twitter','campaignList'));
        elseif(empty($shares_twitter))
            return view('home', compact('shares_fb','campaignList'));
        else
            return view('home', compact('campaignList','shares_twitter'));
    }

    public function file(){
        return view('backend.file');
    }
}
