<?php

namespace App\Http\Controllers\Auth;

use App\Model\backend\states;
use App\Model\frontend\influencerSocial;
use App\Model\frontend\InfluencerUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Session;
class FacebookController extends Controller
{
    public function redirect() {

        return Socialite::driver('facebook')->redirect();
        
    }

    function split_name($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#' . $last_name . '#', '', $name));
        return array($first_name, $last_name);
    }

    public function callback(Request $request) {
        if(isset($request->error)){
            Session::put('facebook_error',true);
            return redirect()->route('register');
        }
        $getInfo = Socialite::driver('facebook')->user();
        Session::put('facebook_info',$getInfo);
        if(Session::has('edit_facebook')){
            return redirect()->route('fb.add');
        }else{
            $inf = influencerSocial::where('channel_userid', $getInfo->id)->first();
            if(is_null($inf)){
                $id = 101;
                $statusList = states::where('country_id', '=', $id)
                    ->get();
                $name = $this->split_name($getInfo->name);
                return view('auth.register', compact('getInfo', 'statusList','name'));
            }else{
                if (Session::has('facebook_oauth_state')){
                    Session::forget('facebook_oauth_state');
                    return redirect()->to('/register');
                }else{
                    $inf = InfluencerUsers::findorfail($inf->influencer_id);
                    auth()->login($inf);
                    return redirect()->to('/campaign');
                }
            }
        }
    }
}
