<?php

namespace App\Http\Controllers\Auth;

use App\Model\backend\AdminUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AdminRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'user_name'=>'required|unique:adminuser,username',
            'firstname'=>'required',
            'lastname'=>'required',
            'email'=>'required|email|unique:adminuser,email',
            'skype'=>'required|unique:adminuser,skype',
            'phone'=>'required|integer|unique:adminuser,phone',
            'mobile'=>'required|integer|unique:adminuser,mobile',
            'pass'=>'required|min:6',
            'retype_pass'=>'required|same:pass|min:6',
            'profile'=>'required',
            'designation'=>'required|integer'
        ]);

        if($request->file('profile') != null)
        {
            $ext = $request->file('profile')->getClientOriginalExtension();
            $size = $request->file('profile')->getSize();

            $file = $request->file('profile');
            $pathfile = md5($file->getClientOriginalName(). time()).".".$ext;
            $file->move(public_path('adminProfile'), $pathfile);
        }

        $obj = new AdminUser();
        $obj->username = $request['user_name'];
        $obj->firstname = $request['firstname'];
        $obj->lastname = $request['lastname'];
        $obj->email = $request['email'];
        $obj->skype = $request['skype'];
        $obj->phone = $request['phone'];
        $obj->mobile = $request['mobile'];
        $obj->password =  Hash::make($request['retype_pass']);
        $obj->au_ar_id = $request['designation'];
        $obj->photo = $pathfile;
        $obj->save();

        toastr()->success('Admin has been Generated successfully!Please Login To Continue');
        return redirect()->route('admin.show.LoginForm');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function showRegisterForm()
    {
        return view('auth.adminRegister');
    }
}
