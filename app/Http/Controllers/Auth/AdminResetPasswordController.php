<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

class AdminResetPasswordController extends Controller{
    use ResetsPasswords;

    protected $redirectTo = '/admin/dashboard';

    public function showResetForm(Request $request, $token = null){
        return view('auth.passwords.adminReset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function broker() {
        return Password::broker('admins');
    }

    protected function guard() {
        return auth()->guard('admin');
    }
}
