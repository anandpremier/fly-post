<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Model\backend\Campaign;
use App\Model\backend\crone_mail;
use App\Model\backend\emailField;
use App\Model\backend\matched_influencer;
use App\User;
use Crypt;
use App\Model\frontend\InfluencerUsers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Model\backend\states;
use App\Model\backend\cities;
use App\Model\frontend\influencerSocial;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Model\backend\emailTemplate;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;
/*use Session;*/
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/campaign';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    public function showRegistrationForm() {
    //     Session::forget('facebook_info');
    //     Session::forget('twitter_token');
    //     Session::forget('facebook_token');
    //    dd(Session::all());

        $name = Session::get('name');
        $getInfo = Session::get('getInfo');

        $id = 101;
        $statusList = states::where('country_id', '=', $id)
            ->get();

        return view('auth.register', compact('statusList', 'name', 'getInfo'));
    }

    public function getCity(Request $request) {
        
        $state = $request->state;
        $cityList = cities::where('state_id', '=', $state)
            ->get();

        return $cityList;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    public function store(Request  $request) {

        try {
            
            $request->validate([
                'firstname' => 'required|string|max:30',
                'lastname' => 'required|string|max:30',
                'email' => 'required|string|email|unique:influencerusers',
                'password_confirmation' => 'required|min:8', 
                'date_of_birth' => 'required',
                'country' => 'required',
                'state' => 'required',
                'city' => 'required',
                'zipcode' => 'required',
                'gender' => 'required',
                'terms_and_conditions' => 'required',
                'privacy_policy' => 'required',
            ]);
            
            if ($request->id != '') {
                $InfluencerUsers = InfluencerUsers::findOrFail($request->id);
            } else {
                $InfluencerUsers = new InfluencerUsers();
            }
            
            if(isset($request->twitteroAuth) && isset($request->facebookoAuth)){
               $userChannel = collect([1, 2]);
            }else if(isset($request->twitteroAuth)){
                $userChannel = collect([2]);
            }else if(isset($request->facebookoAuth)){
                $userChannel = collect([1]);
            }
           
            $InfluencerUsers->firstname = $request['firstname'];
            $InfluencerUsers->lastname = $request['lastname'];
            $InfluencerUsers->email = $request['email'];
            $InfluencerUsers->password =  Hash::make($request['password_confirmation']);
            $InfluencerUsers->date_of_birth = $request['date_of_birth'];
            $InfluencerUsers->country = $request['country'];
            $InfluencerUsers->state = $request['state'];
            $InfluencerUsers->city = $request['city'];
            $InfluencerUsers->zipcode = $request['zipcode'];
            $InfluencerUsers->gender = $request['gender'];
            $InfluencerUsers->channel = $userChannel;
            $InfluencerUsers->terms_and_conditions = $request['terms_and_conditions'];
            $InfluencerUsers->privacy_policy = $request['privacy_policy'];
            $InfluencerUsers->save();
            
      
          if ((!is_null(Session::get('facebook_token')) && !is_null(Session::get('twitter_token')))) {
           
            $array = [
                Session::get('facebook_token'),
                Session::get('twitter_token'),
           ];

         }
         else if(!is_null(Session::get('twitter_token'))){
            $array = [
                Session::get('twitter_token'),
           ];
         }else if(!is_null(Session::get('facebook_token'))){
            $array = [
                Session::get('facebook_token'),
           ];
         }
            foreach($array as $data){
             
                $InfluencerUsersSocial = new influencerSocial();
                $InfluencerUsersSocial->influencer_id = $InfluencerUsers->id;
                $InfluencerUsersSocial->channel_id = $data['channel_id'];
                $InfluencerUsersSocial->channelname = $data['channelname'];
                $InfluencerUsersSocial->channel_userid = $data['user_id'];
                $InfluencerUsersSocial->isapporved = 2;
                $InfluencerUsersSocial->user_profile_image =  $data['user_profile_image'];
                $InfluencerUsersSocial->channle_url =  $data['channle_url'];
                $InfluencerUsersSocial->channel_accesstoken = $data['accessToken'];
                $InfluencerUsersSocial->twitter_accesstoken_secret =  $data['twitter_oauth_token_secret'];                
                $InfluencerUsersSocial->save();
            }
            Session::forget('twitter_token');
            Session::forget('facebook_token');
            Session::save();
           // $email = $request['email'];
          //  $this->mail($email);

            if ($request->id != '') {
                toastr()->success('Influencer  has been updated', 'Successfully updated', ['timeOut' => 5000]);
            } else {
                toastr()->success('Influencer register', 'Customer Created Successfully', ['timeOut' => 5000]);
                return redirect('register');
               }
        } catch (Exception $e) {
            toastr()->warning('', 'Something went wrong', ['timeOut' => 5000]);
        }
      
    }

    // protected function create(array $data) {
      
    //     dd($data);
    //     if (!isset($data['channel_userid']))
    //         $data['channel_userid'] = null;

    //     if(Session::has('access_token'))
    //         $filter['channel_status'] = "1,2";
    //     else
    //         $filter['channel_status'] = "1";

    //     if (isset($data['channel_accesstoken'])) {
    //         $influencer = InfluencerUsers::create([
    //             'firstname' => $data['firstname'],
    //             'lastname' => $data['lastname'],
    //             'email' => $data['email'],
    //             'password' => Hash::make($data['password_confirmation']),
    //             'date_of_birth' => date("Y-m-d", strtotime($data['date_of_birth'])),
    //             'country' => $data['country'],
    //             'state' => $data['state'],
    //             'city' => $data['city'],
    //             'zipcode' => $data['zipcode'],
    //             'gender' => $data['gender'],
    //             'channel' => $filter['channel_status'],
    //             'terms_and_conditions' => $data['terms_and_conditions'],
    //             'privacy_policy' => $data['privacy_policy'],
    //         ]);

    //         $influencerSocial = influencerSocial::create([
    //             'influencer_id' => $influencer->id,
    //             'channel_id' => '1',
    //             'channelname' => 'facebook',
    //             'channel_userid' => $data['channel_userid'],
    //             'isapporved' => '2',
    //             'channel_accesstoken' => $data['channel_accesstoken'],
    //             'twitter_accesstoken_secret' => 'NULL',
    //             'channel_approval_date' => 'NULL'
    //         ]);

    //         if(Session::has('access_token')){
    //             $secret_token = Session::get('access_token');
    //             $influencerSocial = influencerSocial::create([
    //                 'influencer_id' => $influencer->id,
    //                 'channel_id' => '2',
    //                 'channelname' => 'twitter',
    //                 'channel_userid' => $secret_token['user_id'],
    //                 'isapporved' => '2',
    //                 'channel_accesstoken' => $secret_token['oauth_token'],
    //                 'twitter_accesstoken_secret' => $secret_token['oauth_token_secret'],
    //             ]);
    //         }
    //         $userID = $data['channel_userid'];
    //         $email = $data['email'];
    //         $cities = $data['city'];
    //         $dob = $data['date_of_birth'];

    //         $ch = curl_init("http://graph.facebook.com/" . $data['channel_userid'] . "/picture?width=130&height=150&redirect=false");
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); // Mean 5 seconds
    //         $content = curl_exec($ch);
    //         $data = json_decode($content, true);
    //         curl_close($ch);
    //         copy($data["data"]["url"], public_path('profileImages') . "/" . $userID . ".jpg");

    //         $this->mail($email);
    //         return $influencer;

    //     }
    //     else
    //     {
    //         if(Session::has('access_token')){
    //             if(Session::has('facebook_info')){
    //                 $inf =  InfluencerUsers::create([
    //                     'firstname' => $data['firstname'],
    //                     'lastname' => $data['lastname'],
    //                     'email' => $data['email'],
    //                     'password' => Hash::make($data['password_confirmation']),
    //                     'date_of_birth' => date("Y-m-d", strtotime($data['date_of_birth'])),
    //                     'country' => $data['country'],
    //                     'state' => $data['state'],
    //                     'city' => $data['city'],
    //                     'zipcode' => $data['zipcode'],
    //                     'gender' => $data['gender'],
    //                     'channel' => "1,2",
    //                     'terms_and_conditions' => $data['terms_and_conditions'],
    //                     'privacy_policy' => $data['privacy_policy'],
    //                 ]);
    //             }
    //             else{
    //                 $inf =  InfluencerUsers::create([
    //                     'firstname' => $data['firstname'],
    //                     'lastname' => $data['lastname'],
    //                     'email' => $data['email'],
    //                     'password' => Hash::make($data['password_confirmation']),
    //                     'date_of_birth' => date("Y-m-d", strtotime($data['date_of_birth'])),
    //                     'country' => $data['country'],
    //                     'state' => $data['state'],
    //                     'city' => $data['city'],
    //                     'zipcode' => $data['zipcode'],
    //                     'gender' => $data['gender'],
    //                     'channel' => "2",
    //                     'terms_and_conditions' => $data['terms_and_conditions'],
    //                     'privacy_policy' => $data['privacy_policy'],
    //                 ]);
    //             }
    //         }
    //         else{
    //             if(Session::has('facebook_info')){
    //                 $inf =  InfluencerUsers::create([
    //                     'firstname' => $data['firstname'],
    //                     'lastname' => $data['lastname'],
    //                     'email' => $data['email'],
    //                     'password' => Hash::make($data['password_confirmation']),
    //                     'date_of_birth' => date("Y-m-d", strtotime($data['date_of_birth'])),
    //                     'country' => $data['country'],
    //                     'state' => $data['state'],
    //                     'city' => $data['city'],
    //                     'zipcode' => $data['zipcode'],
    //                     'gender' => $data['gender'],
    //                     'channel' => "1",
    //                     'terms_and_conditions' => $data['terms_and_conditions'],
    //                     'privacy_policy' => $data['privacy_policy'],
    //                 ]);
    //             }
    //             else{
    //                 $inf =  InfluencerUsers::create([
    //                     'firstname' => $data['firstname'],
    //                     'lastname' => $data['lastname'],
    //                     'email' => $data['email'],
    //                     'password' => Hash::make($data['password_confirmation']),
    //                     'date_of_birth' => date("Y-m-d", strtotime($data['date_of_birth'])),
    //                     'country' => $data['country'],
    //                     'state' => $data['state'],
    //                     'city' => $data['city'],
    //                     'zipcode' => $data['zipcode'],
    //                     'gender' => $data['gender'],
    //                     'terms_and_conditions' => $data['terms_and_conditions'],
    //                     'privacy_policy' => $data['privacy_policy'],
    //                 ]);
    //             }
    //         }

    //         if(Session::has('access_token')){
    //             $secret_token = Session::get('access_token');
    //             $influencerSocial = influencerSocial::create([
    //                 'influencer_id' => $inf->id,
    //                 'channel_id' => '2',
    //                 'channelname' => 'twitter',
    //                 'channel_userid' => $secret_token['user_id'],
    //                 'isapporved' => '2',
    //                 'channel_accesstoken' => $secret_token['oauth_token'],
    //                 'twitter_accesstoken_secret' => $secret_token['oauth_token_secret'],
    //             ]);
    //         }
    //         $email = $data['email'];
    //         if(Session::has('facebook_info')){
    //             $secret_token = Session::get('facebook_info');
    //             $influencerSocial = influencerSocial::create([
    //                 'influencer_id' => $inf->id,
    //                 'channel_id' => '1',
    //                 'channelname' => 'facebook',
    //                 'channel_userid' => $secret_token['id'],
    //                 'isapporved' => '2',
    //                 'channel_accesstoken' => $secret_token->token,
    //             ]);
    //             $email = $data['email'];

    //             $ch = curl_init("http://graph.facebook.com/" . $secret_token['id'] . "/picture?width=130&height=150&redirect=false");
    //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //             curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); // Mean 5 seconds
    //             $content = curl_exec($ch);
    //             $data = json_decode($content, true);
    //             curl_close($ch);
    //             copy($data["data"]["url"], public_path('profileImages') . "/" . $secret_token['id'] . ".jpg");
    //         }

    //         $this->mail($email);
    //         return  $inf;
    //     }
    // }

    public function mail($email) {

        $influencer = InfluencerUsers::where('email',$email)->get();
        $obj = emailTemplate::where('id', '1')
            ->where('status', '=', '1')
            ->get();

        $replaceDom = emailField::get();

        foreach($replaceDom as $field){
            $field['field_name'] = '['.$field['field_name'].']';
            $find = strstr($obj[0]->template,$field['field_name']);
            if(isset($find) && !empty($find)){
                $obj[0]->template = $this->replace_parameter($obj[0]->template,$field['field_name'],$influencer);
            }
        }

        $obj[0]->mail_url_id = \Crypt::encrypt($influencer[0]->id);

        foreach ($obj as $objData){

            Mail::to($email)->send(new SendMailable($objData));
        }
        return 'Email was sent';
    }

    public function replace_parameter($str,$field,$data,$custom_msg=null)
    {
        if($field == "[influencer_name]"){
            return str_replace($field,$data[0]->firstname,$str);
        }
        elseif($field == "[verify_account]"){
            $content = File::get(public_path('/css/btnDesign.txt'));
            $token = \Crypt::encrypt($data[0]->id);
            $redirect = url('/influencer/account/'.$token);
            $url = "<a href='$redirect' style='".preg_replace( "/\r|\n/", "", $content )."'>Click Here</a>";
            return str_replace($field,$url,$str);
        }
        elseif ($field == "[user_email]"){
            return str_replace($field,$data[0]->email,$str);
        }
        elseif ($field == "[not_approve]"){
            if($custom_msg != null){
                return str_replace($field,$custom_msg,$str);
            }
            else{
                return str_replace($field,"Due To Privacy We Cannot Reason For Not Approval",$str);
            }
        }
        else
            return str_replace($field,$data[0]->lastname,$str);
    }

    public function verifyMail(){
        return view('auth.verifyMail');
    }

    public function adminVerifyMail(){
        return view('auth.passwords.adminVerifyMail');
    }
}
