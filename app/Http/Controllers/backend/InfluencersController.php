<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Auth\RegisterController;
use App\Mail\CampaignNotify;
use App\Mail\ChannelNotVerified;
use App\Mail\SendMailable;
use App\Model\backend\Campaign;
use App\Model\backend\cities;
use App\Model\backend\crone_mail;
use App\Model\backend\emailField;
use App\Model\backend\emailTemplate;
use App\Model\backend\matched_influencer;
use App\Model\backend\states;
use App\Model\frontend\Campaignshared;
use App\Model\frontend\influencerSocial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\frontend\InfluencerUsers;
use Illuminate\Support\Facades\Hash;
use App\Model\backend\Country;
use Illuminate\Support\Facades\Mail;
use function foo\func;

class InfluencersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $obj = InfluencerUsers::get();
        return view('backend.pages.influencer.influencers')->with(compact('obj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country = states::where('country_id',101)->get();
        return view('backend.pages.influencer.CreateInfluencer')->with(compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->editid != '' || isset($request->editid))
            {
                if($request->facebook){
                    $updateChannelInfo = influencerSocial::query()->where('channel_id',1)
                        ->where('influencer_id',$request->editid)
                        ->update([
                            'isapporved'=>$request->facebook,
                            'reason_notapproved'=>$request->bookDesc
                        ]);
                }
                else{
                    $updateChannelInfo = influencerSocial::query()->where('channel_id',1)
                        ->where('influencer_id',$request->editid)
                        ->update([
                            'isapporved'=>0,
                            'reason_notapproved'=>$request->bookDesc
                        ]);
                }

                if($request->twitter){
                    $updateChannelInfo = influencerSocial::query()->where('channel_id',2)
                        ->where('influencer_id',$request->editid)
                        ->update([
                            'isapporved'=>$request->twitter,
                            'reason_notapproved'=>$request->descTwitter
                        ]);
                }
                else{
                    $updateChannelInfo = influencerSocial::query()->where('channel_id',2)
                        ->where('influencer_id',$request->editid)
                        ->update([
                            'isapporved'=>0,
                            'reason_notapproved'=>$request->descTwitter
                        ]);
                }

                $statusActive['status'] = 1;
                $statusActive['userId'] = $request->editid;

                $tmpobj = InfluencerUsers::findOrFail($request->editid);
                $tmpobj1 = InfluencerUsers::where('id',$request->editid)->get();
                /*====================Not Approval Mail========================================*/
                $updateChannelInfo = influencerSocial::query()->where('influencer_id',$request->editid)->get();
                foreach($updateChannelInfo as $channels){
                    if($channels->channel_id == 2){
                        $oldTwitter = $channels->isapporved;
                    }
                    else{
                        $oldFacebook = $channels->isapporved;
                    }
                }

                $notApprovedTemplate = emailTemplate::where('event','NOT APPROVE FOR INFLUENCER')->select(['template'])->get();
                $replaceDom = emailField::get();
                $staticCall = new RegisterController();

                if(isset($oldFacebook) && $oldFacebook == 2 && $request->facebook == 0){
                    if(isset($oldTwitter) && $oldTwitter == 2 && $request->twitter == 0){
                        $status = "Your Facebook And Twitter Channel was Dissaproved !! Please Connect With Another Account To Start Sharing The Data";
                        foreach ($replaceDom as $emailField){
                            $emailField['field_name'] = '['.$emailField['field_name'].']';
                            $notApprovedTemplate[0]->template = $staticCall->replace_parameter($notApprovedTemplate[0]->template,$emailField['field_name'],$tmpobj1,$status);
                        }
                        Mail::to($tmpobj->email)->send(new ChannelNotVerified($notApprovedTemplate[0]->template));
                    }else{
                        $status = "Your Facebook Channel was Dissaproved !! Please Connect With Another Account To Start Sharing The Data";
                        foreach ($replaceDom as $emailField){
                            $emailField['field_name'] = '['.$emailField['field_name'].']';
                            $notApprovedTemplate[0]->template = $staticCall->replace_parameter($notApprovedTemplate[0]->template,$emailField['field_name'],$tmpobj1,$status);
                        }
                        Mail::to($tmpobj->email)->send(new ChannelNotVerified($notApprovedTemplate[0]->template));
                    }
                }else
                {
                    if(isset($oldTwitter) && $oldTwitter == 2 && $request->twitter == 0){
                        $status = "Your Twitter Channel was Dissaproved !! Please Connect With Another Account To Start Sharing The Data";
                        foreach ($replaceDom as $emailField){
                            $emailField['field_name'] = '['.$emailField['field_name'].']';
                            $notApprovedTemplate[0]->template = $staticCall->replace_parameter($notApprovedTemplate[0]->template,$emailField['field_name'],$tmpobj1,$status);
                        }
                        Mail::to($tmpobj->email)->send(new ChannelNotVerified($notApprovedTemplate[0]->template));
                    }
                }

                /*==============================================================================*/

                if($tmpobj->status==0 && $request['status'] == 1)
                    Mail::to($tmpobj->email)->send(new SendMailable($statusActive));

                $tmpobj->status = $request['status'];
                $tmpobj->save();

                if(isset($request['status']) && $request['status'] == 1){
                    $dob = Carbon::parse($tmpobj->date_of_birth)->age;
                    $cities = str_getcsv($tmpobj->city);

                    $camp = $this->fetchCampaigns($cities,$dob,$tmpobj) ;
                    foreach ($camp as $campaign){
                        $matched = new matched_influencer();
                        $matched->campaign_id = $campaign->id;
                        $matched->influencer_id = $tmpobj->id;
                        $matched->save();

                        $email_Template = emailTemplate::where('event','CAMPAIGNNOTIFICATION')->firstOrFail();

                        $matched1 = crone_mail::where('influencer_id',$tmpobj->id)->where('campaign_id',$campaign->id)->get();
                        if($matched1->isNotEmpty()){
                            $matched = new crone_mail();
                            $matched->email = $tmpobj->email;
                            $matched->subject = $email_Template->subject;
                            $matched->body = $email_Template->template;
                            $matched->influencer_id = $tmpobj->id;
                            $matched->campaign_id = $campaign->id;
                            $matched->status = 1;
                            $matched->executedate = Carbon::now()->toDateTimeString()   ;
                            $matched->save();
                        }
                    }
                    //Mail::to($tmpobj->email)->send(new CampaignNotify($camp));
                }
                toastr()->success('Changes Have Been Made successfully!');
                return redirect()->route('admin.getInfluencers');
            }
            else
            {
                $this->validate($request,[
                    'firstname'=>'required',
                    'lastname'=>'required',
                    'email'=>'required|email|unique:influencerusers',
                    'password'=>'required|min:8',
                    'password_confirmation'=>'required|same:password|min:8',
                    'date_of_birth'=>'date|required',
                    'country'=>'required',
                    'zipcode'=>'required|numeric',
                    'gender'=>'numeric|required',
                    'state'=>'required',
                    'cities'=>'required',
                    'facebook'=>'required'
                ]);
            }

            if(!empty($request->facebook) || $request->facebook != null) {
                $channel = '';
                foreach ($request->facebook as $data)
                    $channel = $channel . $data . ",";
            }

            if ($request->editid != '')
                $obj = InfluencerUsers::findOrFail($request->editid);
            else
                $obj = new InfluencerUsers();

            $obj->firstname = $request->firstname;
            $obj->lastname= $request->lastname;
            $obj->email= $request->email;

            if (isset($request->password) && !empty($request->password))
                $obj->password=Hash::make($request->password);

            if(isset($request->profile_url))
                $obj->profile_url = $request->profile_url;

            if(isset($channel) && !empty($channel))
                $obj->channel = $channel;

            if(isset($request->privacy_policy) && $request->privacy_policy == "on")
                $obj->privacy_policy=1;
            else if(isset($request->privacy_policy) && $request->privacy_policy == "off")
                $obj->privacy_policy=0;

            if(isset($request->terms_and_conditions) && $request->terms_and_conditions == "on")
                $obj->terms_and_conditions=1;
            else if(isset($request->terms_and_conditions) && $request->terms_and_conditions == "off")
                $obj->terms_and_conditions=0;

            if(isset($request['status']))
                $obj->status=$request->status;
            else
                $obj->status=0;


            $obj->date_of_birth=$request->date_of_birth;
            $obj->country=101;
            $obj->zipcode=$request->zipcode;
            $obj->gender=$request->gender;
            $obj->state=$request->state;
            $obj->city=$request->cities;
            $obj->save();

            if ($request->editid != ''){
                toastr()->success('Influencer has been Updated successfully!');
                return redirect()->route('admin.getInfluencers');
            }
            else{
                toastr()->success('Influencer has been Added successfully!');
                return redirect()->route('admin.getInfluencers');
            }
        }catch(Exception $e){
            toastr()->error('Influencer has not been Added!');
            return redirect()->route('admin.getInfluencers');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = influencerSocial::where('influencer_id',$id)->where('channel_id',1)->get();
        $obj = InfluencerUsers::find($id);
        $obj->state = str_getcsv($obj->state);
        $obj->city	 = str_getcsv($obj->city);
        $obj->channel	 = str_getcsv($obj->channel);
        $cities = cities::where('id',$obj->city[0])->get();
        $country = states::where('country_id',101)->get();
        if ($profile->isNotEmpty()){
            foreach ($profile as $imagePath){
                $obj->provider_user_id = $imagePath->channel_userid;
                break;
            }
        }
        else{
            $obj->provider_user_id = "avtarfacebookImage";
        }
        $profile = influencerSocial::where('influencer_id',$id)->get();
        foreach($profile as $info){
            if($info->channel_id == 2)
                $twitter = $info->isapporved;

            if($info->channel_id == 1)
                $facebook = $info->isapporved;

        }
        if(isset($facebook) && isset($twitter))
            return view('backend.pages.influencer.EditInfluencer')->with(compact('obj','country','cities','facebook','twitter'));

        if(isset($facebook))
            return view('backend.pages.influencer.EditInfluencer')->with(compact('obj','country','cities','facebook'));

        if (isset($twitter))
            return view('backend.pages.influencer.EditInfluencer')->with(compact('obj','country','cities','twitter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id)
    {   
        $destroy = InfluencerUsers::where('id',$id)->delete();
        $destroy = influencerSocial::where('influencer_id',$id)->delete();
        return response()->json(1);
    }
    public function changeStatus(Request $request){
        $Rows = InfluencerUsers::where('id',$request['id'])->get();
        $email = $Rows[0]->email;
        $statusActive['status'] = 1;
        $statusActive['userId'] = $request['id'];
        $affectedRows = InfluencerUsers::where('id',$request['id'])
            ->update([
                'status' => $request['status'],
            ]);
        if($request['status']){

            $tmpobj = InfluencerUsers::findOrFail($request['id']);
            $dob = Carbon::parse($tmpobj->date_of_birth)->age;
            $cities = str_getcsv($tmpobj->city);
            $camp = Campaign::query()
                ->where('cities','like','%'.$cities[0].'%')
                ->where('age_from','<=',$dob)
                ->where('age_to','>=',$dob)
                ->where('campaign_start','<=',Carbon::today()->toDateString())
                ->where('campaign_end','>=',Carbon::today()->toDateString())
                ->get();

            foreach ($camp as $campaign){
                $matched = new matched_influencer();
                $matched->campaign_id = $campaign->id;
                $matched->influencer_id = $tmpobj->id;
                $matched->save();

                $email_Template = emailTemplate::findorfail(1);

                $matched = new crone_mail();
                $matched->email = $tmpobj->email;
                $matched->subject = $email_Template->subject;
                $matched->body = $email_Template->template;
                $matched->influencer_id = $tmpobj->id;
                $matched->campaign_id = $campaign->id;
                $matched->executedate = Carbon::now()->toDateTimeString()   ;
                $matched->save();
            }
            Mail::to($email)->send(new SendMailable($statusActive));
        }

        return response()->json(1);
    }

    public function history($id){
        $influencer = InfluencerUsers::query()
            ->leftJoin('states','states.id','=','influencerusers.state')
            ->leftJoin('cities','cities.id','=','influencerusers.city')
            ->where('influencerusers.id',$id)
            ->select([
                'influencerusers.firstname',
                'influencerusers.lastname',
                'influencerusers.email',
                'influencerusers.date_of_birth',
                'influencerusers.gender',
                'states.name',
                'cities.name As city_name',
            ])
            ->get();

        $influencerFacebookShare = Campaignshared::query()
            ->leftJoin('campaign','campaign.id','=','campaign_shared.ca_id')
            ->where('influencerusers_id',$id)
            ->where('shared_on_fb',1)
            ->select([
                'campaign.name',
                'campaign.title',
                'campaign_shared.post_url_fb',
                'campaign.remuneration'
            ])
            ->get();


        $influencerTwitterShare = Campaignshared::query()
            ->leftJoin('campaign','campaign.id','=','campaign_shared.ca_id')
            ->where('influencerusers_id',$id)
            ->where('shared_on_twitter',1)
            ->select([
                'campaign.name',
                'campaign.title',
                'campaign_shared.post_url_twitter',
                'campaign.remuneration'
            ])
            ->get();

        if($influencer->isNotEmpty())
            return view('backend.pages.influencer.history')->with(compact('influencer','influencerFacebookShare','influencerTwitterShare'));
        else
            return view('backend.pages.influencer.history');

    }

    public function fetchCampaigns($cities,$dob,$tmpobj){
        return Campaign::query()
            ->where('cities','like','%'.$cities[0].'%')
            ->where('age_from','<=',$dob)
            ->where('age_to','>=',$dob)
            ->where('campaign_start','<=',Carbon::today()->toDateString())
            ->where('campaign_end','>=',Carbon::today()->toDateString())
            ->where('gender',$tmpobj->gender)
            ->where('channel','like','%'.$tmpobj->channel.'%')
            ->orWhere('gender',2)
            ->where('status',2)
            ->get();
    }
}
