<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Campaign;
use App\Model\frontend\influencerSocial;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\frontend\InfluencerUsers;
use App\Model\backend\AdminUser;
use Illuminate\Support\Facades\Hash;
USE Illuminate\Support\Facades\Mail;

class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country = AdminUser::get();
        return view('backend.pages.admins.adminList')->with(compact('country'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	dd('hello');
        try{
            if($request->editid != '' || isset($request->editid)){
                $request->validate([
                    'user_name'=>'required|unique:adminuser,username,'.$request->editid,
                    'firstname'=>'required',
                    'lastname'=>'required',
                    'email'=>'required|email|unique:adminuser,email,'.$request->editid,
                    'skype'=>'required|unique:adminuser,skype,'.$request->editid,
                    'phone'=>'required|integer|unique:adminuser,phone,'.$request->editid,
                    'mobile'=>'required|integer|unique:adminuser,mobile,'.$request->editid,
                    'designation' => 'required|integer',
                ]);

                if($request->file('profile1') != null)
                {
                    $ext = $request->file('profile1')->getClientOriginalExtension();
                    $size = $request->file('profile1')->getSize();

                    $file = $request->file('profile1');
                    $pathfile = md5($file->getClientOriginalName(). time()).".".$ext;
                    $file->move(public_path('adminProfile'), $pathfile);
                }
                $obj = AdminUser::findOrFail($request->editid);
                $obj->username = $request['user_name'];
                $obj->firstname = $request['firstname'];
                $obj->lastname = $request['lastname'];
                $obj->email = $request['email'];
                $obj->skype = $request['skype'];
                $obj->phone = $request['phone'];
                $obj->mobile = $request['mobile'];
                $obj->au_ar_id = $request['designation'];

                if(isset($pathfile) && !empty($pathfile))
                    $obj->photo = $pathfile;

                $obj->save();

                toastr()->success('Admin has been Updated successfully!');
                return redirect()->route('admin.get.admin');
            }
            else{
                $request->validate([
                    'user_name'=>'required|unique:adminuser,username',
                    'firstname'=>'required',
                    'lastname'=>'required',
                    'email'=>'required|email|unique:adminuser,email',
                    'skype'=>'required|unique:adminuser,skype',
                    'phone'=>'required|integer|unique:adminuser,phone',
                    'mobile'=>'required|integer|unique:adminuser,mobile',
                    'pass'=>'required|min:6',
                    'retype_pass'=>'required|same:pass|min:6',
                    'profile'=>'required',
                    'designation' => 'required|integer',
                ]);
                if($request->file('profile') != null)
                {
                    $ext = $request->file('profile')->getClientOriginalExtension();
                    $size = $request->file('profile')->getSize();

                    $file = $request->file('profile');
                    $pathfile = md5($file->getClientOriginalName(). time()).".".$ext;
                    $file->move(public_path('adminProfile'), $pathfile);
                }

                $obj = new AdminUser();
                $obj->username = $request['user_name'];
                $obj->firstname = $request['firstname'];
                $obj->lastname = $request['lastname'];
                $obj->email = $request['email'];
                $obj->skype = $request['skype'];
                $obj->phone = $request['phone'];
                $obj->mobile = $request['mobile'];
                $obj->password =  Hash::make($request['retype_pass']);
                $obj->au_ar_id = "temp";
                $obj->photo = $pathfile;
                $obj->au_ar_id = $request['designation'];
                $obj->save();
                toastr()->success('Admin has been Generated successfully!');
                return redirect()->route('admin.get.admin');
            }
        }catch(Exception $e){

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = AdminUser::find($id);
        return view('backend.pages.admins.edit')->with(compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id){
        $admin = AdminUser::where('id',$id)->delete();
        return response()->json(1);
    }
    public function showDashboard()
    {

        $campaign['total'] = Campaign::get()->count();
        $campaign['active'] = Campaign::query()
            ->where('campaign_start','<=',Carbon::today()->toDateString())
            ->where('campaign_end','>=',Carbon::today()->toDateString())
            ->get()
            ->count();
        $campaign['upcoming'] = Campaign::query()
            ->where('campaign_start','>=',Carbon::today()->toDateString())->get()->count();
        $campaign['over'] = Campaign::query()->where('campaign_end','<=',Carbon::today()->toDateString())->get()->count();

        $influencer['total'] = InfluencerUsers::get()->count();
        $influencer['active'] = InfluencerUsers::query()->where('status',1)->count();
        $influencer['pending'] = influencerSocial::query()->where('isapporved',2)->count();
        $influencer['deactive'] = InfluencerUsers::query()->where('status',0)->count();

        return view('backend.pages.Dashboard')->with(compact('campaign','influencer'));
    }
}
