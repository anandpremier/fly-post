<?php

namespace App\Http\Controllers\backend;
use Socialite;

use App\Model\backend\Campaign;
use App\Model\backend\crone_mail;
use App\Model\frontend\Campaignshared;
use App\Model\frontend\influencerSocial;
use App\Model\frontend\InfluencerUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Support\Facades\URL;

class TwitterController extends Controller
{
    public function editConnect(){
        Session::put('edit_twitter_channel', true);
        return redirect()->to('/connect');
    }

    public function postTweet($id){
        Session::put('share_on_twitter', $id);
        return redirect()->to('/connect');
    }

    public function connect(){

        // \Twitter::reconfig(['token' => '', 'secret' => '']);
  
        $token = \Twitter::getRequestToken(route('twitter.callback'));
     
        /* Build TwitterOAuth object with client credentials. */
        $connection = new TwitterOAuth(getenv('TWITTER_CONSUMER_KEY'), getenv('TWITTER_CONSUMER_SECRET'));

        /* Get temporary credentials. */
        $request_token = $connection->oauth("oauth/request_token", array("oauth_callback" => getenv('TWITTER_OAUTH_CALLBACK')));
       
        /* Save temporary credentials to session. */
        $token1 = $request_token['oauth_token'];
        Session::put('oauth_token', $request_token['oauth_token']);
        Session::put('oauth_token_secret', $request_token['oauth_token_secret']);
      
        $url = $connection->url("oauth/authorize", array("oauth_token" => $token1));
      
        return Redirect::to($url);
        
}
    public function callback(Request $request)
    {  
       
        $connection = new TwitterOAuth(getenv('TWITTER_CONSUMER_KEY'), getenv('TWITTER_CONSUMER_SECRET'), $request->session()->get('oauth_token'), $request->session()->get('oauth_token_secret'));
        $access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $request->oauth_verifier));
        $twitterUrl = 'https://twitter.com/';

        $connection1 = new TwitterOAuth(getenv('TWITTER_CONSUMER_KEY'), getenv('TWITTER_CONSUMER_SECRET'), $access_token['oauth_token'], $access_token['oauth_token_secret']);
        $twitter_user_profile = $connection1->get('account/verify_credentials', ['tweet_mode' => 'extended', 'include_entities' => 'true']);
        $profile_url = $twitter_user_profile->profile_image_url;
        $request->session()->put('twitter_token',[
            'accessToken' => $request->session()->get('oauth_token'),
            'twitter_oauth_token_secret'=> $request->session()->get('oauth_token_secret'),
            'user_id'=> $access_token['user_id'],
            'user_profile_image'=> $profile_url,
            'channle_url'=> $twitterUrl.''.$access_token['screen_name'],
            'channel_id'=> '2',
            'channelname'=> 'twitter'

        ]);
       
        $socialLink = influencerSocial::where('channel_userid',$access_token['user_id'])->first();
      
        if( !isset( $socialLink )){ ?>
            <script>
          closeMe();
          function closeMe() {
              window.opener.CloseTwitterPopup(window, 'success','<?php echo $access_token['user_id']; ?>');
          }
          </script>
<?php
        }else{ ?>
        <script>
          closeMe();
          function closeMe() {
              window.opener.CloseTwitterPopup(window, 'fail');
          }

          </script>
<?php

        }

    }

    // public function connect(){
    //     $sign_in_twitter = true;
    //     $force_login = false;
    //     \Twitter::reconfig(['token' => '', 'secret' => '']);
    //     $token = \Twitter::getRequestToken(route('twitter.callback'));
    //     if (isset($token['oauth_token_secret']))
    //     {
    //         $url = \Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);
    //         Session::put('oauth_state', 'start');
    //         Session::put('oauth_request_token', $token['oauth_token']);
    //         Session::put('oauth_request_token_secret', $token['oauth_token_secret']);
    //         return Redirect::to($url);
    //     }
    //     return Redirect::route('twitter.error');
    // }

    // public function callback(){
    //     if (Session::has('oauth_request_token'))
    //     {
    //         $request_token = [
    //             'token'  => Session::get('oauth_request_token'),
    //             'secret' => Session::get('oauth_request_token_secret'),
    //         ];
    //         \Twitter::reconfig($request_token);
    //         $oauth_verifier = false;
    //         if (\request()->has('oauth_verifier')){
    //             $oauth_verifier = \request()->get('oauth_verifier');
    //             $token = \Twitter::getAccessToken($oauth_verifier);
    //         }
    //         if (!isset($token['oauth_token_secret'])){
    //             return Redirect::route('twitter.error')->with('flash_error', 'We could not log you in on Twitter.');
    //         }
    //         $credentials = \Twitter::getCredentials();
    //         if (is_object($credentials) && !isset($credentials->error)) {
    //             Session::put('access_token', $token);
    //             if(Session::has('share_on_twitter')){
    //                 $id = Session::get('share_on_twitter');
    //                 $request = new Request();
    //                 $request->id = $id;
    //                 $this->shareTweet($request);
    //             }
    //             if(Session::has('edit_twitter_channel')){
    //                 return redirect()->route('twitter.add');
    //             }
    //             return Redirect::to('/register');
    //         }
    //         return Redirect::route('twitter.error')->with('flash_error', 'Crab! Something went wrong while signing you up!');
    //     }
    // }

    public function shareTweet(Request $request){
        $session_values = Session::get('access_token');
        Session::forget('access_token');
        $id = Auth::user()->id;

        $inf = influencerSocial::query()
            ->where('influencer_id',$id)
            ->where('channel_id',2)
            ->update([
                'channel_accesstoken' =>$session_values['oauth_token'],
                'twitter_accesstoken_secret' => $session_values['oauth_token_secret']
            ]);

        $inf = influencerSocial::query()
            ->where('influencer_id',$id)
            ->where('channel_id',2)
            ->get();

        $crone_mail = crone_mail::find($request->id);
        $influencerUsers = InfluencerUsers::find($crone_mail->influencer_id);
        $campaign = Campaign::find($crone_mail->campaign_id);

        $CONSUMER_KEY = env('TWITTER_CONSUMER_KEY');
        $TWITTER_CONSUMER_SECRET = env('TWITTER_CONSUMER_SECRET');

        $access_token = $inf[0]->channel_accesstoken;
        $access_token_secret = $inf[0]->twitter_accesstoken_secret;;

        $connection = new TwitterOAuth($CONSUMER_KEY, $TWITTER_CONSUMER_SECRET, $access_token, $access_token_secret);
        $media1 = $connection->upload('media/upload', ['media' => public_path('campaignImages')."/".$campaign['image']]);
        $parameters = [
            'status' => 'Title:='.$campaign['title']."\n".'Name:='.$campaign['name']."\n".'Link:='.$campaign['link'],
            'media_ids' => implode(',', [$media1->media_id_string])
        ];
        $statues = $connection->post('statuses/update', $parameters);
        if($connection->getLastHttpCode() == 200){
            $check = Campaignshared::query()
                ->where('influencerusers_id',$influencerUsers->id)
                ->where('ca_id',$campaign->id)
                ->update([
                    'shared_on_twitter'=>1,
                    'post_url_twitter'=>'https://twitter.com/98Programmer/status/'.$statues->id_str
                ]);
            if($check == 0){
                $sharePost = Campaignshared::create([
                    'ca_id' => $campaign->id,
                    'first_channel_shared'=>2,
                    'influencerusers_id' => $influencerUsers->id,
                    'comment_remuneration' => $campaign->comment_remuneration,
                    'shared_on_twitter'=> 1,
                    'sharing_comment' => $campaign->comment_remuneration,
                    'post_url_twitter'=>'https://twitter.com/98Programmer/status/'.$statues->id_str
                ]);
            }
            Session::put('share_tweet_status',200);
        }
        else
            Session::put('share_tweet_status',100);

        $closePopup = true;
        return view('home')->with(compact('closePopup'));
    }

    public function validateTweet(){
        $id = Session::get('share_on_twitter');
        Session::forget('share_on_twitter');
        $crone_mail = crone_mail::find($id);
        $campaign = Campaign::find($crone_mail->campaign_id);
        $inf_id = auth()->user()->id;
        $share = Campaignshared::query()
            ->where('influencerusers_id',auth()->user()->id)
            ->where('shared_on_twitter',1)
            ->where('ca_id',$campaign->id)
            ->get();

        if($share->isNotEmpty())
            return response()->json([
                'data' => 1,
                'id' => $id
            ]);
        else
            return response()->json(0);
    }

    public function addChannel(){
        if(Session::has('access_token')){
            $id = Session::get('current_user_id_twitter');
            $secret_token = Session::get('access_token');
            $influencerSocial = influencerSocial::create([
                'influencer_id' => $id,
                'channel_id' => '2',
                'channelname' => 'twitter',
                'channel_userid' => $secret_token['user_id'],
                'isapporved' => '2',
                'channel_accesstoken' => $secret_token['oauth_token'],
                'twitter_accesstoken_secret' => $secret_token['oauth_token_secret'],
            ]);
            $influencer = InfluencerUsers::findorfail($id);
            if(empty($influencer->channel))
                $influencer->channel = $influencer->channel."2";
            else
                $influencer->channel = $influencer->channel.",2";

            Session::put('access_token_submit', true);
            Session::forget('access_token');
            $influencer->status = 0;
            $influencer->save();

            $path = "/edit/profile/".$id;
            return redirect()->to($path);
        }
    }

    public function removeSession(){
        Session::forget('access_token');
        Session::forget('access_token_submit');
        Session::forget('facebook_info');
        Session::forget('access_token');
        Session::forget('facebook_error');
        Session::forget('twitter_error');
        Session::forget('edit_twitter_channel');

        $channel = influencerSocial::query()
            ->where('influencer_id',auth()->user()->id)
            ->where('channel_id',2)
            ->get();
        if($channel->isNotEmpty())
            return response()->json(1);
        else
            return response()->json(0);
    }

    public function removeChannel(){
        $var = $this->removeSession();
        $id = Auth::user()->id;
        $influencer = InfluencerUsers::findorfail($id);
        $parts = explode(',', $influencer->channel);
        while(($i = array_search(2, $parts)) !== false) {
            unset($parts[$i]);
        }
        $influencer->channel = implode(',', $parts);
        $influencer->save();
        $influencer = influencerSocial::where('influencer_id', '=', $id)->where('channel_id','=',2)->delete();
        return response()->json(1);
    }

    public function error(){
        Session::put('twitter_error',true);
        return redirect()->to('/register');
    }

    public function sessionData(Request $request){
        $data =  Session::get('access_token');

        $info = influencerSocial::query()
            ->where('channel_userid',$data['user_id'])
            ->where('channel_id',2)
            ->get();

        if($info->isNotEmpty())
            $data = "undefined";

        return response()->json($data);
    }

    public function redirect()
    {
        return Socialite::driver('twitter')->redirect();
    }
}
