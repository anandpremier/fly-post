<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\AdminUser;
use App\Model\backend\email;
use App\Model\backend\emailField;
use App\Model\backend\emailTemplate;
use App\Model\frontend\InfluencerUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = emailTemplate::get();
        return view('backend.pages.contentManagement.mailIndex')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $email = AdminUser::where('au_ar_id',3)->get();
        $email_fields = emailField::get();
        return view('backend.pages.contentManagement.mailTemplate')->with(compact('email','email_fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $request->validate([
                'emailEvent'=>'required|string',
                'subject'=>'required|string',
                'from'=>'required|email',
                /*'cc'=>'required|email',
                'bcc'=>'required|email',*/
                'status'=>'required|integer',
                'for_whom'=>'required|integer',
                'editor'=>'required'
            ]);

            if(isset($request->id))
                $obj = emailTemplate::findorfail($request->id);
            else
                $obj = new emailTemplate();

            $obj->event = strtoupper($request['emailEvent']);
            $obj->for_whom = $request['for_whom'];
            $obj->status = $request['status'];
            $obj->from = $request['from'];

            if(isset($obj->cc))
                $obj->cc = $request['cc'];
            if(isset($obj->bcc))
                $obj->bcc = $request['bcc'];

            $obj->subject = strtoupper($request['subject']);
            $obj->template = $request['editor'];
            $obj->save();

            if(isset($request->id)){
                toastr()->success('Template has been Updated successfully!');
                return redirect()->route('admin.mail.get');
            }
            else{
                toastr()->success('Template has been Added successfully!');
                return redirect()->route('admin.mail.get');
            }

        }catch (Exception $e){
            toastr()->error($e);
            return redirect()->route('admin.mail.get');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = emailTemplate::where('id',$id)
                ->get();
        $email = AdminUser::where('au_ar_id',3)->get();
        $email_fields = emailField::get();
        return view('backend.pages.contentManagement.mailEdit')->with(compact('data','email','email_fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $emailTemplate = emailTemplate::where('id',$id)->delete();
        return response()->json(1);
    }
}
