<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Campaign;
use App\Model\backend\crone_mail;
use App\Model\frontend\Campaignshared;
use App\Model\frontend\influencerSocial;
use App\Model\frontend\InfluencerUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

class FacebookController extends Controller
{
    public function connect(){
        Session::put('facebook_oauth_state', true);
		
        return redirect()->to('/redirect');
    }

    public function getInfo(){
        Session::put('edit_facebook', true);
        return redirect()->to('/redirect');
    }

   public function getFacebookuser(Request $request){
    

        $ch = curl_init("http://graph.facebook.com/" . $request->userID . "/picture?width=200&height=200&redirect=false");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); // Mean 5 seconds
        $content = curl_exec($ch);
        $fb_user_profile = json_decode($content, true);
        
        $info = influencerSocial::query()
            ->where('channel_userid',$request->userID)
            ->where('channel_id',1)
            ->get();
            $fbUrl = 'https://www.facebook.com/app_scoped_user_id/';
            if(!isset($info)){
                $data = "undefined";
            }else{

                $request->session()->put('facebook_token',[
                    'accessToken' => $request->accessToken,
                    'twitter_oauth_token_secret'=> 'NULL',
                    'user_profile_image'=> $fb_user_profile["data"]["url"],
                    'channle_url'=> $fbUrl.''.$request->userID,
                    'user_id'=> $request->userID,
                    'channel_id'=> '1',
                    'channelname'=> 'facebook'
                ]);
                $data = "defined";
            }

        return response()->json($data);
    }
 
    public function sessionData(Request $request){

        $data =  Session::get('facebook_info');
        $info = influencerSocial::query()
            ->where('channel_userid',$data['id'])
            ->where('channel_id',1)
            ->get();

        if($info->isNotEmpty())
            $data = "undefined";

        return response()->json($data);
    }

    public function addChannel(){
        $id = Auth::user()->id;
        $secret_token = Session::get('facebook_info');
        $influencerSocial = influencerSocial::create([
            'influencer_id' => $id,
            'channel_id' => '1',
            'channelname' => 'facebook',
            'channel_userid' => $secret_token['id'],
            'isapporved' => '2',
            'channel_accesstoken' => $secret_token->token,
        ]);

        $influencer = InfluencerUsers::findorfail($id);
        if(empty($influencer->channel))
            $influencer->channel = "1";
        else
            $influencer->channel = "1,2";

        $influencer->status = 0;
        $influencer->save();

        return redirect()->to('/register');
    }

    public function removeSession(){
        Session::forget('access_token');
        Session::forget('access_token_submit');
        Session::forget('facebook_info');
        Session::forget('facebook_error');
        Session::forget('twitter_error');
        Session::forget('facebook_oauth_state');
        Session::forget('edit_twitter_channel');

        $channel = influencerSocial::query()
            ->where('influencer_id',auth()->user()->id)
            ->where('channel_id',1)
            ->get();

        if($channel->isNotEmpty())
            return response()->json(1);
        else
            return response()->json(0);
    }

    public function shareCamping(Request $request) {
        $crone_mail = crone_mail::find($request->id);
        $infoSocial = influencerSocial::query()
            ->where('influencer_id',$crone_mail->influencer_id)
            ->where('channel_id',1)
            ->get();
        if($infoSocial->isNotEmpty()){
            foreach ($infoSocial as $info){
                $userId = $info->channel_userid;
                break;
            }
        }
        else
            $userId = "00000";

        $influencerUsers = InfluencerUsers::find($crone_mail->influencer_id);
        $campaign = Campaign::find($crone_mail->campaign_id);

        $check = Campaignshared::query()
            ->where('influencerusers_id',$influencerUsers->id)
            ->where('ca_id',$campaign->id)
            ->update([
                'shared_on_fb'=>1,
                'post_url_fb'=>'https://www.facebook.com/'.$userId.'/posts/000000000000000',
            ]);

        if($check == 0){
            $sharePost = Campaignshared::create([
                'ca_id' => $campaign->id,
                'first_channel_shared'=>1,
                'influencerusers_id' => $influencerUsers->id,
                'comment_remuneration' => $campaign->comment_remuneration,
                'sharing_comment' => $campaign->comment_remuneration,
                'shared_on_fb' => 1,
                'post_url_fb'=>'https://www.facebook.com/'.$userId.'/posts/000000000000000',
            ]);
        }
        return $campaign;
    }

    public function removeChannel(){
        $val = $this->removeSession();
        $id = Auth::user()->id;
        $influencer = InfluencerUsers::findorfail($id);
        $parts = explode(',', $influencer->channel);
        while(($i = array_search(1, $parts)) !== false) {
            unset($parts[$i]);
        }
        $influencer->channel = implode(',', $parts);
        $influencer->save();
        $influencer = influencerSocial::where('influencer_id', '=', $id)->where('channel_id','=',1)->delete();
        return response()->json(1);
    }

    public function getFeed(){
        try {
            $fb = new Facebook([
                'app_id' => 555176518557600,
                'app_secret' => '55ce91ec042f523ac26246bb71ef8239',
                'default_graph_version' => 'v3.1'
            ]);
            $userPosts = $fb->get("/121251655924716/feed", 'EAAH47hstH6ABAJQPo6SQ5Npp5M4NbK1ruy3MtZCBeFvnyMsCreqZApEjBcZBXVuZBkm8L55TZCBNvByZCBnHNtg0NwYkjJ0RoFUNfke5HeZAwtnkaSILcaYkRXqynrGKg2x4wLitZCnSjJFEuL1T7abCdQ7FboQA8mFfjJyCU3v4bQZDZD');
            $postBody = $userPosts->getDecodedBody();
            $postData = $postBody["data"];
            dd($userPosts,$postBody);
        } catch (FacebookResponseException $e) {
            dd($e);
            exit();
        } catch (FacebookSDKException $e) {
            dd($e);
            exit();
        }
    }
}
