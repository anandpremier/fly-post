<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\AdminUser;
use App\Model\backend\Campaign;
use App\Model\backend\crone_mail;
use App\Model\backend\email;
use App\Model\backend\emailField;
use App\Model\backend\emailTemplate;
use App\Model\backend\matched_influencer;
use App\Model\frontend\influencerSocial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\backend\states;
use App\Model\backend\cities;
use App\Model\frontend\InfluencerUsers;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Model\backend\CampaignState;
use App\Model\backend\CampaigCity;
use App\Model\backend\CampaignChannel;
use DateTime;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $obj = Campaign::get();
        return view('backend.pages.campaign.index')->with(compact('obj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $state = states::where('country_id',101)->get();
        $email = AdminUser::where('au_ar_id',3)
            ->get();
        return view('backend.pages.campaign.create')->with(compact('state','email'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        try{
            if($request->editid != '' || isset($request->editid)){
                $request->validate([
                    'campaign_type'=>'required|integer',
                    'campaign_name'=>'required',
                    'shares'=>'required|integer',
                    'overperform'=>'required|numeric|between:0,99.99',
                    'link'=>'required|url',
                    'title'=>'required',
                    'labeling'=>'required',
                    'description'=>'required',
                    'cities'=>'required',
                    'channel'=>'required',
                    'gender'=>'required',
                    'startdate'=>'required|date',
                    'enddate'=>'required|date',
                    'fromage'=>'required|integer',
                    'toage'=>'required|integer',
                    'c_d_s'=>'required|integer',
                    'salesman'=>'required|integer',
                ]);
            }else{
                $request->validate([
                    'campaign_type'=>'required|integer',
                    'campaign_name'=>'required',
                    'shares'=>'required|integer',
                    'overperform'=>'required|numeric|between:0,99.99',
                    'link'=>'required|url',
                    'title'=>'required',
                    'labeling'=>'required',
                    'description'=>'required',
                    'states'=>'required',
                    'cities'=>'required',
                    'channel'=>'required',
                    'gender'=>'required',
                    'startdate'=>'required|date',
                    'enddate'=>'required|date',
                    'fromage'=>'required|integer',
                    'toage'=>'required|integer',
                    'salesman'=>'required|integer',
                    'c_d_s'=>'required|integer',
                ]);
            }

            if(isset($request->editid))
            {
                $obj = Campaign::findOrFail($request->editid);
            }
            else{
                $obj = new Campaign();
            }

            if($request->file('imageCampaign') != null && isset($request->editid))
            {
                $ext = $request->file('imageCampaign')->getClientOriginalExtension();
                $file = $request->file('imageCampaign');
                $pathfile = md5($file->getClientOriginalName(). time()).".".$ext;
                $file->move(public_path('campaignImages'), $pathfile);
            }

            if($request->file('image') != null) {
                $ext = $request->file('image')->getClientOriginalExtension();
                $file = $request->file('image');
                $pathfile = md5($file->getClientOriginalName(). time()).".".$ext;
                $file->move(public_path('campaignImages'), $pathfile);
            }
            else if(!isset($request->editid) || empty($request->editid)){
                $errors1['notnull'] = "Image Is Required";
                return view('backend.pages.campaign.create')->with(compact('errors1'));
            }

            $obj->sales_person = $request['salesman'];
            $obj->campaign_start = $request['startdate'];
            $obj->campaign_end = $request['enddate'];
            $obj->age_from = $request['fromage'];
            $obj->age_to = $request['toage'];

            if($request->file('imageCampaign') != null)
                $obj->image = $pathfile;

            if($request->file('image') != null)
                $obj->image = $pathfile;

            $obj->campaign_type = $request['campaign_type'];
            $obj->name = $request['campaign_name'];
            $obj->number_of_shares = $request['shares'];
            $obj->shares_over_performance = $request['overperform'];
            $obj->link = $request['link'];
            $obj->title = $request['title'];
            $obj->sponsored_post = $request['labeling'];
            $obj->message = $request['description'];
            $obj->c_d_S = $request['c_d_s'];
            $obj->gender = $request['gender'];
            $obj->save();

            /* Channel  */
            if(isset($request->editid))
            {
                $obj = Campaign::findOrFail($request->editid);

                foreach($request->channel as $key => $channelData){
                    $channel = new CampaignChannel();
                    $channel->campaign_id = $obj->id;
                    $channel->channel =$channelData;
                    $channel->save();
                }
              
            }
            else{

                foreach($request->channel as $key => $channelData){
                    $channel = new CampaignChannel();
                    $channel->campaign_id = $obj->id;
                    $channel->channel =$channelData;
                    $channel->save();
                }
              
            }
          
          
            /* State and city add */

            if(isset($request->editid))
            {   
                $getState = CampaignState::where('campaign_id',$request->editid)->get();
                
                foreach($getState as $key => $statusData){
                   
                    $state = new CampaignState();
                    $state->campaign_id = $request->editid;
                    $state->country_id = 101;
                    $state->state =$statusData->state;
                    $state->save();
                }
              
            }
            else{
                
                foreach($request->states as $key => $statusData){
                 
                    $state = new CampaignState();
         
                    $state->campaign_id = $obj->id;
                    $state->country_id = 101;
                    $state->state =$statusData;
                    $state->save();
                    } 
            }

            if(isset($request->editid))
            {
                $getCity = CampaigCity::where('campaign_id',$request->editid)->get();
              
                foreach($getCity as $key => $cityData){
                    $city = new CampaigCity();
                    $city->campaign_id = $request->editid;
                    $city->state_id = 101;
                    $city->city =$cityData->city;
                    $city->save();
                    }
              
            }
            else{
                
                foreach($request->states as $key => $statusData){
                 
                    foreach($request->cities as $key => $cityData){
                        $city = new CampaigCity();
                        $city->campaign_id = $obj->id;
                        $city->state_id = 101;
                        $city->city =$cityData;
                        $city->save();
                        }
              
            }
        }
          
           
          
        

            /*=======Remove Old Entries And Add New============*/
            

            $delete = matched_influencer::where('campaign_id',$obj->id)->delete();
            if($request->influencers[0] != null || !empty($request->influencers[0])) {
                foreach (str_getcsv($request->influencers[0]) as $data) {
                    $matched_influencer = new matched_influencer();
                    $matched_influencer->campaign_id = $obj->id;
                    $matched_influencer->influencer_id = $data;
                    $matched_influencer->save();
                }
            }
            /*=======Remove Old Entries And Add New============*/

            if(isset($request->editid))
            {
                toastr()->success('Campaign has been Updated successfully!');
                return redirect()->route('admin.get.campaign');
            }
            else{
                toastr()->success('Campaign has been Added successfully!');
                return redirect()->route('admin.get.campaign');
            }
        }
        catch (Exception $e){
            toastr()->warning($e);
            return redirect()->route('admin.get.campaign');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // $state = states::where('country_id',101)->get();
        $campaign = Campaign::with('getState','getCity','getCampaignChannel')->find($id);
       
        foreach($campaign->getState as $da){   
            $state = states::where('id',$da->state)->get();
        }
        foreach($campaign->getCity as $da){
            $cities = cities::where('id',$da->city)->get();
        }

        $email = AdminUser::where('au_ar_id',3)
            ->get();
        return view('backend.pages.campaign.edit')->with(compact('campaign','state','cities','email'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
    public function delete($id){

        $campaign = Campaign::where('id',$id)->delete();
        return response()->json(1);
    }
    public function fetchCities($id){
        if(isset($id) && $id !=-1){
            $val[] = str_getcsv($id);
            $output = '<option disabled selected>---SELECT CITIES---</option>';
            $count = count($val[0]);
            if($count > 0){
                for($i=0;$i<$count;$i++){
                    $cities = cities::where('state_id',$val[0][$i])->get();
                    if(!$cities->isEmpty()) {
                        foreach ($cities as $row) {
                            $output .= '<option value="' . $row->id . '">' . $row->name . '</option>';
                        }
                    }
                }
            }
        }else{
            $output = '<option disabled selected>---SELECT CITIES---</option>';
        }
        return response()->json($output);
    }

    public function fetchInfluencers(Request $request){
        
        foreach($request as $data){
            dd($data);
        }
        $count = InfluencerUsers::query();

        if(isset($request->state)){
            $filter['state'] = '';
            foreach ($request->state as $data)
                $filter['state'] = $filter['state'] . $data . ",";

            $count = $count->where('state','like','%'.$filter['state'].'%');
        }

        if(isset($request->cities)){
            $filter['cities'] = '';
            foreach ($request->cities as $data)
                $filter['cities'] = $filter['cities'] . $data . ",";

            $filter['cities'] = rtrim($filter['cities'], ", ");
            $count = $count->where('city','like','%'.$filter['cities'].'%');
        }

        if(isset($request->channel)){
            $filter['channel'] = '';
            foreach ($request->channel as $data){
                $filter['channel'] = $filter['channel'] . $data . ",";
                $count = $count->where('channel','like','%'.$filter['channel'].'%');
            }
        }

        if(isset($request->gender) && $request->gender != 2)
            $count = $count->where('gender',$request->gender);

        if(isset($request->fromage)){
            $minDate = Carbon::today()->subYears($request->fromage)->toDateString();
            $count = $count->where('date_of_birth','<=',$minDate);
        }


        if(isset($request->toage)){
            $maxDate = Carbon::today()->subYears($request->toage)->endOfDay()->toDateString();
            $count = $count->where('date_of_birth','>=',$maxDate);
        }

        $response = $count->get();
        $count = $response->count();

        return response()->json(array(
            'count' => $count,
            'data' => $response
        ));
    }

    public function manage($id){
        $campaign = Campaign::where('id',$id)->get();

        if($campaign->isNotEmpty())
            return view('backend.pages.campaign.manageCampaign')->with(compact('campaign'));
        else
            return view('backend.pages.campaign.manageCampaign');

    }

    public function manageStore(Request $request){

        $request->validate([
            'id'=>'required|integer',
            'status'=>'required|integer',
            'datetime'=>'required',
            'batchsize'=>'required|integer',
            'time'=>'required|integer',
            'remuneration'=>'required',
            'rc'=>'required',
            'noc'=>'required',
        ]);

        $matched_influencer = matched_influencer::where('campaign_id',$request->id)->get();
        $totalInfluencer = $matched_influencer->count();
        $time = new DateTime($request['datetime']);
        $untilDate = $time->format('Y-m-d');
        $camp = Campaign::where('id',$request->id)->get();
        $start_date= new DateTime($camp[0]->campaign_start);
        $end_date = new DateTime($untilDate);
        $interval = $start_date->diff($end_date);
        $days = $interval->format('%a');
        $days++;
        $loop = $totalInfluencer/$days;
        $loop = ceil($loop);
        $i=1;
        $cnt = 0;
        foreach($matched_influencer as $infData)
        {
            $infEmail = InfluencerUsers::where('id',$infData->influencer_id)
                ->select([
                    'email'
                ])
                ->get();

            $email_template = emailTemplate::where('id','3')
                ->select([
                    'subject',
                    'template'
                ])
                ->first();

            if($i%$loop == 0 && $cnt == 0){
                $executed = $start_date;
                $cnt++;
            }
            else{
                if($i%$loop == 0){
                    $executed = $start_date->add(new \DateInterval('P1D'));
                    echo $loop;
                }
                else{
                    $executed = $start_date;
                }
            }
            $field = emailField::get();
            $CampaingData = Campaign::where('id',$request->id)->get();
            $influencerData = InfluencerUsers::where('id',$infData->influencer_id)->get();

            $mail_vars['influencer_name'] = $influencerData[0]['firstname'];
            $mail_vars['influencer_lastname'] = $influencerData[0]['lastname'];
            $mail_vars['verify_account'] = '123';
            $mail_vars['user_email'] = $influencerData[0]['email'];
            $mail_vars['not_approve'] = 'not approve';
            $mail_vars['campaign_Title'] = $CampaingData[0]['title'];

            $mail_vars['campaign_description'] = $CampaingData[0]['message'];
            $mail_vars['start_campaign'] = $CampaingData[0]['campaign_start'];
            $mail_vars['end_campaign'] = $CampaingData[0]['campaign_end'];
            $mail_vars['remuneration_amount'] = $CampaingData[0]['remuneration'];

            if($infEmail->isNotEmpty()){
                $store = new crone_mail();
                $store->email = $infEmail[0]->email;
                $store->subject = $this->srepltags_array($mail_vars,$email_template->subject);
                $store->body = $this->srepltags_array($mail_vars,$email_template->template);
                $store->influencer_id = $infData->influencer_id;
                $store->campaign_id = $request->id;
                $store->executedate = $executed;
                $store->status = 1;
                $store->reminder = 0;
                $store->save();
            }
            $i++;
        }

        /*========Update Campaign Table===========*/
        $camp = Campaign::where('id',$request->id)
            ->update([
                'status'=>$request->status,
                'send_mail_until' => $request->datetime,
                'batch_size'=>$request->batchsize,
                'cron_interval'=>$request->time,
                'remuneration'=>$request->remuneration,
                'comment_remuneration'=>$request->rc,
                'comment_number'=>$request->noc
            ]);
        /*========Update Campaign Table===========*/
        toastr()->info('Campaign has been Managed successfully!');
        return redirect()->route('admin.get.campaign');
    }

    public function srepltags_array( $arr, $str) {
        if ( is_array( $arr) ) {
            reset($arr);
            $keys = array_keys( $arr);
            array_walk( $keys, create_function('&$val', '$val = "[$val]";'));
            $vals = array_values( $arr);
            return str_replace( $keys, $vals, $str);
        } else {
            return $str;
        }
    }

    public function batchSize(Request $request){
        $time = new DateTime($request->date);
        $date = $time->format('Y-m-d');

        $camp = Campaign::where('id',$request->campId)
            ->select([
                'campaign_start',
                'campaign_end'
            ])
            ->get();


        $totalInfluencer = matched_influencer::where('campaign_id',$request->campId)
            ->get()
            ->count();

        if($date >= $camp[0]['campaign_start'] && $date <= $camp[0]['campaign_end']){
            $start_date= new DateTime($camp[0]['campaign_start']);
            $end_date = new DateTime($date);
            $interval = $start_date->diff($end_date);
            $days = $interval->format('%a');
            $days++;
            $cronInterval = $request->interval;
            $availMinutes = 1440 * $days;
            $batchsize = $cronInterval*$totalInfluencer/$availMinutes;
            $batchsize = round($batchsize) + 1;
            return response()->json($batchsize);
        }
        else{
            return response()->json(-1);
        }
    }
}
