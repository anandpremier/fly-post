<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\faqs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Abraham\TwitterOAuth\TwitterOAuth;
use Thujohn\Twitter\Facades\Twitter as Twitter;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $obj = faqs::get();
        return view('backend.pages.contentManagement.faq.index')->with(compact('obj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.contentManagement.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->editid != '' || isset($request->editid))
        {
            $request->validate([
                'question'=>'required',
                'editor'=>'required',
                'status'=>'required|integer',
                'for_whom'=>'required|integer',
            ]);
            $obj = faqs::findorfail($request->editid);
            $obj->question = $request->question;
            $obj->answer = $request->editor;
            $obj->status = $request->status;
            $obj->for_whom = $request->for_whom;
            $obj->save();

            toastr()->success('FAQ has been Updated successfully!');
            return redirect()->route('admin.faq.get');
        }
        else
        {
            $request->validate([
                'question'=>'required',
                'editor'=>'required',
                'status'=>'required|integer',
                'for_whom'=>'required|integer',
            ]);
            $obj = new faqs();
            $obj->question = $request->question;
            $obj->answer = $request->editor;
            $obj->status = $request->status;
            $obj->for_whom = $request->for_whom;
            $obj->save();

            toastr()->success('FAQ has been Added successfully!');
            return redirect()->route('admin.faq.get');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = faqs::where('id',$id)->get();
        return view('backend.pages.contentManagement.faq.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id){
        $campaign = faqs::where('id',$id)->delete();
        return response()->json(1);
    }

    public function post(){
        /*Twitter::postTweet(array('status' => '1Tweet sent using Laravel and the Twitter'));*/
        $tweets = Twitter::getUserTimeline(array('screen_name' => 'vegibit', 'count' => 20, 'format' => 'object'));
        dd($tweets);
        foreach($tweets as $tweet) {
            echo '<b>Tweet Text:</b> ' . Twitter::linkify($tweet->text) . '<br>';
            echo '<strong>Posted By:</strong> <a href="http:' . Twitter::linkUser($tweet->user) .
                '">' . $tweet->user->name . '</a> <em>' . Twitter::ago($tweet->created_at) . '</em><br>';
            echo '<strong>Original Tweet:</strong> <a href="http:' . Twitter::linkTweet($tweet) .
                '">http:' . Twitter::linkTweet($tweet) . '</a><hr>';
        }
    }
}
