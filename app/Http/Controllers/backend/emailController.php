<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\email;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class emailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $obj = email::get();
        return view('backend.pages.contentManagement.emailList')->with(compact('obj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.contentManagement.addEmail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if(isset($request->id)){
                $request->validate([
                    'email'=>'required|string|unique:emails,email,'.$request->id,
                    'designation'=>'required|string',
                ]);

                $obj = email::where('id',$request->id)
                    ->update([
                        'email'=>strtoupper($request['email']),
                        'designation'=>strtoupper($request['designation'])
                    ]);

                toastr()->success('Email has been Updated successfully!');
                return redirect()->route('admin.email.index');
            }else{
                $request->validate([
                    'email'=>'required|string|unique:emails,email',
                    'designation'=>'required|string',
                ]);

                $obj = new email();
                $obj->email = strtoupper($request['email']);
                $obj->designation = strtoupper($request['designation']);
                $obj->save();

                toastr()->success('Email has been Added successfully!');
                return redirect()->route('admin.email.index');
            }
        }catch (Exception $e){
            toastr()->error($e);
            return redirect()->route('admin.email.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = email::where('id',$id)->get();
        return view('backend.pages.contentManagement.emailEdit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id){
        $campaign = email::where('id',$id)->delete();
        return response()->json(1);
    }
}
