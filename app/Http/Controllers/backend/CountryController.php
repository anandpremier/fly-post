<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $country = Country::get();
        return view('backend.pages.country.country')->with(compact('country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.country.addCountry');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->editid != '' || isset($request->editid))
            {
                $this->validate($request,[
                    'country'=>'required|unique:countries,name,'.$request->editid,
                    'country_code'=>'required',
                    'phoneCode'=>'required|integer',
                ]);
                $country1 = Country::findOrFail($request->editid);
                $country1->name = $request->country;
                $country1->code = $request->country_code;
                $country1->phonecode = $request->phoneCode;
                $country1->save();
                toastr()->success('Country has been Updated successfully!');
                return redirect()->route('admin.get.country');
            }else{
                $this->validate($request,[
                    'country'=>'required|unique:countries,name',
                    'country_code'=>'required',
                    'phoneCode'=>'required|integer',
                ]);
                $country1 = new Country();
                $country1->name = $request->country;
                $country1->code = $request->country_code;
                $country1->phonecode = $request->phoneCode;
                $country1->save();
            }
            toastr()->success('Country has been Added successfully!');
            return redirect()->route('admin.get.country');
        }catch (Exception $e){
            toastr()->error('Country has not been Added!');
            return redirect()->route('admin.get.country');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        return view('backend.pages.country.editCountry')->with(compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id){
        $destroy = Country::where('id',$id)->delete();
        return response()->json(1);
    }
}
