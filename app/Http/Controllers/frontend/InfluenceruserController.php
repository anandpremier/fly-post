<?php

namespace App\Http\Controllers\frontend;

use App\Model\backend\cities;
use App\Model\backend\states;
use App\Model\frontend\InfluencerUsers;
use Illuminate\Http\Request;
use App\Model\backend\faqs;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Crypt;
use Illuminate\Support\Carbon;
use App\Model\frontend\Campaignshared;
use App\Model\backend\crone_mail;
use App\Model\backend\Campaign;


class InfluenceruserController extends Controller {

    public function edit($id) {
        $InfluUserget = InfluencerUsers::find($id);
        $InfluUserget->state = str_getcsv($InfluUserget->state);
        $InfluUserget->city = str_getcsv($InfluUserget->city);
        $InfluUserget->channel = str_getcsv($InfluUserget->channel);
        $city_id = $InfluUserget->city[0];
        $state = states::where('country_id', 101)->get();
        $city = cities::where('id', $city_id)->get();
        return view('auth.edit', compact('InfluUserget', 'state', 'city'));
    }

    public function update(Request $request) {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'email|required',
            'date_of_birth' => 'date|required',
            'country' => 'required',
            'zipcode' => 'required|integer',
            'state' => 'required|integer',
            'city' => 'required|integer',
            'gender' => 'required|integer',
        ]);

        $obj = InfluencerUsers::findorfail($request->id);
        $obj->firstname = $request->firstname;
        $obj->lastname = $request->lastname;
        $obj->email = $request->email;
        $obj->date_of_birth = $request->date_of_birth;
        $obj->country = $request->country;
        $obj->zipcode = $request->zipcode;
        $obj->state = $request->state;
        $obj->city = $request->city;
        $obj->gender = $request->gender;
        $obj->save();

        toastr()->success('Influencer has been Updated successfully!');
        return redirect()->route('campaign');
    }

    public function influenceruserFaq() {
        $sessionUserCheck = Session::get('influencer');
        if (isset($sessionUserCheck))
            $value = 1;
        else
            $value = 0;
        $influnceFaqget = faqs::where('for_whom', '=', $value)
            ->orWhere('for_whom', '=', 0)
            ->get();
        return view('frontend.page.faq', compact('influnceFaqget'));
    }

    public function changePassword(Request $request) {
        $user = InfluencerUsers::findOrFail($request->id);
        if (!Hash::check($request['old_password'], $user->password)) {
            toastr()->error('Password does not match');
            return redirect()->route('campaign');
        } else {
            $user->fill([
                'password' => Hash::make($request->retype_password)
            ])->save();
            toastr()->success('Password changed');
            return redirect()->route('campaign');
        }
    }

    public function fetchCities(Request $request) {
        $val[] = str_getcsv($request->id);
        $output = '<option disabled selected>SELECT CITY</option>';
        $count = count($val[0]);
        for ($i = 0; $i < $count; $i++) {
            $cities = cities::where('state_id', $val[0][$i])->get();
            if (!$cities->isEmpty()) {
                foreach ($cities as $row) {
                    $output .= '<option value="' . $row->id . '">' . $row->name . '</option>';
                }
            }
        }
        return response()->json($output);
    }



    public function history() {
        $campaign_shared = Campaignshared::query()
            ->leftJoin('campaign', 'campaign.id', '=', 'campaign_shared.ca_id')
            ->where('influencerusers_id', Auth::user()->id)
            ->select([
                'campaign.title',
                'campaign.sponsored_post',
                'campaign_shared.created_at',
                'campaign.remuneration'
            ])->get();

        return view('frontend.page.history')->with(compact('campaign_shared'));
    }

    public function updateAccountStatus($token)
    {
        $token = \Crypt::decrypt($token);
        $obj = InfluencerUsers::findorfail($token);
        $obj->email_verify = 1;
        $obj->save();

        Session::start();
        Session::put('successEmailVerify','Your Email Verification was Successfully!!Please Login To Continue');
        return redirect()->route('index');
    }

    public function shareVerify(Request $request){
        try{
            $mytime = Carbon::now();
            $cron_mail = crone_mail::findorfail($request->id);
            $campaign = Campaign::findorfail($cron_mail->campaign_id);
            $campaignshared = Campaignshared::whereRaw('DATE(created_at) = ?', [$mytime->toDateString()])->get()->count();

            if($campaign->c_d_S > $campaignshared)
                return response()->json(1);
            else
                return response()->json(0);
        }catch (\Exception $e){
            return response()->json(0);
        }
    }
}
