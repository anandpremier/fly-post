<?php

namespace App\Model\frontend;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Campaignshared extends Model
{
    use Notifiable;

    protected $table = 'campaign_shared';
    protected $primaryKey = 'id';
     
      protected $fillable = [
        'ca_id','influencerusers_id', 'first_channel_shared', 'shared_on_fb','shared_on_twitter','share_price','comment_remuneration','sharing_comment','ispaid','comment_checked','pa_id','device',
          'post_url_fb','post_url_twitter','cs_post_DeletedOrNotPublic',
          
    ];

}
