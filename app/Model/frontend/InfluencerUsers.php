<?php

namespace App\Model\frontend;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class InfluencerUsers extends Authenticatable
{
     use Notifiable;

    protected $table = 'influencerusers';
    protected $primaryKey = 'id';

    protected $fillable = [
        'firstname','lastname', 'email', 'password','date_of_birth','country','city','state','zipcode','gender','terms_and_conditions','privacy_policy',
          'status','email_verify','provider_user_id', 'provider','channel'

    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
