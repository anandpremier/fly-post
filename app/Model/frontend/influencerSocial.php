<?php

namespace App\Model\frontend;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class influencerSocial extends Model {

    use Notifiable;

    protected $table = 'influencer_social_networks';
    protected $primaryKey = 'id';
    protected $fillable = [
        'influencer_id', 'channel_id', 'channelname', 'channel_userid', 'isapporved', 'reason_notapproved', 'channel_accesstoken', 'twitter_accesstoken_secret', 'channel_approval_date'];



}
