<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class email extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'emails';
    protected $guard = 'admin';
    protected $fillable = [
        'email','designation'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
