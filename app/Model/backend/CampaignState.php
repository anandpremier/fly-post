<?php

namespace App\Model\backend;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class CampaignState extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'campaign_state';
    protected $guard = 'admin';
    protected $fillable = [
        'campaign_id','country_id','state'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
