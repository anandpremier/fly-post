<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class emailField extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'email_fields';
    protected $guard = 'admin';
    protected $fillable = [
        'field_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
