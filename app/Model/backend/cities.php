<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class cities extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'cities';
    protected $guard = 'admin';
    protected $fillable = [
        'name','state_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
