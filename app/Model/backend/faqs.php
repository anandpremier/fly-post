<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class faqs extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'faqs';
    protected $guard = 'admin';
    protected $fillable = [
        'question','answer','status','for_whom'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
