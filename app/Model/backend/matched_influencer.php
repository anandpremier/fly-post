<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class matched_influencer extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'matched_influencers';
    protected $guard = 'admin';
    protected $fillable = [
        'campaign_id','influencer_id','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
