<?php

namespace App\Model\backend;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class CampaigCity extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'campaign_city';
    protected $guard = 'admin';
    protected $fillable = [
        'campaign_id','state_id','city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
