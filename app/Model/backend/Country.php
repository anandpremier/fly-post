<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Country extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'countries';
    protected $guard = 'admin';
    protected $fillable = [
        'iso','name', 'phonecode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
