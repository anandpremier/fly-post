<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminUser extends Authenticatable
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'adminuser';
    protected $guard = 'admin';
    protected $fillable = [
        'username','firstname','lastname', 'email', 'password','photo','phone','mobile','skype'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
