<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class emailTemplate extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'email_templates';
    protected $guard = 'admin';
    protected $fillable = [
        'event','from','cc','bcc','subject','template'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
