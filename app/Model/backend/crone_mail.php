<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class crone_mail extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'cron_mail';
    protected $guard = 'admin';
    protected $fillable = [
        'email','subject','body','influencer_id','campaign_id','executedate','status','reminder'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function InfluencerUsers()
    {
        return $this->hasMany('App\Model\frontend\InfluencerUsers', 'id', 'influencer_id');

    }
    public function campaignData()
    {
        return $this->hasMany('App\Model\backend\Campaign', 'id', 'campaign_id');

    }
}
