<?php

namespace App\Model\backend;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class CampaignChannel extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'campaign_channel';
    protected $guard = 'admin';
    protected $fillable = [
        'campaign_id','channel'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
