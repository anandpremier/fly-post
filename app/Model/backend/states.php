<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class states extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'states';
    protected $guard = 'admin';
    protected $fillable = [
        'name','country_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
