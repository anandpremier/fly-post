<?php

namespace App\Model\backend;
use App\Model\backend\CampaignState;
use App\Model\backend\CampaigCity;
use App\Model\backend\CampaignChannel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Campaign extends Model
{
    use Notifiable;

    protected  $primaryKey = 'id';
    protected $table = 'campaign';
    protected $guard = 'admin';
    protected $fillable = [
        'admin_id','campaign_start','campaign_end','campaign_type','message',
        'sponsored_post','link','image','title','name','gender','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function getState(){

       return $this->hasMany(CampaignState::class, 'campaign_id');
        
        
    }
    public function getCity(){
        return $this->hasMany(CampaigCity::class, 'campaign_id');
    }

    public function getCampaignChannel(){
        return $this->hasMany(CampaignChannel::class, 'campaign_id');
    }
}
