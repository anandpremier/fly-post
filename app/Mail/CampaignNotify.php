<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CampaignNotify extends Mailable
{
    use Queueable, SerializesModels;
    public $template;
    public $from;
    public $subject;
    public $objData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($objData)
    {
        $this->campaign = $objData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $campaigns = $this->campaign;
        return $this
            ->from("jatin.premierinfotech@gmail.com",'TheFlyPost-[Campaign Notification]')
            ->subject('You Are Invited For Following Campaigns')
            ->view('mail.campaignNotification',['campaigns' => $campaigns]);

    }
}
