<?php

namespace App\Mail;

use App\Http\Controllers\Auth\RegisterController;
use App\Model\backend\crone_mail;
use App\Model\backend\emailField;
use App\Model\backend\emailTemplate;
use App\Model\frontend\InfluencerUsers;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $template;
    public $from;
    public $subject;
    public $objData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($objData)
    {
        $this->objData = $objData;
        $this->userId = isset($this->objData['userId']) ? $this->objData['userId'] : '';
        $this->status = isset($this->objData['status']) ? $this->objData['status'] : '';
        $this->statusActive = isset($this->objData['statusActive']) ? $this->objData['statusActive'] : '';
        $this->mail_url_id = isset($this->objData['mail_url_id']) ? $this->objData['mail_url_id'] : '';
        $this->template = isset($this->objData['template']) ? $this->objData['template'] : '';
        $this->from_email = isset($this->objData['from']) ? $this->objData['from'] : '';
        $this->subject = isset($this->objData['subject']) ? $this->objData['subject'] : '';
        $this->mailSubject = isset($this->objData['mailSubject']) ? $this->objData['mailSubject'] : '';
        $this->mailBody = isset($this->objData['mailBody']) ? $this->objData['mailBody'] : '';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->from_email) && !empty($this->from_email)){
            $url = url('/influencer/account/'.$this->mail_url_id);
            return $this
                ->from(env('USERNAME_EMAIL'),'Confirm Your Email Account')
                ->subject($this->subject)
                ->html($this->template);
                //->view('mail.influencerInvitation', ['verify_mail' => $url]);
        }

        if(isset($this->status) && $this->status == 1 && isset($this->userId)){
            $Rows = InfluencerUsers::where('id',$this->userId)->get();
            $emailTemplate = emailTemplate::where('event','APPROVE FOR INFLUENCER')->firstOrFail();

            $replaceDom = emailField::get();
            $replaceParameter = new RegisterController();
            foreach($replaceDom as $field){
                $field['field_name'] = '['.$field['field_name'].']';
                $find = strstr($emailTemplate->template,$field['field_name']);
                if(isset($find) && !empty($find)){
                    $emailTemplate->template = $replaceParameter->replace_parameter($emailTemplate->template,$field['field_name'],$Rows);
                }
            }

            return $this
                ->from($address = 'noreply@domain.com', $name = 'TheFlyPost')
                ->subject('Account Confirmed')
                ->html($emailTemplate->template);
        }

        return $this
            ->from(env('USERNAME_EMAIL'),'CroneMail')
            ->subject($this->mailSubject)
            ->html($this->mailBody);
    }
}
