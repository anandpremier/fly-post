<?php

namespace App\Console\Commands;

use App\Model\backend\crone_mail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;

class ReCallCampaign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ReCallCampaign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminds Influencers About Campaign';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $obj = crone_mail::whereDate('executedate','<=',\Carbon\Carbon::now())
            ->where('status','=','1')->get();
;
        $user = $obj->count();

        foreach ($obj as $email) {

            $object = [
                'mailSubject'=> $email['subject'],
                'mailBody'=> $email['body'],

            ];
            Mail::to($email['email'])
                ->send(new SendMailable($object));
        }
        $this->line('Influencers Were Recalled About Campaign Successfully-');
    }
}
